��    �      4      L      L  
   M     X     f     v  '   z     �     �     �     �     �  	   �     �     
	     	     	  	   	     &	     2	     5	  
   <	     G	  
   P	     [	     p	     x	     �	     �	  	   �	     �	     �	     �	     �	     �	  	   �	     �	  	   �	     
     
     
     &
     -
     2
     9
     >
     C
     J
     R
     X
     _
     g
     l
  	   ~
     �
     �
  	   �
     �
     �
     �
  
   �
  	   �
     �
     �
  +   �
          "     '     -     4     @     M     U     b     w     �     �     �     �  
   �     �     �     �  	   �  	   �     �     
          +     4     <     M     T     e     k     q     v     }     �     �     �     �     �     �     �  '     &   +     R     j     q     y     �     �     �     �     �     �     �     �     �     �  	   �     �     �     �               !     3     7     =     B  �  E  
   �     �     	       +        I     Z     v     �     �     �     �     �     �     �  	   �     �     �  	   �  
     	             ,     I     U     f     r     �     �  	   �  
   �     �     �     �     �  
   �     �     �     �                    "     (  	   .     8  	   A     K     T     g     m  
   �  	   �  	   �     �     �     �     �     �     �     �     �  /        <     L     R     Y     a     n     w     �     �     �     �     �     �     �     �       	     	   "     ,     :     H     h  	   �     �     �     �  	   �     �  	   �     �     �     �     �       
   &     1     J     Q     e     y  7   �  5   �  ;   �     0     7     @     N     S     Y     _     c     g          �     �     �     �     �     �     �     �     �          0     4     <     A   $ - Dollar &euro; - Euro &pound; - Pound 404 404! Page Not Found404! Page Not Found Add to Cart Add to WishList All Categories Archive by Category Archive for Archives: Articles Posted by Author: Best Blog Blog Page Buy product By Cancel Categories Checkout Clear Cart Comments are closed. Compare Continue Shopping Continue reading Continue reading... Customize Daily Archives Description Discount Codes EN Edit Edit item English Error 404 Footer French General Settings German Home Latest Left List Log In Log Out Login Logout MY CART Menu Monthly Archives: Move down Move up My Cart My Search My Wishlist New Next &raquo; Next Image Next page No No products found Oops! The Page you requested was not found! Out of stock Page Pages Pages: Pages:&nbsp Pages:&nbsp; Popular Posts Tagged Posts tagged &ldquo; Previous Image Proceed to Checkout Product Banner Products Published by Quick View RSS URL Random Rated Read More Read more Recently added item(s) Recently added items Recommended Register Related Related Products Remove Remove This Item Reply Right Sale Search Search Results for Search results for &ldquo; Select Select options Seller Shopping Cart Total Show Best Seller Products Social Media Sorry, What you are looking isn't here. Sorry, no posts matched your criteria. Sorry, nothing in cart. Source Special Subtotal Tags Title Total URL USD Update Cart Upsell Upsell Product Upsell Products VIEW ALL View Cart View products Wishlist Yearly Archives: Yes You are logged in as You may also like hot items link no Project-Id-Version: flavours
POT-Creation-Date: 2017-01-13 14:42+0000
PO-Revision-Date: 2017-01-29 20:36+0000
Last-Translator: Selby Khuzwayo <selby.khuzwayo@foodport.ch>
Language-Team: German (Switzerland)
Language: de-CH
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=n != 1
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To:  $ - Dollar &euro; - Euro &pound; - Pfund 404 404! [ Seite konnte nicht gefunden werden ] In den Warenkorb Zur Wunschliste hinzufügen Alle Kategorien Nach Kategorie archivieren Archiv Archiv:  Artikel veröffentlicht von Autor:  Best Blog Blogseite Produkt kaufen Von Abbrechen Kategorien Zur Kasse Warenkorb leeren Kommentare sind geschlossen. vergleichen Weiter einkaufen Weiterlesen Weiterlesen... anpassen Tägliches Archiv Beschrieb Gutscheine EN Anpassen Artikel bearbeiten Englisch Fehler 404 Footer Französisch Allgemeine Einstellungen Deutsch Home Neu Links Liste Einloggen abmelden Einloggen Abmelden Mein Einkaufswagen Menü Monatliches Archiv: Nach unten nach oben Warenkorb Meine Suche Meine Wunschliste Neu Weiter &raquo; Nächstes Bild Nächste Seite Nein Keine Produkte gefunden Oops! Diese Seite konnte nicht gefunden werden! Nicht auf Lager Seite Seiten Seiten: Seiten:&nbsp Seiten:  Populär Markierte Artikel Markierte Beiträge &ldquo; Vorheriges Bild Weiter zur Kassen Produkt-Banner Produkte Veröffentlicht von Schnelle Übersicht RSS URL Zufällig bewertung Mehr erfahren Mehr erfahren Zuletzt hinzugefügte Artikel:  Zuletzt hinzugefügte Artikel Empfohlen Registrieren Ähnlich Ähnliche Produkte Entfernen Diesen Artikel entfernen Antworten Rechts Sale Suche Suchresultate für Suchresultate für &ldquo; Auswählen Einstellungen auswählen Seller Einkaufswagen Total Bestseller anzeigen Social Media Tut uns leid. Ihre Suche war leider nicht erfolgreich.  Tut uns Leid, kein beitrag entpricht Ihren Kriterien. Tut uns leid, Sie haben leider keine Produkte im Warenkorb. Quelle Speziell Zwischensumme Tags Titel Total URL USD Warenkorb aktualisieren Zusatzverkauf zusatzverkauf Zusatzverkäufe Alles ansehen Zum Warenkorb Produkte ansehen. Wunschliste Jährliches Archiv:  Ja Sie sind angemeldet als Das könnte Ihnen gefallen hot Objekte Link Nein 