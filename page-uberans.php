<?php
/* Template name: Uber Ans */
get_header();
?>

<style text="text/css">
.content-9{background:url('/wp-content/uploads/2016/12/content-9-bg.jpg') center top no-repeat;min-height:1024px;background-size:cover}.content-9 .heading,.content-9 h2{color:#fff}.content-9 dl{padding:0;margin:20px 0 0;width:100%;float:left;text-align:left}.content-9 dt{float:left;width:85px;height:85px;text-align:center;padding-top:25px;display:block;border-radius:50%;background:#f2c7c9;margin-right:15px;color:#fff;font-style:italic}.content-9 dd{padding-right:15px}.content-9 .image-with-pointer{position:relative}.content-9 .feature-1 dt{background-color:#ef6167}.content-9 .feature-2 dt{background-color:#e8b735}.content-9 .feature-3 dt{background-color:#2fba71}.content-9 .feature-4 dt{background-color:#2fb6d3}@media (min-width:768px){.content-9 dl{width:50%}}@media (min-width:992px){.content-9 .feature-1 dt,.content-9 .feature-4 dt{float:right;margin-left:15px}.content-9 dl{width:357px;position:absolute}.content-9 .image-with-pointer{max-width:570px}.content-9 .feature-1{left:-180px;top:80px}.content-9 .feature-1 dd{color:#cef4bc;text-align:right}.content-9 .feature-2{right:-175px;top:150px}.content-9 .feature-3{right:-165px;bottom:100px}.content-9 .feature-4{left:-130px;bottom:50px}.content-9 .feature-4 dd{text-align:right}}
.pb60{padding-bottom: 60px;}.hexagon{position:relative;width:280px;height:350px;background-color:#e6e2da;margin:100px auto}.hexagon div{padding:0 50px;text-align:center}.hexagon img{position:absolute;left:35%;z-index:10}.hexagon:after,.hexagon:before{content:"";position:absolute;width:0;border-left:140px solid transparent;border-right:140px solid transparent}.hexagon:before{bottom:100%;border-bottom:86.6px solid #e6e2da}.hexagon:after{top:100%;width:0;border-top:86.6px solid #e6e2da}.hexagon.three{clear:left}.top-left,.top-right{float:left;width:100%}.content-hexa .no{width:50px;height:50px;border-radius:50%;border:1px solid #a29376;display:block;text-align:center;line-height:50px;margin:0 auto 20px}.content-hexa .read-more{font-size:16px}.content-hexa .read-more a{color:#968665}.content-hexa .read-more a:hover{color:#222}@media (min-width:480px){.hexagon{width:480px;height:360px}.hexagon img{left:50%;margin-left:-43px}.hexagon:after,.hexagon:before{border-left-width:240px;border-right-width:240px}.content-hexa .no{width:70px;height:70px;line-height:70px;border-width:2px;font-size:40px;font-weight:100}}@media (min-width:992px){.top-left,.top-right{float:left;width:50%}.hexagon{width:350px;height:310px;margin:85px 0}.hexagon img{bottom:-50px}.hexagon:after,.hexagon:before{border-left-width:175px;border-right-width:175px}.content-hexa .no{width:70px;height:70px;line-height:70px;border-width:2px;font-size:40px}.content-hexa .bottom{position:relative}.content-hexa .bottom .overlap-image{position:absolute;right:-70px;bottom:-170px}.hexagon.one{float:right;margin-right:15px}.hexagon.two{float:left;margin-left:15px}.hexagon.three{margin-top:8px;margin-left:auto;margin-right:auto}}@media (min-width:1200px){.hexagon.three,.read-more{margin-top:20px}.hexagon{width:550px}.hexagon:after,.hexagon:before{border-left-width:275px;border-right-width:275px}.hexagon div{padding:0 120px}.content-hexa .top{margin-top:50px}}.withe-logo{display:block;margin:0 auto}@media (min-width:992px){.withe-logo{margin-top:-200px}}
</style>

<div id="slider" class="sl-slider">
		<div style="background-image:url('/wp-content/uploads/2016/12/bg.jpg');">
			<div class="text-center div-middle">
				<h1 class="tp-caption LargeTitle"> </h1>
				<h2 class="tp-caption Title"> </h2>
				<a class="buy-btn" href="/shop">Shop Now</a>
			</div>
		</div>
</div>

<section class="content-section content-9" style="padding-top: 60px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center heading">
                <h2>FoodPort</h2>
                <p>some killer slogan</p>
            </div>
        </div>
    </div>
    <div class="image-with-pointer text-center container"><img src="/wp-content/uploads/2016/12/content-image.png" alt="" height="650" width="570">
        <dl class="feature-1"><dt>Feature<br>01</dt>
            <dd>long established fact that a reader will be distracted by the readable content of a page when looking</dd>
        </dl>
        <dl class="feature-2"><dt>Feature<br>02</dt>
            <dd>long established fact that a reader will be distracted by the readable content of a page when looking</dd>
        </dl>
        <dl class="feature-3"><dt>Feature<br>03</dt>
            <dd>long established fact that a reader will be distracted by the readable content of a page when looking</dd>
        </dl>
        <dl class="feature-4"><dt>Feature<br>04</dt>
            <dd>long established fact that a reader will be distracted by the readable content of a page when looking</dd>
        </dl>
    </div>
</section>

<section class="content-section content-hexa farming-tipps pb60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>Foodport Shop</h2>
                <p>Waren Sie schon mal in den Ferien und haben eine Delikatesse gekostet, die ihnen äusserst gefiel, jedoch nicht erhältlich ist in der Schweiz? Oder vermissen Sie gewisse Lebensmittel die nur in Ihrer Heimat erhältlich sind? Dann ist unsere Home-Drop Lieferung genau das richtige für Sie. Wir haben uns bemüht ein Sortiment mit mehr als 1'000 ausländischen Produkten aufzubauen. Dieses wächst von Tag zu Tag.  Profitieren Sie davon indem Sie Ihre lieblings Lebensmittel auswählen und diese zu günstigen Konditionen nach Hause liefern lassen. Wir liefern Ihnen die bestellte Ware bis zur Haustür und nehmen Ihre Unterschrift entgegen. Bei Abwesenheit, deponiert der Kurrier Ihr Paket am Eingang.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="top clearfix">
            <div class="top-left">
                <div class="hexagon tipps one">
                    <div><span class="no">01</span>
                        <p>Beschenken Sie Ihre fleissigen Kollegen in der Schweizer armee. Unser Sortiment umfasst fünf verschiedene Fresspäckchen mit unterschiedlichem Inhalt. Die Lieferung ist selbstverständlich gratis.</p>
                    </div>
                </div>
            </div>
            <div class="top-right">
                <div class="hexagon tipps two">
                    <div><span class="no">02</span>
                        <p>Unser intelligenter Online-Shop erleichtert Ihnen den Einkauf. Sie können Einkaufslisten erstellen, speichern und zu jeder Zeit wieder verwenden. Haben Sie ein Produkt nicht finden können? Kein Problem, schlagen Sie das Produkt möglichst detailiert vor. Ist die Nachfrage stark, wird das gewünschte Produkt sehr schnell in unser Sortiment aufgenommen. Des Weiteren ist der Shop sehr schlicht aufgebaut. Egal ob Debitkarte oder Kreditkarte, können Zahlungen kinderleicht getättigt werden.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="hexagon tipps three">
                <div><span class="no">03</span>
                    <p>Sind Sie Mitglied in einem Verein und möchten Ihre Bestellung auf Grund des Administrativen Aufwands im Voraus abgeben? Findet demnächst eine Hochzeit statt und Sie möchten die gewünschten Lebensmittel auf die Stunde genau erhalten? Veranstalten Sie eine grosse Geburtstagsparty oder grössere Feier anderer Art und wollen die bestellte Ware am Stichtag direkt vor Ort liefern lassen und nicht in Paketen an Ihre Privatadresse? Unsere Sonderkonditionen für spezielle Kunden ermöglichen Ihnen dies und noch vie mehr. Kontaktieren Sie uns.</p>
                </div>
        </div>
    </div>
</section>









<?php get_footer(); ?>
