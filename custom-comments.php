<div class="comment-content wow bounceInUp animated">
   <div id="review"></div>
   <div class="comments-form-wrapper clearfix">
      <?php if (comments_open()) { ?>
      <?php
          if (get_comments_number() > 0) { 
      ?>
         <ol class="commentlist">
           <?php wp_list_comments(); ?>
         </ol>
         <article class="blog_entry clearfix"></article>
      <?php
         }

         comment_form($args);
      ?>
      
      <?php } else { ?>
      <h3>Comments are close</h3>
      <?php } ?>
   </div>
   <!-- comments-form-wrapper clearfix -->
</div>