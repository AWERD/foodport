<?php

 /*Define Contants */
define('TMFLAVOURS_FLAVOURS_VERSION', '1.0');  
define('TMFLAVOURS_THEME_PATH', get_template_directory());
define('TMFLAVOURS_THEME_URI', get_template_directory_uri());
define('TMFLAVOURS_THEME_STYLE_URI', get_stylesheet_directory_uri());
define('TMFLAVOURS_THEME_LIB_PATH', get_template_directory() . '/includes/');
define('TMFLAVOURS_THEME_NAME', 'flavours');


/* Include required tgm activation */
require_once (trailingslashit( get_template_directory()). '/includes/tgm_activation/install-required.php');
require_once (trailingslashit( get_template_directory()). '/includes/reduxActivate.php');
if (file_exists(trailingslashit( get_template_directory()). '/includes/reduxConfig.php')) {
    require_once (trailingslashit( get_template_directory()). '/includes/reduxConfig.php');
}

/* Include theme variation functions */
 
/* Include theme variation functions */
 
require_once (trailingslashit( get_template_directory()). '/skins/default/functions.php');
require_once (trailingslashit( get_template_directory()). '/skins/default/custom.php');
   
  
require_once(TMFLAVOURS_THEME_PATH . '/core/tm_framework.php');


if (!isset($content_width)) {
    $content_width = 800;
}

function custom_excerpt_length( $length ) {
  return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

class TmFlavours {
   
  /**
  * Constructor
  */
  function __construct() {
    // Register action/filter callbacks
  
    add_action('after_setup_theme', array($this, 'tmFlavours_flavours_setup'));
    add_action( 'init', array($this, 'tmFlavours_theme'));
    add_action('wp_enqueue_scripts', array($this,'tmFlavours_custom_enqueue_google_font'));
    
    add_action('admin_enqueue_scripts', array($this,'tmFlavours_admin_scripts_styles'));
    add_action('wp_enqueue_scripts', array($this,'tmFlavours_scripts_styles'));
    add_action('wp_head', array($this,'tmFlavours_apple_touch_icon'));
  
    add_action('widgets_init', array($this,'tmFlavours_widgets_init'));
    add_action('wp_head', array($this,'tmFlavours_front_init_js_var'),1);
    add_action('wp_head', array($this,'tmFlavours_enqueue_custom_css'));
    
    add_action('add_meta_boxes', array($this,'tmFlavours_reg_page_meta_box'));
    add_action('save_post',array($this, 'tmFlavours_save_page_layout_meta_box_values')); 
    add_action('add_meta_boxes', array($this,'tmFlavours_reg_post_meta_box'));
    add_action('save_post',array($this, 'tmFlavours_save_post_layout_meta_box_values')); 
 
    }

function tmFlavours_theme() {

global $flavours_Options;

}

  /**
  * Theme setup
  */
  function tmFlavours_flavours_setup() {   
    global $flavours_Options;
     load_theme_textdomain('flavours', get_template_directory() . '/languages');
     load_theme_textdomain('woocommerce', get_template_directory() . '/languages');

      // Add default posts and comments RSS feed links to head.
      add_theme_support('automatic-feed-links');
      add_theme_support('title-tag');
      add_theme_support('post-thumbnails');
      add_image_size('tmFlavours-featured_preview', 55, 55, true);
      add_image_size('tmFlavours-article-home-large',1140, 450, true);
      add_image_size('tmFlavours-article-home-small', 335, 150, true);
      add_image_size('tmFlavours-article-home-medium', 335, 155, true); 
      add_image_size('tmFlavours-product-size-large',214, 214, true);      
          
         
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
    
    add_theme_support( 'post-formats', array(
      'aside','video','audio'
    ) );
    
    // Setup the WordPress core custom background feature.
    $default_color = trim( 'ffffff', '#' );
    $default_text_color = trim( '333333', '#' );
    
    add_theme_support( 'custom-background', apply_filters( 'tmFlavours_custom_background_args', array(
      'default-color'      => $default_color,
      'default-attachment' => 'fixed',
    ) ) );
    
    add_theme_support( 'custom-header', apply_filters( 'tmFlavours_custom_header_args', array(
      'default-text-color'     => $default_text_color,
      'width'                  => 1170,
      'height'                 => 450,
      
    ) ) );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style('css/editor-style.css' );
    
    /*
    * Edge WooCommerce Declaration: WooCommerce Support and settings
    */    
    
      if (class_exists('WooCommerce')) {
        add_theme_support('woocommerce');
        require_once(TMFLAVOURS_THEME_PATH. '/woo_function.php');
        // Disable WooCommerce Default CSS if set
        if (!empty($flavours_Options['woocommerce_disable_woo_css'])) {
          add_filter('woocommerce_enqueue_styles', '__return_false');
          wp_enqueue_style('woocommerce_enqueue_styles', get_template_directory_uri() . '/woocommerce.css');
        }
      }
 
    // Register navigation menus
    
    register_nav_menus(
      array(
      'toplinks' => esc_html__( 'Top menu', 'flavours' ),
       'main_menu' => esc_html__( 'Main menu', 'flavours' )
      ));
    
  }
    

function tmFlavours_fonts_url() {
  $fonts_url = '';
  $fonts     = array();
  $subsets   = 'latin,latin-ext';
 
   if ( 'off' !== _x( 'on', 'Source Sans: on or off', 'flavours' ) ) {
       $fonts[]='Source Sans Pro:200,200italic,300,300italic,400,400italic,600,600italic,700,700italic,900,900italic';
    }
  

 
    if ( 'off' !== _x( 'on', 'Montserrat: on or off', 'flavours' ) ) {
       $fonts[]='Montserrat:400,700';
    }
    
 
    if ( 'off' !== _x( 'on', 'Roboto: on or off', 'flavours' ) ) {
        $fonts[]='Roboto:400,500,300,700,900';
    }
    
 
    if ( 'off' !== _x( 'on', 'Raleway: on or off', 'flavours' ) ) {
         $fonts[]='Raleway:400,100,200,300,600,500,700,800,900';
    }

    if ( $fonts ) {
    $fonts_url = add_query_arg( array(
      'family' => urlencode( implode( '|', $fonts ) ),
      'subset' => urlencode( $subsets ),
    ), 'https://fonts.googleapis.com/css' );
  }
    return $fonts_url;
}







/*
Enqueue scripts and styles.
*/
function tmFlavours_custom_enqueue_google_font() {

  wp_enqueue_style( 'tmFlavours-Fonts', $this->tmFlavours_fonts_url() , array(), '1.0.0' );
}


  function tmFlavours_admin_scripts_styles()
  {  
      wp_enqueue_script('tmFlavours-admin', TMFLAVOURS_THEME_URI . '/js/admin_menu.js', array(), '', true);
  }

function tmFlavours_scripts_styles()
{
    global $flavours_Options;
    /*JavaScript for threaded Comments when needed*/
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


//theme css

  wp_enqueue_style('bootstrap.min-css', TMFLAVOURS_THEME_URI . '/css/bootstrap.min.css', array(), ''); 
  wp_enqueue_style('font-awesome.min', TMFLAVOURS_THEME_URI . '/css/font-awesome.min.css', array(), '');   
  wp_enqueue_style('simple-line-icons', TMFLAVOURS_THEME_URI . '/css/simple-line-icons.css', array(), ''); 
  wp_enqueue_style('owl.carousel', TMFLAVOURS_THEME_URI . '/css/owl.carousel.css', array(), '');
  wp_enqueue_style('owl.theme', TMFLAVOURS_THEME_URI . '/css/owl.theme.css', array(), ''); 
  wp_enqueue_style('jquery.bxslider', TMFLAVOURS_THEME_URI . '/css/jquery.bxslider.css', array(), '');
  wp_enqueue_style( 'jquery.mobile-menu', TMFLAVOURS_THEME_URI . '/css/jquery.mobile-menu.css', array(), '');
  wp_enqueue_style('revslider', TMFLAVOURS_THEME_URI . '/css/revslider.css', array(), '');
  wp_enqueue_style('stylesheet', TMFLAVOURS_THEME_URI . '/css/stylesheet.css', array(), '');
  wp_enqueue_style( 'style', TMFLAVOURS_THEME_URI . '/css/style.css', array(), '');  
  wp_enqueue_style('flexslider', TMFLAVOURS_THEME_URI . '/css/flexslider.css', array(), ''); 
  wp_enqueue_style('thmsoftblog', TMFLAVOURS_THEME_URI . '/css/thmsoftblog.css', array(), '');
  wp_enqueue_style('custom', TMFLAVOURS_THEME_URI . '/css/custom.css', array(), '');
     
    
 //theme js

  wp_enqueue_script('bootstrap.min', TMFLAVOURS_THEME_URI . '/js/bootstrap.min.js', array('jquery'), '', true);
  wp_register_script('admin_menu', TMFLAVOURS_THEME_URI . '/js/admin_menu.js', array('jquery'), '', true);
  wp_register_script('cloud-zoom', TMFLAVOURS_THEME_URI . '/js/cloud-zoom.js', array('jquery'), '', true);
  wp_register_script('common', TMFLAVOURS_THEME_URI . '/js/common.js', array('jquery'), '', true);
  wp_register_script('common1', TMFLAVOURS_THEME_URI . '/js/common1.js', array('jquery'), '', true);
  wp_register_script('countdown', TMFLAVOURS_THEME_URI . '/js/countdown.js', array('jquery'), '', true);
  wp_register_script('fastclick', TMFLAVOURS_THEME_URI . '/js/fastclick.js', array('jquery'), '', true);
  wp_register_script('jquery.bxslider.min', TMFLAVOURS_THEME_URI . '/js/jquery.bxslider.min.js', array('jquery'), '', true);
  wp_register_script('jquery.flexslider', TMFLAVOURS_THEME_URI . '/js/jquery.flexslider.js', array('jquery'), '', true);
  wp_register_script('jquery.countdown.min', TMFLAVOURS_THEME_URI . '/js/jquery.countdown.min.js', array('jquery'), '', true);
  wp_register_script('jquery.mobile-menu', TMFLAVOURS_THEME_URI . '/js/jquery.mobile-menu.min.js', array('jquery'), '', true);
  wp_register_script('owl.carousel.min', TMFLAVOURS_THEME_URI . '/js/owl.carousel.min.js', array('jquery'), '', true);
  wp_register_script('parallax', TMFLAVOURS_THEME_URI . '/js/parallax.js', array('jquery'), '', true);
  wp_register_script('revolution.extension', TMFLAVOURS_THEME_URI . '/js/revolution.extension.js', array('jquery'), '', true);
  wp_register_script('revolution-slider', TMFLAVOURS_THEME_URI . '/js/revolution-slider.js', array('jquery'), '', true);
  wp_register_script('revslider', TMFLAVOURS_THEME_URI . '/js/revslider.js', array('jquery'), '', true);
  wp_register_script('tm_menu', TMFLAVOURS_THEME_URI . '/js/tm_menu.js', array('jquery'), '', true);
  wp_register_script('viewport-units-buggyfill.hacks', TMFLAVOURS_THEME_URI . '/js/viewport-units-buggyfill.hacks.js', array('jquery'), '', true);
  wp_register_script('jquery.magnific-popup.min', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', false, '', true);
  wp_register_script('moment', get_template_directory_uri() . '/js/moment.js', false, '', true);
  wp_register_script('bootstrap-datetimepicker.min', get_template_directory_uri() . '/js/bootstrap-datetimepicker.min.js', false, '', true);
  wp_register_script('viewport-units-buggyfill', TMFLAVOURS_THEME_URI . '/js/viewport-units-buggyfill.js', array('jquery'), '', true);
  wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', false, '', true);


            wp_localize_script( 'tmFlavours-theme', 'js_flavours_vars', array(
            'ajax_url' => esc_js(admin_url( 'admin-ajax.php' )),
            'container_width' => 1160,
            'grid_layout_width' => 20           
        ) );
           
}
 
  function tmFlavours_apple_touch_icon()
  {
    printf(
      '<link rel="apple-touch-icon" href="%s" />',
      esc_url(TMFLAVOURS_THEME_URI). '/images/apple-touch-icon.png'
    );
    printf(
      '<link rel="apple-touch-icon" href="%s" />',
      esc_url(TMFLAVOURS_THEME_URI). '/images/apple-touch-icon57x57.png'
    );
    printf(
      '<link rel="apple-touch-icon" href="%s" />',
       esc_url(TMFLAVOURS_THEME_URI). '/images/apple-touch-icon72x72.png'
    );
    printf(
      '<link rel="apple-touch-icon" href="%s" />',
      esc_url(TMFLAVOURS_THEME_URI). '/images/apple-touch-icon114x114.png'
    );
    printf(
      '<link rel="apple-touch-icon" href="%s" />',
      esc_url(TMFLAVOURS_THEME_URI). '/images/apple-touch-icon144x144.png'
    );
  }
  //register sidebar widget
  function tmFlavours_widgets_init()
  {
      register_sidebar(array(
      'name' => esc_html__('Blog Sidebar', 'flavours'),
      'id' => 'sidebar-blog',
      'description' => esc_html__('Sidebar that appears on the right of Blog and Search page.', 'flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="block-title">',
      'after_title' => '</h3>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Shop Sidebar','flavours'),
      'id' => 'sidebar-shop',
      'description' => esc_html__('Main sidebar that appears on the left.', 'flavours'),
      'before_widget' => '<div id="%1$s" class="block %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Left', 'flavours'),
      'id' => 'sidebar-content-left',
      'description' => esc_html__('Additional sidebar that appears on the left.','flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Right', 'flavours'),
      'id' => 'sidebar-content-right',
      'description' => esc_html__('Additional sidebar that appears on the right.', 'flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
   
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 1','flavours'),
      'id' => 'footer-sidebar-1',
      'description' => esc_html__('Appears in the footer section of the site.','flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 2', 'flavours'),
      'id' => 'footer-sidebar-2',
      'description' => esc_html__('Appears in the footer section of the site.', 'flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 3', 'flavours'),
      'id' => 'footer-sidebar-3',
      'description' => esc_html__('Appears in the footer section of the site.','flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 4', 'flavours'),
      'id' => 'footer-sidebar-4',
      'description' => esc_html__('Appears in the footer section of the site.', 'flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 5', 'flavours'),
      'id' => 'footer-sidebar-5',
      'description' => esc_html__('Appears in the footer section of the site.', 'flavours'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));

  }


  function tmFlavours_front_init_js_var()
  {
    global $yith_wcwl, $post;
    ?>
  <script type="text/javascript">
      var TM_PRODUCT_PAGE = false;
      var THEMEURL = '<?php echo esc_url(TMFLAVOURS_THEME_URI) ?>';
      var IMAGEURL = '<?php echo esc_url(TMFLAVOURS_THEME_URI) ?>/images';
      var CSSURL = '<?php echo esc_url(TMFLAVOURS_THEME_URI) ?>/css';
      <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) { ?>
      var TM_ADD_TO_WISHLIST_SUCCESS_TEXT = '<?php printf(preg_replace_callback('/(\r|\n|\t)+/',  create_function('$match', 'return "";'),htmlspecialchars_decode('Product successfully added to wishlist. <a href="%s">Browse Wishlist</a>')), esc_url($yith_wcwl->get_wishlist_url())) ?>';

      var TM_ADD_TO_WISHLIST_EXISTS_TEXT = '<?php printf(preg_replace_callback('/(\r|\n|\t)+/',  create_function('$match', 'return "";'),htmlspecialchars_decode('The product is already in the wishlist! <a href="%s">Browse Wishlist</a>')), esc_url($yith_wcwl->get_wishlist_url()) )?>';
      <?php } ?>
      <?php if(is_singular('product')){?>
      TM_PRODUCT_PAGE = true;
      <?php }?>
    </script>
  <?php
  }

  function tmFlavours_reg_page_meta_box() {
    $screens = array('page');

    foreach ($screens as $screen) {        
      add_meta_box(
          'tmFlavours_page_layout_meta_box', esc_html__('Page Layout', 'flavours'), 
          array($this, 'tmFlavours_page_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function tmFlavours_page_layout_meta_box_cb($post) {

    $saved_page_layout = get_post_meta($post->ID, 'tmFlavours_page_layout', true);
    
    $show_breadcrumb = get_post_meta($post->ID, 'tmFlavours_show_breadcrumb', true);
    
   if(empty($saved_page_layout)) {
      $saved_page_layout = 3;
    }
    $page_layouts = array(
      1 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-1.png',
      2 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-2.png',
      3 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-3.png',
      4 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-4.png',
    );  
    ?>
  <style type="text/css">
        input.of-radio-img-radio{display: none;}
        .tile_img_wrap{
          display: block;                
        }
        .tile_img_wrap > span > img{
          float: left;
          margin:0 5px 10px 0;
        }
        .tile_img_wrap > span > img:hover{
          cursor: pointer;
        }            
        .tile_img_wrap img.of-radio-img-selected{
          border: 3px solid #CCCCCC;
        }
         #tmFlavours_page_layout_meta_box h2 {
    margin-top: 20px;
    font-size: 1.5em;
    
     }
        #tmFlavours_page_layout_meta_box .inside h2 {
    margin-top: 20px;
    font-size: 1.5em;
    margin-bottom: 15px;
    padding: 0 0 3px;
    clear: left;
}
        
      </style>
  <?php
    echo "<input type='hidden' name='tmFlavours_page_layout_verifier' value='".wp_create_nonce('tmFlavours_7a81jjde')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($page_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_page_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="tmFlavours_page_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
    ?>
  <script type="text/javascript">
      jQuery(function($){            
        $(document.body).on('click','.of-radio-img-img',function(){
          $(this).parents('.tile_img_wrap').find('.of-radio-img-img').removeClass('of-radio-img-selected');
          $(this).parent().find('.of-radio-img-radio').attr('checked','checked');
          $(this).addClass('of-radio-img-selected');
        });            
    });
      
      </script>

  <h2><?php esc_attr_e('Show breadcrumb', 'flavours'); ?></h2>
  <p>
    <input type="radio" name="tmFlavours_show_breadcrumb" value="1" <?php echo "checked='checked'"; ?> />
    <label><?php esc_attr_e('Yes','flavours'); ?></label>
    &nbsp;
    <input type="radio" name="tmFlavours_show_breadcrumb" value="0"  <?php if($show_breadcrumb === '0'){ echo "checked='checked'"; } ?>/>
    <label><?php esc_attr_e('No', 'flavours'); ?></label>
  </p>
  <?php
  }

  function tmFlavours_save_page_layout_meta_box_values($post_id){
    if (!isset($_POST['tmFlavours_page_layout_verifier']) 
        || !wp_verify_nonce($_POST['tmFlavours_page_layout_verifier'], 'tmFlavours_7a81jjde') 
        || !isset($_POST['tmFlavours_page_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'tmFlavours_page_layout',sanitize_text_field( $_POST['tmFlavours_page_layout']),true) or 
    update_post_meta($post_id,'tmFlavours_page_layout',sanitize_text_field( $_POST['tmFlavours_page_layout']));
    
    add_post_meta($post_id,'tmFlavours_show_breadcrumb',sanitize_text_field( $_POST['tmFlavours_show_breadcrumb']),true) or 
    update_post_meta($post_id,'tmFlavours_show_breadcrumb',sanitize_text_field( $_POST['tmFlavours_show_breadcrumb']));  
  }


  /*Register Post Meta Boxes for Blog Post Layouts*/

    function tmFlavours_reg_post_meta_box() {
    $screens = array('post');

    foreach ($screens as $screen) {        
      add_meta_box(
          'tmFlavours_post_layout_meta_box', esc_html__('Post Layout', 'flavours'), 
          array($this, 'tmFlavours_post_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function tmFlavours_post_layout_meta_box_cb($post) {

    $saved_post_layout = get_post_meta($post->ID, 'tmFlavours_post_layout', true);         
    if(empty($saved_post_layout))
    {
      $saved_post_layout = 2;
    }
    
    $post_layouts = array(
      1 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-1.png',
      2 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-2.png',
      3 => esc_url(TMFLAVOURS_THEME_URI).'/images/tmFlavours_col/category-layout-3.png',
      
    );  
    ?>
  <style type="text/css">
        input.of-radio-img-radio{display: none;}
        .tile_img_wrap{
          display: block;                
        }
        .tile_img_wrap > span > img{
          float: left;
          margin:0 5px 10px 0;
        }
        .tile_img_wrap > span > img:hover{
          cursor: pointer;
        }            
        .tile_img_wrap img.of-radio-img-selected{
          border: 3px solid #CCCCCC;
        }
        .postbox-container .inside .tile_img_wrap
        {
          height:70px;
        }
        
      </style>
  <?php
    echo "<input type='hidden' name='tmFlavours_post_layout_verifier' value='".wp_create_nonce('tmFlavours_7a81jjde1')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($post_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_post_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="tmFlavours_post_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
    ?>
  <script type="text/javascript">
      jQuery(function($){            
        $(document.body).on('click','.of-radio-img-img',function(){
          $(this).parents('.tile_img_wrap').find('.of-radio-img-img').removeClass('of-radio-img-selected');
          $(this).parent().find('.of-radio-img-radio').attr('checked','checked');
          $(this).addClass('of-radio-img-selected');
        });            
    });
      
      </script>

  
  <?php
  }

  function tmFlavours_save_post_layout_meta_box_values($post_id){
    if (!isset($_POST['tmFlavours_post_layout_verifier']) 
        || !wp_verify_nonce($_POST['tmFlavours_post_layout_verifier'], 'tmFlavours_7a81jjde1') 
        || !isset($_POST['tmFlavours_post_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'tmFlavours_post_layout',sanitize_text_field($_POST['tmFlavours_post_layout']),true) or 
    update_post_meta($post_id,'tmFlavours_post_layout',sanitize_text_field($_POST['tmFlavours_post_layout']));
    
    
  }

  //custom functions 

  //search form code
  function tmFlavours_custom_search_form()
  { global $flavours_Options;
  ?>
 
<form name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
         <input type="text" name="s" class="tm-search" maxlength="70" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e('Search', 'flavours'); ?>">
  
    
     <?php if (class_exists('WooCommerce')) : ?>    
      <input type="hidden" value="product" name="post_type">
    <?php endif; ?>
    <button type="submit" class="search-btn-bg search-icon"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
  </form>

  <?php
  }



// page title code
function tmFlavours_page_title() {

    global  $post, $wp_query, $author,$flavours_Options;

    $home = esc_html__('Home', 'flavours');

  
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {

        if ( is_home() ) {
           echo htmlspecialchars_decode(single_post_title('', false));

        } else if ( is_category() ) {

            echo esc_html(single_cat_title( '', false ));

        } elseif ( is_tax() ) {

            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            echo htmlspecialchars_decode(esc_html( $current_term->name ));

        }  elseif ( is_day() ) {

            printf( esc_html__( 'Daily Archives: %s', 'flavours' ), get_the_date() );

        } elseif ( is_month() ) {

            printf( esc_html__( 'Monthly Archives: %s', 'flavours' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'flavours' ) ) );

        } elseif ( is_year() ) {

            printf( esc_html__( 'Yearly Archives: %s', 'flavours' ), get_the_date( _x( 'Y', 'yearly archives date format', 'flavours' ) ) );

        }   else if ( is_post_type_archive() ) {
            sprintf( esc_html__( 'Archives: %s', 'flavours' ), post_type_archive_title( '', false ) );
        } elseif ( is_single() && ! is_attachment() ) {
        
                echo esc_html(get_the_title());

            

        } elseif ( is_404() ) {

            echo esc_html__( 'Error 404', 'flavours' );

        } elseif ( is_attachment() ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && !$post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && $post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode(esc_html__( 'Search results for &ldquo;', 'flavours' ) . get_search_query() . '&rdquo;');

        } elseif ( is_tag() ) {

            echo htmlspecialchars_decode(esc_html__( 'Posts tagged &ldquo;', 'flavours' ) . single_tag_title('', false) . '&rdquo;');

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode(esc_html__( 'Author:', 'flavours' ) . ' ' . $userdata->display_name);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($post_type->labels->singular_name);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode( ' (' . esc_html__( 'Page', 'flavours' ) . ' ' . get_query_var( 'paged' ) . ')');
        }
    } else {
        if ( is_home() && !is_front_page() ) {
            if ( ! empty( $home ) ) {               
                  echo htmlspecialchars_decode(single_post_title('', false));
            }
        }
    }
}

// page breadcrumbs code
function tmFlavours_breadcrumbs() {
    global $post, $flavours_Options,$wp_query, $author;

    $delimiter = ' / ';
    $before = '<li>';
    $after = '</li>';
    $home = esc_html__('Home', 'flavours');

  
  // breadcrumb code
   
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {
        echo '<ul class="breadcrumb">';

        if ( ! empty( $home ) ) {
            echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url() ) . '">' . $home . '</a>' . $delimiter . $after);
        }

        if ( is_home() ) {

            echo htmlspecialchars_decode($before . single_post_title('', false) . $after);

         }      
         else if ( is_category() ) {

            if ( get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            $cat_obj = $wp_query->get_queried_object();
            if ($cat_obj) {
                $this_category = get_category( $cat_obj->term_id );
                if ( 0 != $this_category->parent ) {
                    $parent_category = get_category( $this_category->parent );
                    if ( ( $parents = get_category_parents( $parent_category, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                        echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
                    }
                }
                echo htmlspecialchars_decode($before . single_cat_title( '', false ) . $after);
            }

        } 
        elseif ( is_tax()) {      
                    
            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            $ancestors = array_reverse( get_ancestors( $current_term->term_id, get_query_var( 'taxonomy' ) ) );

            foreach ( $ancestors as $ancestor ) {
                $ancestor = get_term( $ancestor, get_query_var( 'taxonomy' ) );

                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_term_link( $ancestor->slug, get_query_var( 'taxonomy' ) )) . '">' . esc_html( $ancestor->name ) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before . esc_html( $current_term->name ) . $after);

        } 
       
        elseif ( is_day() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . get_the_time('d') . $after);

        } elseif ( is_month() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . get_the_time('F') . $after);

        } elseif ( is_year() ) {

            echo htmlspecialchars_decode($before . get_the_time('Y') . $after);

        } elseif ( is_single() && ! is_attachment() ) {

         
            if ( 'post' != get_post_type() ) {
                $post_type = get_post_type_object( get_post_type() );
                $slug = $post_type->rewrite;
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_post_type_archive_link( get_post_type() )) . '">' . esc_html($post_type->labels->singular_name) . '</a>' . $delimiter . $after);
                echo htmlspecialchars_decode($before . get_the_title() . $after);

            } else {

                if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                    echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
                }

                $cat = current( get_the_category() );
              if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                $getitle=get_the_title();
                  if(empty($getitle))
                  {
                    $newdelimiter ='';
                  }
                  else
                  {
                     $newdelimiter=$delimiter;
                  }
                    echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $newdelimiter . $after);
                }
                echo htmlspecialchars_decode($before . get_the_title() . $after);

            }

        } elseif ( is_404() ) {

            echo htmlspecialchars_decode($before . esc_html__( 'Error 404', 'flavours' ) . $after);

        } elseif ( is_attachment() ) {

            $parent = get_post( $post->post_parent );
            $cat = get_the_category( $parent->ID );
            $cat = $cat[0];
            if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
            }
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( $parent )) . '">' . esc_html($parent->post_title) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . get_the_title() . $after);

        } elseif ( is_page() && !$post->post_parent ) {

            echo htmlspecialchars_decode($before . get_the_title() . $after);

        } elseif ( is_page() && $post->post_parent ) {

            $parent_id  = $post->post_parent;
            $breadcrumbs = array();

            while ( $parent_id ) {
                $page = get_post( $parent_id );
                $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title( $page->ID )) . '</a>';
                $parent_id  = $page->post_parent;
            }

            $breadcrumbs = array_reverse( $breadcrumbs );

            foreach ( $breadcrumbs as $crumb ) {
                echo htmlspecialchars_decode($before . $crumb . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before . get_the_title() . $after);

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode($before . esc_html__( 'Search results for &ldquo;', 'flavours' ) . get_search_query() . '&rdquo;' . $after);

        } elseif ( is_tag() ) {

            if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before . esc_html__( 'Posts tagged &ldquo;', 'flavours' ) . single_tag_title('', false) . '&rdquo;' . $after);

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode($before . esc_html__( 'Author:', 'flavours' ) . ' ' . $userdata->display_name . $after);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($before . $post_type->labels->singular_name . $after);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode($before . '&nbsp;(' . esc_html__( 'Page', 'flavours' ) . ' ' . get_query_var( 'paged' ) . ')' . $after);
        }

        echo '</ul>';
    } else { 
        if ( is_home() && !is_front_page() ) {
            echo '<ul class="breadcrumb">';

            if ( ! empty( $home ) ) {
                echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url()) . '">' . $home . '</a>' . $delimiter . $after);

               
                echo htmlspecialchars_decode($before . single_post_title('', false) . $after);
            }

            echo '</ul>';
        }
    }
}
  
  // breadcrumb
  function tmFlavours_page_breadcrumb()
  {
    /* === OPTIONS === */

    $text['home'] = 'Home'; // text for the 'Home' link
    $text['category'] = 'Archive by Category "%s"'; // text for a category page
    $text['tax'] = 'Archive for "%s"'; // text for a taxonomy page
    $text['search'] = 'Search Results for "%s" Query'; // text for a search results page
    $text['tag'] = 'Posts Tagged "%s"'; // text for a tag page
    $text['author'] = 'Articles Posted by %s'; // text for an author page
    $text['404'] = 'Error 404'; // text for the 404 page

    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $showOnHome = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = ' &mdash;&rsaquo; '; // delimiter between crumbs
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */

    global $post;

    $homeLink = home_url() . '/';
    $linkBefore = '<span typeof="v:Breadcrumb">';
    $linkAfter = '</span>';
    $linkAttr = ' rel="v:url" property="v:title"';
    $link = $linkBefore . '<a' . htmlspecialchars_decode($linkAttr) . ' href="%1$s">%2$s</a>' . htmlspecialchars_decode($linkAfter);

    if (is_home() || is_front_page()) {

      if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . esc_url($homeLink) . '">' . esc_html($text['home']) . '</a></div>';
     
    } else {
     
      echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, esc_url($homeLink), esc_html($text['home']) ). $delimiter;


      if (is_category()) {
        $thisCat = get_category(get_query_var('cat'), false);
        if ($thisCat->parent != 0) {
          $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
          $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
          $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
          echo htmlspecialchars_decode($cats);
        }
        echo htmlspecialchars_decode($before . sprintf($text['category'], single_cat_title('', false)) . $after);

      } elseif (is_tax()) {
        $thisCat = get_category(get_query_var('cat'), false);
        if ($thisCat->parent != 0) {
          $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
          $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
          $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
          echo htmlspecialchars_decode($cats);
        }
        echo htmlspecialchars_decode($before . sprintf(esc_html($text['tax']), single_cat_title('', false)) . $after);

      } elseif (is_search()) {
        echo htmlspecialchars_decode($before . sprintf(esc_html($text['search']), get_search_query()) . $after);

      } elseif (is_day()) {
        echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
        echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
        echo htmlspecialchars_decode($before . get_the_time('d') . $after);

      } elseif (is_month()) {
        echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
        echo htmlspecialchars_decode($before . get_the_time('F') . $after);

      } elseif (is_year()) {
        echo htmlspecialchars_decode($before . get_the_time('Y') . $after);

      } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
          $post_type = get_post_type_object(get_post_type());
          $slug = $post_type->rewrite;
          printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
          if ($showCurrent == 1) echo htmlspecialchars_decode($delimiter . $before . get_the_title() . $after);
        } else {
          $cat = get_the_category();
          $cat = $cat[0];
          $cats = get_category_parents($cat, TRUE, $delimiter);
          if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
          $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
          $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
          echo htmlspecialchars_decode($cats);
          if ($showCurrent == 1) echo htmlspecialchars_decode($before . get_the_title() . $after);
        }

      } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
        $post_type = get_post_type_object(get_post_type());
        echo htmlspecialchars_decode($before . $post_type->labels->singular_name . $after);

      } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $delimiter);
        $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
        echo htmlspecialchars_decode($cats);
        printf($link, get_permalink($parent), $parent->post_title);
        if ($showCurrent == 1) echo htmlspecialchars_decode($delimiter . $before . get_the_title() . $after);

      } elseif (is_page() && !$post->post_parent) {
        if ($showCurrent == 1) echo htmlspecialchars_decode($before . get_the_title() . $after);

      } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo htmlspecialchars_decode($breadcrumbs[$i]);
          if ($i != count($breadcrumbs) - 1) echo htmlspecialchars_decode($delimiter);
        }
        if ($showCurrent == 1) echo htmlspecialchars_decode($delimiter . $before . get_the_title() . $after);

      } elseif (is_tag()) {
        echo htmlspecialchars_decode($before . sprintf($text['tag'], single_tag_title('', false)) . $after);

      } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo htmlspecialchars_decode($before . sprintf($text['author'], $userdata->display_name) . $after);

      } elseif (is_404()) {
        echo htmlspecialchars_decode($before . $text['404'] . $after);
      }

      if (get_query_var('paged')) {
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ' (';
         esc_attr_e('Page', 'flavours') . ' ' . get_query_var('paged');
        if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ')';
      }

      echo '</div>';

    }
  }

  // mini cart
  function tmFlavours_mini_cart()
{
    global $woocommerce;
    ?>

<div class="mini-cart">
   <div  class="basket">
      <a href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>">
         
        <span><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> </span>
      </a>
   </div>

      <div class="top-cart-content arrow_box">
         <div class="block-subtitle">
            <div class="top-subtotal"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> <?php  esc_attr_e('items','flavours'); ?>, <span class="price"><?php echo htmlspecialchars_decode(WC()->cart->get_cart_subtotal()); ?></span> </div>
         </div>
         <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>
         <ul id="cart-sidebar" class="mini-products-list">
            <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
            <?php
               $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
               $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
               
               if ($_product && $_product->exists() && $cart_item['quantity'] > 0
                   && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)
               ) :
               
                   $product_name = apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
                   $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(array(60, 60)), $cart_item, $cart_item_key);
                   $product_price = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                   $cnt = sizeof(WC()->cart->get_cart());
                   $rowstatus = $cnt % 2 ? 'odd' : 'even';
                   ?>
            <li class="item <?php echo esc_html($rowstatus); ?> <?php if ($cnt - 1 == $i) { ?>last<?php } ?>">
              <div class="item-inner">
               <a class="product-image"
                  href="<?php echo esc_url($_product->get_permalink($cart_item)); ?>"  title="<?php echo esc_html($product_name); ?>"> <?php echo str_replace(array('http:', 'https:'), '', htmlspecialchars_decode($thumbnail)); ?> </a>
             

                  <div class="product-details">
                       <div class="access">
                     <a href="<?php echo esc_url(WC()->cart->get_remove_url($cart_item_key)); ?>"
                        title="<?php esc_attr_e('Remove This Item','flavours') ;?>" onClick="" class="btn-remove1"><?php esc_attr_e('Remove','flavours') ;?></a> <a class="btn-edit" title="<?php esc_attr_e('Edit item','flavours') ;?>"
                        href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>"><i
                        class="icon-pencil"></i><span
                        class="hidden"><?php esc_attr_e('Edit item','flavours') ;?></span></a>
                         </div>
                      <strong><?php echo esc_html($cart_item['quantity']); ?>
                  </strong> x <span class="price"><?php echo htmlspecialchars_decode($product_price); ?></span>
                     <p class="product-name"><a href="<?php echo esc_url($_product->get_permalink($cart_item)); ?>"
                        title="<?php echo esc_html($product_name); ?>"><?php echo esc_html($product_name); ?></a> </p>
                  </div>
                  <?php echo htmlspecialchars_decode(WC()->cart->get_item_data($cart_item)); ?>
                     </div>
              
            </li>
            <?php endif; ?>
            <?php $i++; endforeach; ?>
         </ul>    
         <div class="actions">
            <button class="btn-checkout" type="button"
               onClick="window.location.assign('<?php echo esc_js(WC()->cart->get_checkout_url()); ?>')"><span><?php esc_attr_e('Checkout','flavours') ;?></span> </button>          
         </div>
         <?php else:?>
         <p class="a-center noitem">
            <?php esc_attr_e('Sorry, nothing in cart.', 'flavours');?>
         </p>
         <?php endif; ?>
      </div>
   </div>

<?php
}
 
  //company logo
  function tmFlavours_footer_brand_logo()
  {
    global $flavours_Options;
    if (isset($flavours_Options['enable_brand_logo']) && $flavours_Options['enable_brand_logo'] && !empty($flavours_Options['all-company-logos'])) : ?>
    <div class="logo-brand col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="new_title">
      <h2>Brand Logo</h2>
    </div>
    <div class="slider-items-products">
      <div id="brand-slider" class="product-flexslider hidden-buttons">
      <div class="slider-items slider-width-col6"> 
        <?php $i=0;
        foreach ($flavours_Options['all-company-logos'] as $_logo) : ?>
        <!-- Item -->
        <?php if($i==0) { ?> 
         <div class="item"> 
        <?php } ?>         
          <a href="<?php echo esc_url($_logo['url']); ?>" target="_blank"> <img
                        src="<?php echo esc_url($_logo['image']); ?>" 
                        alt="<?php echo esc_attr($_logo['title']); ?>"> </a>
        <?php if($i==1) { $i=0; ?>  
        </div>               
        <?php } else { $i++; } ?>
        <!-- End Item -->
        <?php endforeach; ?>
      </div>
      </div>
    </div>
    </div>
  

  <?php endif;
  }

 
  //social links
  function tmFlavours_social_media_links()
  {
    global $flavours_Options;

    if (isset($flavours_Options
  ['social_facebook']) && !empty($flavours_Options['social_facebook'])) {
      echo "<li class=\"fb pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_facebook']) ."'></a></li>";
    }

    if (isset($flavours_Options['social_twitter']) && !empty($flavours_Options['social_twitter'])) {
      echo "<li class=\"tw pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_twitter']) ."'></a></li>";
    }

    if (isset($flavours_Options['social_googlep']) && !empty($flavours_Options['social_googlep'])) {
      echo "<li class=\"googleplus pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_googlep'])."'></a></li>";
    }

    if (isset($flavours_Options['social_rss']) && !empty($flavours_Options['social_rss'])) {
      echo "<li class=\"rss pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_rss'])."'></a></li>";
    }

    if (isset($flavours_Options['social_pinterest']) && !empty($flavours_Options['social_pinterest'])) {
      echo "<li class=\"pintrest pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_pinterest'])."'></a></li>";
    }

    if (isset($flavours_Options['social_linkedin']) && !empty($flavours_Options['social_linkedin'])) {
      echo "<li class=\"linkedin pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_linkedin'])."'></a></li>";
    }

    if (isset($flavours_Options['social_youtube']) && !empty($flavours_Options['social_youtube'])) {
      echo "<li class=\"youtube pull-left\"><a target=\"_blank\" href='".  esc_url($flavours_Options['social_youtube'])."'></a></li>";
    }
  }


  // bottom cpyright text 
  function tmFlavours_footer_text()
  {
    global $flavours_Options;
    if (isset($flavours_Options['bottom-footer-text']) && !empty($flavours_Options['bottom-footer-text'])) {
      echo htmlspecialchars_decode ($flavours_Options['bottom-footer-text']);
    }
  }


  function tmFlavours_getPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0 Aufrufe";
    }
    return $count . ' Aufrufe';
  }

  function tmFlavours_setPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
    } else {
      $count++;
      update_post_meta($postID, $count_key, $count);
    }
  }


  function tmFlavours_is_blog() {
    global  $post;
    $posttype = get_post_type($post );
    return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
  }
  //add to cart function
function tmFlavours_woocommerce_product_add_to_cart_text() {
    global $product;
    $product_type = $product->product_type;
    $product_id=$product->id;
    if($product->is_in_stock())
    {
    switch ( $product_type ) {
    case 'external':
    ?>
    <button class="button btn-cart" title='<?php esc_attr_e("Buy product",'flavours'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span> <?php echo htmlspecialchars_decode($product->get_price_html()); ?></span>
    </button>
    <?php
       break;
       case 'grouped':
        ?>
    <button class="button btn-cart" title='<?php esc_attr_e("View products",'flavours'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php esc_attr_e('View products', 'flavours'); ?> </span>
    </button>
    <?php
       break;
       case 'simple':
        ?>
    <?php tmFlavours_simple_product_link(); ?>
    <?php
       break;
       case 'variable':
        ?>
    <button class="button btn-cart"  title='<?php esc_attr_e("Select options",'flavours'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span>
    <?php esc_attr_e('Select options', 'flavours'); ?>
    </span> 
    </button>
    <?php
       break;
       default:
        ?>
    <?php tmFlavours_bundel_link(); ?>
    <?php
       break;
       
       }
       }
       else
       {
       ?>
    <button type='button' class="button btn-cart" title='<?php esc_attr_e('Out of stock', 'flavours'); ?> '
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'
       class='button btn-cart'>
    <span> <?php esc_attr_e('Out of stock', 'flavours'); ?> </span>
    </button>
  <div style="height: 36.25px;width:1px;opacity:0;"></div>
    <?php
    }
}
 
 // comment display 
  function tmFlavours_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>

  <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
    <div class="comment-body">
      <div class="img-thumbnail">
        <?php echo get_avatar($comment, 80); ?>
      </div>
      <div class="comment-block">
        <div class="comment-arrow"></div>
        <span class="comment-by">
          <strong><?php echo get_comment_author_link() ?></strong>
          <span class="pt-right">
            <span> <?php edit_comment_link('<i class="fa fa-pencil"></i> ' . esc_html__('Edit', 'flavours'),'  ','') ?></span>
            <span> <?php comment_reply_link(array_merge( $args, array('reply_text' => '<i class="fa fa-reply"></i> ' . esc_html__('Reply', 'flavours'), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
          </span>
        </span>
        <div>
          <?php if ($comment->comment_approved == '0') : ?>
            <em><?php echo esc_html__('Your comment is awaiting moderation.', 'flavours') ?></em>
            <br />
          <?php endif; ?>
          <?php comment_text() ?>
        </div>
        <span class="date pt-right"><?php printf(esc_html__('%1$s at %2$s', 'flavours'), get_comment_date(),  get_comment_time()) ?></span>
      </div>
    </div>
  </li>
  <?php }

  //css manage by admin
  function tmFlavours_enqueue_custom_css() {
    global $flavours_Options;

    ?>
    <style rel="stylesheet" property="stylesheet" type="text/css">
      <?php if(isset($flavours_Options['opt-color-rgba']) &&  !empty($flavours_Options['opt-color-rgba'])) {
      ?>
      .tm-main-menu {
        background-color: <?php echo esc_html($flavours_Options['opt-color-rgba'])." !important";
      ?>
      }
      <?php
      }
      ?>
       
      
      <?php if(isset($flavours_Options['footer_color_scheme']) && $flavours_Options['footer_color_scheme']) {
      if(isset($flavours_Options['footer_copyright_background_color']) && !empty($flavours_Options['footer_copyright_background_color'])) {
       ?>
      .footer-bottom {
        background-color: <?php echo esc_html($flavours_Options['footer_copyright_background_color'])." !important";
       ?>
      }

      <?php
       }
       ?>
      <?php if(isset($flavours_Options['footer_copyright_font_color']) && !empty($flavours_Options['footer_copyright_font_color'])) {
       ?>
      .coppyright {
        color: <?php echo esc_html($flavours_Options['footer_copyright_font_color'])." !important";
      ?>
      }

      <?php
       }
       ?>
      <?php
       }
       ?>
    </style>
    <?php
  }
}

add_action( 'admin_bar_menu', 'add_navigation', 999 );
function add_navigation( $wp_admin_bar ) {
  $args = array(
    'id'    => 'nav_menus',
    'title' => 'Navigation',
    'href'  => '/wp-admin/nav-menus.php',
    'meta'  => array( 'class' => 'my-toolbar-page' )
  );
  $wp_admin_bar->add_node( $args );
}

add_action( 'admin_bar_menu', 'add_subscribers', 999 );
function add_subscribers( $wp_admin_bar ) {
  $args = array(
    'id'    => 'nav_menus',
    'title' => 'Subscribers',
    'href'  => '/wp-admin/admin.php?page=newsletter_main_index',
    'meta'  => array( 'class' => 'my-toolbar-page' )
  );
  $wp_admin_bar->add_node( $args );
}

add_action( 'admin_bar_menu', 'add_widgets', 999 );
function add_widgets( $wp_admin_bar ) {
  $args = array(
    'id'    => 'nav_widgets',
    'title' => 'Widgets',
    'href'  => '/wp-admin/widgets.php',
    'meta'  => array( 'class' => 'my-toolbar-page' )
  );
  $wp_admin_bar->add_node( $args );
}

add_action( 'admin_bar_menu', 'add_export', 999 );
function add_export( $wp_admin_bar ) {
  $args = array(
    'id'    => 'nav_widgets',
    'title' => 'Export Produkts',
    'href'  => '/wp-admin/admin.php?page=hm_sbpf',
    'meta'  => array( 'class' => 'my-toolbar-page' )
  );
  $wp_admin_bar->add_node( $args );
}

add_action( 'widgets_init', 'add_sidebar_wc1' );
function add_sidebar_wc1() {
    register_sidebar( array(
        'name' => __( 'Category Left', 'efortis' ),
        'id' => 'category-left',
        'before_widget' => '<div id="%1$s" class="block %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<div class="block-title">',
  'after_title'   => '</div>',
    ) );
}

add_action( 'widgets_init', 'add_sidebar_wc2' );
function add_sidebar_wc2() {
    register_sidebar( array(
        'name' => __( 'Category Right', 'efortis' ),
        'id' => 'category-right',
        'before_widget' => '<div id="%1$s" class="block %2$s">',
  'after_widget'  => '</div>',
  'before_title'  => '<div class="block-title">',
  'after_title'   => '</div>',
    ) );
}

if(get_current_user_id() != 1){
  add_action('admin_head', 'custom_colors');
}

function custom_colors() {
   echo '<style type="text/css">
           #wpadminbar ul#wp-admin-bar-root-default>li#wp-admin-bar-wpfc-toolbar-parent, #wpadminbar ul#wp-admin-bar-root-default>li#wp-admin-bar-wpseo-menu, #adminmenu li.menu-top#toplevel_page__options, #adminmenu li.menu-top#toplevel_page_foodport_settings, #wpadminbar ul#wp-admin-bar-root-default>li#wp-admin-bar-_options, #adminmenu li.menu-top#toplevel_page_edit-post_type-acf ul li{ display: none; }
           #adminmenu li.menu-top#toplevel_page_loco, #adminmenu li.menu-top#menu-dashboard, #adminmenu li.menu-top.wp-menu-separator, #adminmenu li.menu-top#menu-posts, 
       #adminmenu li.menu-top#menu-media, #adminmenu li.menu-top#menu-pages, #adminmenu li.menu-top#menu-comments, #adminmenu li.menu-top#toplevel_page_woocommerce, 
       #adminmenu li.menu-top#menu-posts-product, #adminmenu li.menu-top#menu-posts-product, #adminmenu li.menu-top#menu-users, #adminmenu li.menu-top#toplevel_page_edit-post_type-acf, #adminmenu li.menu-top#toplevel_page_edit-post_type-acf ul li.wp-first-item
       { display: block; }
       .column-customer_message .note-on{ color: red; font-size: 16px; text-decoration: blink; }
         </style>';
}
show_admin_bar( false );

// Instantiate theme
$TmFlavours = new TmFlavours();

function set_attributes($post_id, $attributes) {
    //Type attribute
  $product_attributes = get_post_meta($post_id, '_product_attributes')[0];
    $product_attributes['pa_herkunft'] = array(
        //Make sure the 'name' is same as you have the attribute
        'name' => htmlspecialchars(stripslashes('Herkunft')),
        'value' => $attributes,
        'position' => 1,
        'is_visible' => 1,
        'is_variation' => 1,
        'is_taxonomy' => 0
    );
  //Add as post meta
  update_post_meta($post_id, '_product_attributes', $product_attributes);
}

function runMyPlugin(){
  
  echo '<h1>Test</h1>';
  
  //global $post;
  //$ids = get_products_from_category_by_ID();
  
  /*
  $result = add_role(
    'large_customers',
    __( 'Large Customers' ),
    array(
      'read'         => true,  // true allows this capability
      'edit_posts'   => false,
      'delete_posts' => false, // Use false to explicitly deny
    )
  );
  
  $result = add_role(
    'friend',
    __( 'Friend' ),
    array(
      'read'         => true,  // true allows this capability
      'edit_posts'   => false,
      'delete_posts' => false, // Use false to explicitly deny
    )
  );
  */
  
  
  /*
  foreach($ids as $id){
    
    

    /*
      $my_post = array(
          'ID'           => $id,
          'comment_status'           => 'open',
      );
      wp_update_post( $my_post );
    */

    
    //wp_delete_post( $id, true ); 
    
    /*
    $sku = get_product_sku($id);
    if($sku != null){
      $im = wp_get_attachment_by_post_name($sku);
      if($im > 0) set_post_thumbnail( $id, $im );
    }
    
  }
  */

  
}

if(!empty($_GET['func'])){
  //add_action('init', 'runMyPlugin');
}

function update_post_date($id, $date){
  $post = array(
      'ID'           => $id,
    'post_date'  => $date
  );
  wp_update_post( $post );
}

function get_product_sku( $id ) {
  global $wpdb;
  $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key='_sku' AND post_id='%s' LIMIT 1", $id ) );
  if ( $product_id ) return $product_id;
  return null;
}

function get_product_id( $sku ) {
  global $wpdb;
  $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
  if ( $product_id ) return $product_id;
  return null;
}

function get_attachment_url_by_title( $title ) {
    global $wpdb;
    $attachments = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_title = '$title' AND post_type = 'attachment' ", OBJECT );
    //print_r($attachments);
    if ( $attachments ){
        $attachment_url = $attachments[0]->guid;
    }else{
        return null;
    }
    return $attachments;
}

function wp_get_attachment_by_post_name( $str ) {
    global $wpdb;
  $posts = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_title = '$str' LIMIT 1", OBJECT ); 
  if ( $posts ){
    foreach ( $posts as $post ){
      return $post->ID;
    }
  } else {
    return 0;
  }
}

function get_products_from_category_by_ID() {
    $products_IDs = new WP_Query( array(
      'post_type' => 'product',
      'post_status' => 'publish',
      'fields' => 'ids',
      'posts_per_page'   => -1,
      'orderby' => 'date',
      'order'   => 'ASC',
  ) );

  return $products_IDs->posts;
}

add_action( 'init', 'add_endpoint' );
function add_endpoint(){
    add_rewrite_endpoint( 'einkaufsliste', EP_ROOT | EP_PAGES );
}
add_filter( 'wc_get_template', 'custom_endpoint', 10, 5 );
function custom_endpoint($located, $template_name, $args, $template_path, $default_path){
    if( $template_name == 'myaccount/my-account.php' ){
        global $wp_query;
        if(isset($wp_query->query['einkaufsliste'])){
            $located = get_template_directory() . '/woocommerce/myaccount/einkaufsliste.php';
        }
    }
    return $located;
}
function my_custom_my_account_menu_items( $items ) {
    $items = array(
        'dashboard'         => __( 'Dashboard', 'woocommerce' ),
        'orders'            => __( 'Orders', 'woocommerce' ),
        'einkaufsliste'            => __( 'Einkaufsliste', 'woocommerce' ),
        //'downloads'       => __( 'Downloads', 'woocommerce' ),
        'edit-address'    => __( 'Addresses', 'woocommerce' ),
        //'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
        'edit-account'      => __( 'Edit Account', 'woocommerce' ),
        'customer-logout'   => __( 'Logout', 'woocommerce' ),
    );

    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );