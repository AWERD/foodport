<?php  get_header();

// if(isset($flavours_Options['blog-page-layout']))
// {
// $design = $flavours_Options['blog-page-layout'];
// }
// else
// {
// $design='';	
// }
// $leftbar = $rightbar = $main = '';

// switch ((int)$design) {
//     case 1:
//         $rightbar ='hidesidebar';
//         $main = 'col2-left-layout';
//         $col = 'col-sm-9 col-md-9 col-lg-9 col-xs-12';
//         break;
//    case 3:
//        $leftbar = $rightbar = 'hidesidebar';
//         $main = 'col1-layout';
//         $col = 'col-sm-12 col-md-12 col-lg-12 col-xs-12';
//         break;

//     default:
//         $leftbar = 'hidesidebar';
//         $main = 'col2-right-layout';
//         $col = 'col-sm-9 col-md-9 col-lg-9 col-xs-12';
//         break;
        
// }
?>

<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>


<div class="main-container col2-right-layout">
   <div class="main container">
      <div class="row">
         <div id="content" class="col-sm-9 bounceInUp animated thm-blog">
            <div class="col-main">
               <!-- <div class="page-title"><h2></h2></div> -->
               <div id="main" class="row blog-wrapper">
                  <div class="col-sm-12">
                     <div id="primary" class="site-content">
                        <div id="content" role="main">
                        	<?php
                        		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

    		               	    $next_args = array(
    		               	                    'post_type' => 'post',
    		               	                    'post_status' => 'publish',
    		               	                    'posts_per_page' => 5,
    		               	                    'paged' => $paged,
    		               	                    'order'=>'DESC',
    		               	                    'orderby'=>'ID',
    		               	                    );

    		               	    $the_query = new WP_Query( $next_args );
    		               	    if ( $the_query->have_posts() ) {
    		               	        while ( $the_query->have_posts() ) {
    		               	            $the_query->the_post();
    		               	            ?>
                           <article class="blog_entry clearfix wow bounceInUp animated">
                              <header class="blog_entry-header clearfix">
                                 <div class="blog_entry-header-inner">
                                    <h2 class="blog_entry-title">
                                       <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                    </h2>
                                 </div>
                              </header>
                              <div class="entry-content">
                                 <div class="featured-thumb">
                                    <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" title="">
                                    </a>
                                 </div>
                                 <!-- featured-thumb -->
                                 <div class="entry-content">
                                    <p><?php the_excerpt(); ?>
                                    </p>
                                 </div>
                                 <!-- entry-content -->
                                 <p>
                                    <a class="btn" href="<?php the_permalink(); ?>">Read more</a>
                                 </p>
                              </div>
                              <!-- entry-content -->
                              <div class="entry-meta">
                                 This entry was posted on
                                 <time class="entry-date" datetime=""><?php the_date('M d, Y'); ?></time> 
                                 .
                              </div>
                           </article>
                                   <?php
                               }
                           } else {
                               echo "";
                           }
                           wp_reset_postdata();
                            ?> 
                        </div>
                        <!-- <div id="content" role="main"> -->
                     </div>
                     <div class="row">
                        <div class="col-sm-6 text-left">
                        	<?php 
                           echo paginate_links( array(
                           	'total'   => $the_query->max_num_pages
                           ) );
                           ?> 
                        </div>
                        <div class="col-sm-6 text-right"><!-- <?php global $wp_query; echo get_query_var('paged'); ?>Showing 1 to 2 of 2 (1 Pages) --></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <aside id="column-right" class="col-right col-xs-12  col-sm-3 sidebar">
            <div class="popular-posts widget widget__sidebar wow bounceInUp animated">
               <h3 class="widget-title">
                  Latest Posts 
               </h3>
               <div class="widget-content">
                  <ul class="posts-list unstyled clearfix">
                  	<?php
                  			

                  	    $next_args = array(
                  	                    'post_type' => 'post',
                  	                    'post_status' => 'publish',
                  	                    'posts_per_page' => 2,
                  	                    'order'=>'DESC',
                  	                    );

                  	    $the_query = new WP_Query( $next_args );
                  	    if ( $the_query->have_posts() ) {
                  	        while ( $the_query->have_posts() ) {
                  	            $the_query->the_post();
                  	            ?>
                     <li>
                        <figure class="featured-thumb"> 
                           <a href="<?php the_permalink(); ?>"> 
                           <img style="width: 80px; height: 53px;" src="<?php the_post_thumbnail_url(); ?>" > 
                           </a> 
                        </figure>
                        <!--featured-thumb-->
                        <h4><a href="<?php the_permalink(); ?>" title="Lorem ipsum dolor sit amet, consectetur adipiscing"><?php the_title(); ?></a></h4>
                        <p class="post-meta">
                           <i class="icon-calendar"></i>
                           <time class="entry-date">
                              <!-- -->
                              <?php the_date('M d, Y'); ?>   
                           </time>
                           .
                        </p>
                     </li>
                     <?php
	               	        }
	               	    } else {
	               	        echo "";
	               	    }
	               	    wp_reset_postdata();
	               	    ?>
                  </ul>
               </div>
            </div>
            <!-- <div class="hot-banner"><img alt="banner" src="catalog/view/theme/crocus/image/hot-trends-banner.jpg"></div>
            <div class="text-widget widget widget__sidebar">
               <h3 class="widget-title"><span>Text Widget</span></h3>
               <div class="widget-content">Mauris at blandit erat. Nam vel tortor non quam scelerisque cursus. Praesent nunc vitae magna pellentesque auctor. Quisque id lectus.<br>
                  <br>
                  Massa, eget eleifend tellus. Proin nec ante leo ssim nunc sit amet velit malesuada pharetra. Nulla neque sapien, sollicitudin non ornare quis, malesuada.
               </div>
            </div> -->
         </aside>
      </div>
      <!-- row -->
   </div>
   <!-- main container -->
</div>

<?php 

wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('jquery.countdown.min');
wp_enqueue_script('jquery.mobile-menu.min');
wp_enqueue_script('owl.carousel.min');
wp_enqueue_script('countdown');
wp_enqueue_script('revslider');
?>


<?php get_footer(); ?>
