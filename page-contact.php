<?php
/* Template name: Contact */
get_header(); ?>
<!-- <style type="text/css">
.google-maps{height:500px; margin-bottom: -40px;}.map-container{position:relative;background:#ddd}.map-info-window{background:rgba(0,0,0,.7);padding:20px;position:absolute;width:80%;left:10%;top:15%;max-width:530px}.map-info-window h2{font-family:Roboto,sans-serif;font-size:32px}.map-info-window h2,.map-info-window p{color:#fff}.map-info-window p{margin-top:10px}.map-info-window .fa{width:32px;font-size:15px}.map-info-window .fa-map-marker,.map-info-window .fa-mobile{font-size:18px}.map-info-window h2+p{margin-bottom:20px}.map-info-window .row{position:absolute;height:100%;left:0;top:0}@media (min-width:992px){.map-info-window{top:22%;padding:20px 50px}}
.form-control,output{display:block;font-size:14px;line-height:1.428571429;color:#555}.form-control{width:100%;height:38px;padding:6px 12px;background-color:#fff;background-image:none;border:1px solid #ccc;border-radius:0;box-shadow:inset 0 1px 1px rgba(0,0,0,.075);transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s}.form-control:focus{border-color:#66afe9;outline:0;box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)}.form-control::-moz-placeholder{color:#999;opacity:1}.form-control:-ms-input-placeholder{color:#999}.form-control::-webkit-input-placeholder{color:#999}.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control{background-color:#eee;opacity:1}.form-control[disabled],fieldset[disabled] .form-control{cursor:not-allowed}textarea.form-control{height:auto}input[type=search]{-webkit-appearance:none}@media screen and (-webkit-min-device-pixel-ratio:0){input[type=date].form-control,input[type=time].form-control,input[type=datetime-local].form-control,input[type=month].form-control{line-height:38px}.input-group-sm input[type=date],.input-group-sm input[type=time],.input-group-sm input[type=datetime-local],.input-group-sm input[type=month],.input-group-sm>.input-group-btn>input[type=date].btn,.input-group-sm>.input-group-btn>input[type=time].btn,.input-group-sm>.input-group-btn>input[type=datetime-local].btn,.input-group-sm>.input-group-btn>input[type=month].btn,.input-group-sm>input[type=date].form-control,.input-group-sm>input[type=date].input-group-addon,.input-group-sm>input[type=time].form-control,.input-group-sm>input[type=time].input-group-addon,.input-group-sm>input[type=datetime-local].form-control,.input-group-sm>input[type=datetime-local].input-group-addon,.input-group-sm>input[type=month].form-control,.input-group-sm>input[type=month].input-group-addon,input[type=date].input-sm,input[type=time].input-sm,input[type=datetime-local].input-sm,input[type=month].input-sm{line-height:25px}.input-group-lg input[type=date],.input-group-lg input[type=time],.input-group-lg input[type=datetime-local],.input-group-lg input[type=month],.input-group-lg>.input-group-btn>input[type=date].btn,.input-group-lg>.input-group-btn>input[type=time].btn,.input-group-lg>.input-group-btn>input[type=datetime-local].btn,.input-group-lg>.input-group-btn>input[type=month].btn,.input-group-lg>input[type=date].form-control,.input-group-lg>input[type=date].input-group-addon,.input-group-lg>input[type=time].form-control,.input-group-lg>input[type=time].input-group-addon,.input-group-lg>input[type=datetime-local].form-control,.input-group-lg>input[type=datetime-local].input-group-addon,.input-group-lg>input[type=month].form-control,.input-group-lg>input[type=month].input-group-addon,input[type=date].input-lg,input[type=time].input-lg,input[type=datetime-local].input-lg,input[type=month].input-lg{line-height:45px}}.form-group{margin-bottom:15px}
</style> -->
<div class="page-heading" style="background-image:url('/wp-content/uploads/2016/12/apple-1281744_1920.jpg');">
    <div class="page-title">
        <h1 class="entry-title">Get in touch</h1>
    </div>
</div>
<section class="content-layout content-section" style="padding: 60px 0;">
    <div class="container">
        <div class="contents">
            <p class="text-center">Lorem Ipsum is that it has a more-or-less normal distribution of <span class="text-break"></span>letters, as opposed to using Content here, content here', making it look like readable English.</p>
            <div class="contact-form pad-top-large">
                <form data-parsley-validate="" novalidate="">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="text" placeholder="Your Name" class="form-control input-lg" required="" name="name" data-parsley-id="4">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="email" placeholder="Email Address" class="form-control input-lg" required="" name="email" data-parsley-id="6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="text" placeholder="Subject" class="form-control input-lg" required="" name="subject" data-parsley-id="8">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="text" placeholder="Phone No" class="form-control input-lg" required="" name="phoneno" data-parsley-id="10">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <textarea class="form-control" rows="5" required="" placeholder="Message" name="mess" data-parsley-id="12"></textarea>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-primary btn-lg hvr-shutter-out-vertical">Send Message</button>
                    </div>
                </form>
                <div class="alert alert-success col-xs-12 hidden form-messges">
                    <p>We will responce as soon as possible.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-container row-float">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2701.3254145008827!2d8.650195715263415!3d47.38608327917063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479aa3da5a42089f%3A0x1adcaffbeac0b49f!2sSonnenbergstrasse+48%2C+8603+Schwerzenbach%2C+%C5%A0vicarska!5e0!3m2!1shr!2shr!4v1481478357302" frameborder="0" style="border:0; position: relative; width: 100%; height: 100%;min-height:500px;    margin-bottom: -50px;" allowfullscreen></iframe>
    <div class="container">
        <div class="row">
            <div class="map-info-window">
                <h2>FoodPort GmbH</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the</p>
                <p><i class="fa fa-map-marker"></i> Adresa, Switzerland</p>
                <p><i class="fa fa-envelope-o"></i> info@foodport.ch</p>
                <p><i class="fa fa-mobile"></i> +00 000 000</p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>