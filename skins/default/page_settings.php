<?php


if(function_exists("fp_settings"))
{
	fp_settings(array (
		'id' => 'acf_english-foodport-settings',
		'title' => '[:en]FoodPort Settings[:]',
		'fields' => array (
			array (
				'key' => 'field_59835fc9947f3',
				'label' => 'Delivery time',
				'name' => 'delivery_time',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_media',
					'operator' => '!=',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
