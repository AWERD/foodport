<?php

	add_filter( 'manage_edit-shop_order_columns', 'add_profit_column' );
	function add_profit_column($columns){
		$new_columns = (is_array($columns)) ? $columns : array();
		unset( $new_columns['order_actions'] );
		$new_columns['profit_column'] = 'Profit';
		$new_columns['order_actions'] = $columns['order_actions'];
		return $new_columns;
	}

	add_action( 'manage_shop_order_posts_custom_column', 'profit_column_value', 2 );
	function profit_column_value($column){
		global $post;
		$data = get_post_meta( $post->ID );
		if ( $column == 'profit_column' ) {    
			$order = new WC_Order( $post->ID );
			$order_items = $order->get_items();
			$total_price = 0;
			$total_original_price = 0;
			foreach($order_items as $item){
				$total_price += (float) $item['item_meta']['_line_total'][0];
				$total_original_price += (float) get_field( "original_price", $item['item_meta']['_product_id'][0] );
			}
			
			echo '<strong>' . number_format(($total_price - $total_original_price), 2, ",", ".") . ' ' . $data['_order_currency'][0] . '</strong>';
			echo '<br /><small> + ' . $data['_order_shipping'][0] . ' ' . $data['_order_currency'][0] . ' shipping</small>';
		}
	}
	
	add_filter( 'manage_edit-product_columns', 'vol_price_columns', 1 );
	function vol_price_columns($columns){
		$new_columns = (is_array($columns)) ? $columns : array();
		unset( $new_columns['order_actions'] );
		$new_columns['weight_vol'] = 'Weight / Volume';
		$new_columns['original_price'] = 'Original Price';
		return $new_columns;
	}
	add_action( 'manage_product_posts_custom_column', 'MY_COLUMNS_VALUES_FUNCTION', 2 );
	function MY_COLUMNS_VALUES_FUNCTION($column){
		global $post;
		$data = get_post_meta( $post->ID );

		//start editing, I was saving my fields for the orders as custom post meta
		//if you did the same, follow this code
		if ( $column == 'weight_vol' ) {
			$netto = get_field('netto_weight',  $post->ID);
			$volume = get_field('volume',  $post->ID);
			echo (!empty($netto) ? $netto : 'X');
			echo (!empty($volume) ? ' | ' . $volume : '');
		}
		if ( $column == 'original_price' ) {  
			$price = get_field('original_price',  $post->ID);
			echo (!empty($price) ? $price : 'X');
		}
		//stop editing
	}
	
	function buying_list( $atts ){		
		$user = get_current_user_id();
		$field = get_field('buying_lists', 'user_' . $user);
		$ids = explode(", ", $field);
		if(!empty($field)){
			$html = '<ul id="my-lists">';
			foreach($ids as $id){
				if($id){
					$term = get_term($id);
					$html .= '<li><a href="javascript:" data-link="'.$term->slug.'">'.$term->name.'</a></li>';
				}
			}
			$html .= '</ul>';
		} else {
			$html = '<div style="padding: 25px;">Sie haben noch keine Einkaufsliste. Erstellen Sie <a href="/my-account/einkaufsliste/" style="color: red;">hier Ihre erste Einkaufsliste</a>.</div>';
		}		
		return $html;
	}
	add_shortcode( 'buying_list', 'buying_list' );
	
	function get_buying_list(){
		$user = get_current_user_id();
		$field = get_field('buying_lists', 'user_' . $user);
		$ids = explode(", ", $field);
		if(!empty($field)){
			$html = '';
			foreach($ids as $id){
				if($id){
					$term = get_term($id);
					$html .= '<li><a href="javascript:" data-link="'.$term->slug.'">'.$term->name.'</a></li>';
				}
			}
			$html = '<li role="separator" class="divider"></li><li><a href="/my-account/einkaufsliste/" style="color: red;">Erstellen</a></li>';
		} else {
			$html = '<li><a href="/my-account/einkaufsliste/" style="color: red;">Erstellen</a></li>';
		}		
		return $html;
	}
	
	function shop_categories( $atts ){		
		$html = '';
		$terms = get_terms( 'product_cat', array(
			'hide_empty' => true,
			'orderby'    => 'title',
			'order' 	 => 'ASC',
		));
		if ( $terms ) {         
			$html .= '<ul class="product-cats">';			 
				foreach ( $terms as $term ) {
					if($term->parent == '0'){
						//print_r($term);
						$sportcat = '';
						$html .= '<li class="category '.$sportcat.'">';							 
							//woocommerce_subcategory_thumbnail( $term );
							$feat_image_url = get_term_meta($term->term_id)['thumbnail_id'];
							if(!empty($feat_image_url)) $ic = wp_get_attachment_thumb_url( $feat_image_url[0] );
							else $ic = '';							
							$html .= '<a href="' .  esc_url( get_term_link( $term ) ) . '" onClick="changeCat(\''.$term->slug.'\', this)" data-slug="'.$term->slug.'" class="' . $term->slug . '"><i class="fa" style="background-image:url('.$ic.');"></i>' . $term->name . ' <span class="pull-right">('.$term->count.')</span></a>';
							//$html .= wp_get_attachment_image_src(get_post_thumbnail_id(get_term_meta($term->term_id)['thumbnail_id']), 'medium');
							$subs = get_term_children($term->term_id, 'product_cat');
							//print_r($subs);
							if(!empty($subs)){
								$html .= '<ul>';
								$arr = array();
								foreach($subs as $sub){
									$t = get_term_by('id', $sub, 'product_cat');
									$arr[] = $t;
								}
								
								$col  = 'name';
								$sort = array();
								foreach ($arr as $i => $obj) {
								  $sort[$i] = $obj->{$col};
								}
								$sorted_db = array_multisort($sort, SORT_ASC, $arr);
								foreach($arr as $ar){
									$feat_image_url = get_term_meta($ar->term_id)['thumbnail_id'];
									if(!empty($feat_image_url)) $ic = wp_get_attachment_thumb_url( $feat_image_url[0] );
									else $ic = '';
									$html .= '<li><a href="' .  esc_url( get_term_link( $ar ) ) . '" onClick="changeCat(\''.$ar->slug.'\', this)"><i class="fa" style="background-image:url('.$ic.');display:block;width: 30px; height: 30px;margin-right: 7px;"></i>'.$ar->name.' <span class="pull-right">('.$ar->count.')</span></a></li>';
								}
								
								
								$html .= '</ul>';
							}
						$html .= '</li>';
					}
			}
			$html .= '</ul>';
		}
		return $html;
	}
	add_shortcode( 'shop_categories', 'shop_categories' );
	
	function category_has_children( $term_id = 0, $taxonomy = 'product_cat' ) {
		$children = get_categories( array( 'child_of' => $term_id, 'taxonomy' => $taxonomy ) );
		return ( $children );
	}
	
	
	/* Add Custom Taxonomy */
	function add_list() {
     // Add new "Locations" taxonomy to Posts
     register_taxonomy_for_object_type( 'buying_lists', 'product' );
     register_taxonomy('buying_lists', 'product', array(
       // Hierarchical taxonomy (like categories)
       'hierarchical' => false,
 	  'public'                     => true,
     'show_ui'                    => true,
     'show_admin_column'          => false,
     'show_in_nav_menus'          => false,
     'show_tagcloud'              => false,
       // This array of options controls the labels displayed in the WordPress Admin UI
       'labels' => array(
         'name' => _x( 'Lists', 'taxonomy general name' ),
         'singular_name' => _x( 'List', 'taxonomy singular name' ),
         'search_items' =>  __( 'Search Lists' ),
         'all_items' => __( 'All Lists' ),
         'parent_item' => __( 'Parent List' ),
         'parent_item_colon' => __( 'Parent List:' ),
         'edit_item' => __( 'Edit List' ),
         'update_item' => __( 'Update List' ),
         'add_new_item' => __( 'Add New List' ),
         'new_item_name' => __( 'New List Name' ),
         'menu_name' => __( 'Buying Lists' ),
       ),
       // Control the slugs used for this taxonomy
       'rewrite' => array(
         'slug' => 'list', // This controls the base slug that will display before each term
         'with_front' => false, // Don't display the category base before "/locations/"
         'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
       ),
     ));
   }
   add_action( 'init', 'add_list', 0 );
   
	add_action( "wp_ajax_get_gallery", "get_gallery" );
	add_action( "wp_ajax_nopriv_get_gallery", "get_gallery" );
	function get_gallery() {
		echo json_encode(get_field($_POST['category'], 5));
		exit();
	}
	
	add_action( "wp_ajax_removeBuyingList", "removeBuyingList" );
	add_action( "wp_ajax_nopriv_removeBuyingList", "removeBuyingList" );
	function removeBuyingList() {
		$term = term_exists($_POST['term'], 'buying_lists');
		$id = $term['term_id'];
		$user = get_current_user_id();
		$field = get_field('buying_lists', 'user_' . $user);
		if (strpos($field, $id . ', ') !== false){
			update_field('buying_lists', str_replace($id . ', ', '', $field), 'user_' . $user);
			wp_delete_term( $id, 'buying_lists');
		}
		exit();
	}
	
	add_action( "wp_ajax_add_to_ein", "add_to_ein" );
	add_action( "wp_ajax_nopriv_add_to_ein", "add_to_ein" );
	function add_to_ein() {
		$id = $_POST['id'];
		$user = get_current_user_id();
		$field = get_field('buying_lists', 'user_' . $user);
		$term_name = $_POST['term'];
		$term = term_exists($term_name, 'buying_lists');
		if(!$term['term_id']) return false;
		if (strpos($field, $term['term_id'] . ', ') !== false) {
			wp_set_post_terms( $id, Array($term_name), 'buying_lists', true );
		}
		exit();
	}
	
	add_action( "wp_ajax_add_buyinglist", "add_buyinglist" );
	add_action( "wp_ajax_nopriv_add_buyinglist", "add_buyinglist" );
	function add_buyinglist() {
		$user = get_current_user_id();
		$field = get_field('buying_lists', 'user_' . $user);
		$list = $_POST['list'];
		foreach($list as $ls){
			$ex = term_exists($ls);
			if($ls != ''){
				if ($ex !== 0 && $ex !== null) {
				$term = wp_insert_term( $ls . ' - ' . get_current_user_id(), 'buying_lists' );
				} else {
					$term = wp_insert_term( $ls, 'buying_lists' );
				}
			}			
			$field .= $term['term_id'] . ', ';
		}
		update_field('buying_lists', $field, 'user_' . $user);
		exit();
	}
	
	function clog($message){
		print_r('<script type="text/javascript">console.log("'.$message.'");</script>');
	}
	
function bbloomer_wc_discount_total() {
 
    global $woocommerce;
     
    // Get cart contents
     
    $cart_subtotal = $woocommerce->cart->cart_contents;
 
    // Set discount variable to 0 so it is available outside the loop
    $discount_total = 0;
     
    // Loop through the cart contents to get the product IDs
    foreach ($woocommerce->cart->cart_contents as $product_data) {
         
        if ($product_data['variation_id'] > 0) {
            $product = wc_get_product( $product_data['variation_id'] );
        } else {
            $product = wc_get_product( $product_data['product_id'] );
        }
 
        if ( isset($product->sale_price) & $product->sale_price > 0) {
        $discount = ($product->regular_price - $product->sale_price) * $product_data['quantity'];
        $discount_total += $discount;
        }
 
    }
     
    if ( $discount_total > 0 ) {
		return $discount_total+$woocommerce->cart->discount_cart;
    }
 
}

function bbloomer_wc_discount_total_checkout(){
	$discount_total = bbloomer_wc_discount_total();
	if ( $discount_total > 0 ) {
		echo '<tr class="cart-discount">
		<th>'. __( 'Sie haben gespart', 'woocommerce' ) .'</th>
		<td data-title=" '. __( 'You Saved', 'woocommerce' ) .' " style="color: #ab0000 !important;">'
		. wc_price($discount_total) .'</td>
		</tr>';
    }
}
add_action( 'woocommerce_cart_totals_after_order_total', 'bbloomer_wc_discount_total_checkout');
add_action( 'woocommerce_review_order_after_order_total', 'bbloomer_wc_discount_total_checkout');


function notification_100_CHF(){	
	global $woocommerce;
	$total = $woocommerce->cart->subtotal;
	if($total < 100) echo '<div class="woocommerce-info"><a href="/shop">Ab einem Warenwert von 100 CHF fallen die Liefergebühren auf 4.95. Es lohnt sich!</a></div>';
}
add_action( 'woocommerce_before_checkout_form', 'notification_100_CHF');
add_action( 'woocommerce_before_checkout_form', 'notification_100_CHF');

add_action( 'wp_ajax_product_remove', 'product_remove' );
add_action( 'wp_ajax_nopriv_product_remove', 'product_remove' );
function product_remove() {
    global $wpdb, $woocommerce;
	session_start();
    $cart = WC()->instance()->cart;
	$id = $_POST['product_id'];
	$cart_id = $cart->generate_cart_id($id);
	$cart_item_id = $cart->find_product_in_cart($cart_id);
	
	echo 'ID: ' . $id;
	echo 'Cart ID: ' . $cart_id;
	echo 'Cart Item ID: ' . $cart_item_id;
	
	unset($cart->cart_contents[$cart_item_id]);
	$cart->set_quantity($cart_item_id,0);
	$cart->remove_cart_item($cart_item_id);
}

function mode_theme_update_mini_cart() {
  echo wc_get_template( 'cart/mini-cart.php' );
  die();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );

function changeItemQtyCart(){
	$cartKeySanitized = filter_var($_POST['cart_item_key'], FILTER_SANITIZE_STRING);
    $cartQtySanitized = filter_var($_POST['cart_item_qty'], FILTER_SANITIZE_STRING); 
    global $woocommerce;
    ob_start();
    $woocommerce->cart->set_quantity($cartKeySanitized,$cartQtySanitized); 
    ob_get_clean();
}
add_filter( 'wp_ajax_nopriv_changeItemQtyCart', 'changeItemQtyCart' );
add_filter( 'wp_ajax_changeItemQtyCart', 'changeItemQtyCart' );

if($_GET['type'] == 'fresspackli'){
	add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );
	add_action( 'woocommerce_after_order_notes', 'add_army_fields' );
}

function woo_custom_add_to_cart( $cart_item_data ) {
    global $woocommerce;
    $woocommerce->cart->empty_cart();
    return $cart_item_data;
}

function add_army_fields( $checkout ) {
    echo '<div id="army-shipping"><h3>Feldadresse</h3><span>The following information below will be used to create an account.</span>';
	woocommerce_form_field( 'feldadresse_grad', array(
		'type'        => 'select',
		'label'       => __('Grad'),
		'placeholder' => _x('', 'placeholder', 'woocommerce'),
		'required'    => 'true',
		'options'     => array(
				'Rekrut' => __('Rekrut', 'flavours'), 
				'Soldat' => __('Soldat', 'flavours'), 
				'Gefreiter' => __('Gefreiter', 'flavours'), 
				'Obergefreiter' => __('Obergefreiter', 'flavours'), 
				'Korporal' => __('Korporal', 'flavours'), 
				'Wachtmeister' => __('Wachtmeister', 'flavours'), 
				'Oberwachtmeister' => __('Oberwachtmeister', 'flavours'), 
				'Feldweibel' => __('Feldweibel', 'flavours'), 
				'Fourier' => __('Fourier', 'flavours'), 
				'Hauptfeldweibel' => __('Hauptfeldweibel', 'flavours'), 
				'Adjutant Unteroffizier' => __('Adjutant Unteroffizier', 'flavours'), 
				'Stabsadjutant' => __('Stabsadjutant', 'flavours'), 
				'Hauptadjutant' => __('Hauptadjutant', 'flavours'), 
				'Chefadjutant' => __('Chefadjutant', 'flavours'), 
				'Leutnant' => __('Leutnant', 'flavours'), 
				'Oberleutnant' => __('Oberleutnant', 'flavours'), 
				'Hauptmann' => __('Hauptmann', 'flavours'), 
				'Major' => __('Major', 'flavours'), 
				'Oberstleutnant' => __('Oberstleutnant', 'flavours'), 
				'Oberst' => __('Oberst', 'flavours')
		),
	), $checkout->get_value( 'feldadresse_grad' ));
	woocommerce_form_field( 'feldadresse_vorname', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Vorname'),
        'placeholder' => __('Meier'),
        'required'    => 'true',
    ), $checkout->get_value( 'feldadresse_vorname' ));
	woocommerce_form_field( 'feldadresse_name', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Name'),
        'placeholder' => __('Hans'),
        'required'    => 'true',
    ), $checkout->get_value( 'feldadresse_name' ));
	woocommerce_form_field( 'feldadresse_einheit', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Genaue Bezeichnung der Einheit, des Stabs, der Schule oder des Kurses'),
        'placeholder' => __('Inf Kp 13/1 Militär 61114'),
        'required'    => 'true',
    ), $checkout->get_value( 'feldadresse_einheit' ));
	woocommerce_form_field( 'feldadresse_vermerk', array(
        'type'        => 'textarea',
        'class'       => array('form-row-wide'),
        'label'       => __('Vermerk'),
        'placeholder' => __(''),
        'required'    => 'false',
    ), $checkout->get_value( 'feldadresse_vermerk' ));
	echo '<h3>Kasernenadresse</h3>';
	woocommerce_form_field( 'kasernenadresse_grad', array(
		'type'        => 'select',
		'label'       => __('Grad'),
		'placeholder' => _x('', 'placeholder', 'woocommerce'),
		'required'    => 'true',
		'options'     => array(
				'Rekrut' => __('Rekrut', 'flavours'), 
				'Soldat' => __('Soldat', 'flavours'), 
				'Gefreiter' => __('Gefreiter', 'flavours'), 
				'Obergefreiter' => __('Obergefreiter', 'flavours'), 
				'Korporal' => __('Korporal', 'flavours'), 
				'Wachtmeister' => __('Wachtmeister', 'flavours'), 
				'Oberwachtmeister' => __('Oberwachtmeister', 'flavours'), 
				'Feldweibel' => __('Feldweibel', 'flavours'), 
				'Fourier' => __('Fourier', 'flavours'), 
				'Hauptfeldweibel' => __('Hauptfeldweibel', 'flavours'), 
				'Adjutant Unteroffizier' => __('Adjutant Unteroffizier', 'flavours'), 
				'Stabsadjutant' => __('Stabsadjutant', 'flavours'), 
				'Hauptadjutant' => __('Hauptadjutant', 'flavours'), 
				'Chefadjutant' => __('Chefadjutant', 'flavours'), 
				'Leutnant' => __('Leutnant', 'flavours'), 
				'Oberleutnant' => __('Oberleutnant', 'flavours'), 
				'Hauptmann' => __('Hauptmann', 'flavours'), 
				'Major' => __('Major', 'flavours'), 
				'Oberstleutnant' => __('Oberstleutnant', 'flavours'), 
				'Oberst' => __('Oberst', 'flavours')
		),
	), $checkout->get_value( 'kasernenadresse_grad' ));
	woocommerce_form_field( 'kasernenadresse_vorname', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Vorname'),
        'placeholder' => __('Müller'),
        'required'    => 'true',
    ), $checkout->get_value( 'kasernenadresse_vorname' ));
	woocommerce_form_field( 'kasernenadresse_name', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Name'),
        'placeholder' => __('Erich'),
        'required'    => 'true',
    ), $checkout->get_value( 'kasernenadresse_name' ));
	woocommerce_form_field( 'kasernenadresse_einheit', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Genaue Bezeichnung der Einheit, des Stabs, der Schule oder des Kurses'),
        'placeholder' => __('Pz RS 23-1'),
        'required'    => 'true',
    ), $checkout->get_value( 'kasernenadresse_einheit' ));
	woocommerce_form_field( 'kasernenadresse_kaserne', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Kaserne'),
        'placeholder' => __('Kp2, Zug 1'),
        'required'    => 'true',
    ), $checkout->get_value( 'kasernenadresse_kaserne' ));
	woocommerce_form_field( 'kasernenadresse_postleitzahl', array(
        'type'        => 'text',
        'class'       => array('form-row-wide'),
        'label'       => __('Postleitzahl & Ort'),
        'placeholder' => __('3609 Thun'),
        'required'    => 'true',
    ), $checkout->get_value( 'kasernenadresse_postleitzahl' ));

    echo '</div>';
	echo '<style type="text/css">.woocommerce-shipping-fields h3#ship-to-different-address, .woocommerce-shipping-fields #order_comments_field{display: none;}</style>';
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'army_custom_field', 10, 1 );
function army_custom_field($order){
	$feldadresse_grad = get_post_meta( $order->id, 'feldadresse_grad', true );
	$feldadresse_vorname = get_post_meta( $order->id, 'feldadresse_vorname', true );
	$feldadresse_name = get_post_meta( $order->id, 'feldadresse_name', true );
	$feldadresse_einheit = get_post_meta( $order->id, 'feldadresse_einheit', true );
	$feldadresse_vermerk = get_post_meta( $order->id, 'feldadresse_vermerk', true );
	$kasernenadresse_grad = get_post_meta( $order->id, 'kasernenadresse_grad', true );
	$kasernenadresse_vorname = get_post_meta( $order->id, 'kasernenadresse_vorname', true );
	$kasernenadresse_name = get_post_meta( $order->id, 'kasernenadresse_name', true );
	$kasernenadresse_einheit = get_post_meta( $order->id, 'kasernenadresse_einheit', true );
	$kasernenadresse_kaserne = get_post_meta( $order->id, 'kasernenadresse_kaserne', true );
	$kasernenadresse_postleitzahl = get_post_meta( $order->id, 'kasernenadresse_postleitzahl', true );

	if(!empty($feldadresse_grad)) echo '<h3>Feldadresse</h3><p style="margin-bottom:0;"><strong>'.__('Grad').':</strong> ' . $feldadresse_grad . '</p>';
	if(!empty($feldadresse_vorname)) echo '<p style="margin:0;"><strong>'.__('Vorname').':</strong> ' . $feldadresse_vorname . '</p>';
	if(!empty($feldadresse_name)) echo '<p style="margin:0;"><strong>'.__('Name').':</strong> ' . $feldadresse_name . '</p>';
	if(!empty($feldadresse_einheit)) echo '<p style="margin:0;"><strong>'.__('Einheit').':</strong> ' . $feldadresse_einheit . '</p>';
	if(!empty($feldadresse_vermerk)) echo '<p style="margin:0;"><strong>'.__('Vermerk').':</strong> ' . $feldadresse_vermerk . '</p>';
	if(!empty($kasernenadresse_grad)) echo '<h3>Feldadresse</h3><p style="margin-bottom:0;"><strong>'.__('Grad').':</strong> ' . $kasernenadresse_grad . '</p>';
	if(!empty($kasernenadresse_vorname)) echo '<p style="margin:0;"><strong>'.__('Vorname').':</strong> ' . $kasernenadresse_vorname . '</p>';
	if(!empty($kasernenadresse_name)) echo '<p style="margin:0;"><strong>'.__('Name').':</strong> ' . $kasernenadresse_name . '</p>';
	if(!empty($kasernenadresse_einheit)) echo '<p style="margin:0;"><strong>'.__('Einheit').':</strong> ' . $kasernenadresse_einheit . '</p>';
	if(!empty($kasernenadresse_kaserne)) echo '<p style="margin:0;"><strong>'.__('Kaserne').':</strong> ' . $kasernenadresse_kaserne . '</p>';
	if(!empty($kasernenadresse_postleitzahl)) echo '<p style="margin:0;"><strong>'.__('Postleitzahl').':</strong> ' . $kasernenadresse_postleitzahl . '</p>';
}

add_action('woocommerce_checkout_update_order_meta', 'save_army_fields');
function save_army_fields($order_id){
	if ($_POST['feldadresse_grad']) update_post_meta( $order_id, 'feldadresse_grad', esc_attr($_POST['feldadresse_grad']));
	if ($_POST['feldadresse_vorname']) update_post_meta( $order_id, 'feldadresse_vorname', esc_attr($_POST['feldadresse_vorname']));
	if ($_POST['feldadresse_name']) update_post_meta( $order_id, 'feldadresse_name', esc_attr($_POST['feldadresse_name']));
	if ($_POST['feldadresse_einheit']) update_post_meta( $order_id, 'feldadresse_einheit', esc_attr($_POST['feldadresse_einheit']));
	if ($_POST['feldadresse_vermerk']) update_post_meta( $order_id, 'feldadresse_vermerk', esc_attr($_POST['feldadresse_vermerk']));
	if ($_POST['kasernenadresse_grad']) update_post_meta( $order_id, 'kasernenadresse_grad', esc_attr($_POST['kasernenadresse_grad']));
	if ($_POST['kasernenadresse_vorname']) update_post_meta( $order_id, 'kasernenadresse_vorname', esc_attr($_POST['kasernenadresse_vorname']));
	if ($_POST['kasernenadresse_name']) update_post_meta( $order_id, 'kasernenadresse_name', esc_attr($_POST['kasernenadresse_name']));
	if ($_POST['kasernenadresse_einheit']) update_post_meta( $order_id, 'kasernenadresse_einheit', esc_attr($_POST['kasernenadresse_einheit']));
	if ($_POST['kasernenadresse_kaserne']) update_post_meta( $order_id, 'kasernenadresse_kaserne', esc_attr($_POST['kasernenadresse_kaserne']));
	if ($_POST['kasernenadresse_postleitzahl']) update_post_meta( $order_id, 'kasernenadresse_postleitzahl', esc_attr($_POST['kasernenadresse_postleitzahl']));
}

function wooc_extra_register_fields() { ?>
<p class="form-row form-row-wide"><label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label><input type="text" class="input-text" name="first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" /></p><p class="form-row form-row-wide"><label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label><input type="text" class="input-text" name="last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" /></p>
<?php }
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

add_action( 'user_register', 'user_registration', 10, 1 );
function user_registration( $user_id ) {
    if ( isset( $_POST['first_name'] ) ) update_user_meta($user_id, 'first_name', $_POST['first_name']);
    if ( isset( $_POST['first_name'] ) ) update_user_meta($user_id, 'billing_first_name', $_POST['first_name']);
    if ( isset( $_POST['last_name'] ) ) update_user_meta($user_id, 'last_name', $_POST['last_name']);
    if ( isset( $_POST['last_name'] ) ) update_user_meta($user_id, 'billing_last_name', $_POST['last_name']);
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 10, 2 );
function bbloomer_remove_shipping_label($label, $method) {
	//$new_label = preg_replace( '/^.+:/', '', $label );
	if (strpos($label, 'DPD Versand') !== false){
		$label = str_replace( "DPD Versand: ", "", $label );
		return $label . ' - DPD Versand';
	}
	return $label;
}

function wpse_131562_redirect() {
    if (
        ! is_user_logged_in()
        && (is_cart() || is_checkout())
    ) {
        wp_redirect(get_permalink( get_option('woocommerce_myaccount_page_id') ) . '?red=checkout');
        exit;
    }
}
add_action('template_redirect', 'wpse_131562_redirect');

function checkout_redirect() {
  return '/checkout';
}
if($_GET['red'] == 'checkout'){
	add_filter('login_redirect', 'checkout_redirect');
	add_filter( 'woocommerce_login_redirect', 'checkout_redirect', 10, 2 ); 
	add_filter( 'registration_redirect', 'checkout_redirect' );
	add_filter( 'woocommerce_registration_redirect', 'checkout_redirect' );
}

add_filter('woocommerce_email_before_order_table','woo_email_tracking', 10, 2);
function woo_email_tracking( $order ) {
    $track = get_post_meta( $order->id, 'wf_wc_shipment_source', true);
	$track = $track['shipment_id_cs'];
	if($track != ''){
		$link .= '<a href="https://tracking.dpd.de/parcelstatus?query='.$track.'" >';
		$link .= __( 'Ihre Trackingnummer lautet ', 'efortis' ) . $track;
		$link .= '</a>';
		echo $link;
	}	
}

add_action( 'woocommerce_email_after_order_table', 'add_tax_info', 10, 2 );
function add_tax_info($order){
	//echo ''
}

add_action( 'woocommerce_checkout_process', 'wc_minimum_order_amount' );
add_action( 'woocommerce_before_cart' , 'wc_minimum_order_amount' );
 
function wc_minimum_order_amount() {
    // Set this variable to specify a minimum order value
    $minimum = 40.95;

    if ( WC()->cart->total < $minimum ) {

        if( is_cart() ) {

            wc_print_notice( 
                sprintf( 'You must have an order with a minimum of 30,00 CHF to place your order, your current order total is %s.' , 
                    //wc_price( $minimum ), 
                    wc_price( WC()->cart->subtotal )
                ), 'error' 
            );

        } else {

            wc_add_notice( 
                sprintf( 'You must have an order with a minimum of 30,00 CHF to place your order, your current order total is %s.' , 
                    //wc_price( $minimum ), 
                    wc_price( WC()->cart->subtotal )
                ), 'error' 
            );

        }
    }

}


add_action( 'admin_menu', 'my_admin_menu' );
function my_admin_menu() {
	add_menu_page( 'FoodPort Settings', 'FoodPort Settings', 'manage_options', 'foodport_settings.php', 'foodport_settings', 'dashicons-tickets', 6  );
}
function foodport_settings(){
	include_once( plugin_dir_path( __FILE__ ) . 'vendor/advanced-custom-fields/acf.php' );
	add_filter( 'acf/settings/path', array( $this, 'update_acf_settings_path' ) );
	add_filter( 'acf/settings/dir', array( $this, 'update_acf_settings_dir' ) );
	?>
	<div class="wrap">
		<h2>FoodPort Settings</h2>
		<?php include_once( 'page_settings.php' ); fp_settings(); ?>
	</div>
	<?php
}


function get_info_news(){
	$recent_posts = wp_get_recent_posts(array( 'numberposts' => '5', 'category' => 3260 ));
	$cnt = 1;
	foreach( $recent_posts as $recent ){
		echo '<a href="' . get_permalink($recent["ID"]) . '" target="_blank">' .   $recent["post_title"].'</a> </li>';
		if($cnt != count($recent_posts)) echo ' | ';
		$cnt++;
	}
	wp_reset_query();
}

/**
 * Restrict native search widgets to the 'post' post type
 */
add_filter( 'widget_title', function( $title, $instance, $id_base )
{
    // Target the search base
    if( 'search' === $id_base )
        add_filter( 'get_search_form', 'wpse_post_type_restriction' );
    return $title;
}, 10, 3 );
function wpse_post_type_restriction( $html )
{
    // Only run once
    remove_filter( current_filter(), __FUNCTION__ );

    // Inject hidden post_type value
    return str_replace( 
        '</form>', 
        '<input type="hidden" name="post_type" value="post" /></form>',
        $html 
    );
}

add_action('woocommerce_cart_collaterals', 'cart_extra_info');
function cart_extra_info() {
	global $woocommerce;
	?> <script type="text/javascript">shippinginfo = "<tr><td>Gewicht</td><td><?=$woocommerce->cart->cart_contents_weight . ' ' . get_option('woocommerce_weight_unit');?> (<?php $pcg = floor(WC()->cart->get_cart_contents_weight() / 29);  echo ($pcg + 1) . ' <small>x</small> <span class=\"glyphicon glyphicon-folder-close\"></span>'; ?>)<br /><a href=\"https://foodport.ch/lieferkonditionen/\" target=\"_blank\">Lieferkonditionen</a></td></tr>";</script><?php
	echo '<script type="text/javascript">jQuery(document).ready(function(){ jQuery("tr.shipping").after(shippinginfo); });</script>';
}

function getCurrentLink($lang){
	$url = $_SERVER['REQUEST_URI'];
	$url = str_replace("/en/","", $url);
	$url = str_replace("/de/","", $url);
	$url = "https://foodport.ch/" . $lang . "/" . $url;
	return $url;
}