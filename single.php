<?php get_header();
$TmFlavours->tmFlavours_setPostViews(get_the_ID());
 $design = get_post_meta($post->ID, 'tmFlavours_post_layout', true);
 $leftbar = $rightbar = $main = '';

$leftbar = $rightbar = $main = '';

switch ((int)$design) {
    case 1:
        $rightbar ='hidesidebar';
        $main = 'col2-left-layout';
        $col = 'col-sm-9 col-md-9 col-lg-9 col-xs-12';
        break;
    case 3:
        $leftbar = $rightbar = 'hidesidebar';
        $main = 'col1-layout';
        $col = 'col-sm-12 col-md-12 col-lg-12 col-xs-12';
        break;

    default:
        $leftbar = 'hidesidebar';
        $main = 'col2-right-layout';
        $col = 'col-sm-9 col-md-9 col-lg-9 col-xs-12';
        break;

}

?>
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>
</a>
<div class="blog-wrapper <?php echo esc_html($main) ?>">
        <section class="container">
            <div class="row">

    <?php if (empty($leftbar) && $leftbar != 'hidesidebar') { ?>
    <aside id="column-left" class="col-left sidebar col-lg-3 col-sm-3 col-xs-12  <?php echo esc_html($leftbar) ?>">
     <?php get_sidebar('content'); ?>
    </aside>
    <?php } ?>
            
            <section class="<?php echo esc_html($col); ?>">
              <?php  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

              get_query_var('page') ?>
              <?php while (have_posts()) : the_post(); ?>
                <article class="blog_entry clearfix">
                  <div class="blog-post container-paper singlepost">
                    <div class="post-container">
                      <h1 style="color: black">
                          <?php the_title(); ?>
                      </h1>
                      <?php if (has_post_thumbnail()) : ?>
                        <div class="post-img <?php if (has_post_thumbnail()){?> has-img <?php } ?>">
                              <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                              <a href="<?php the_permalink(); ?>"> 
                                <figure>
                                  <img src="<?php echo esc_url($image[0]); ?>" alt="<?php the_title(); ?>">
                                </figure>  
                              </a>
                              </div>
                        <?php endif; ?>
                        <div class="post-detail-container">
                         <div class="title">
                        
                          <?php if (!isset($flavours_Options) || $flavours_Options['blog_full_date'] == 1) { ?>
                                            <div class="blogdate"><time class="entry-date updated">
                                                <?php echo esc_html(get_the_date()); ?></time>
                                            </div>
                               <?php } ?>
                    </div>
                   
                        <div class="detaildesc">
                        <?php the_content(); ?>
                    </div>

                        

                        </footer>
                      </div>  
                    </div>
                </div>
                  <?php
                  // Author bio.
              
                  ?>
              <?php endwhile; ?>
            </article>
              <div class="clearfix"></div>
              <?php comments_template('/custom-comments.php'); ?>
                </section>
    <?php if (empty($rightbar) && $rightbar != 'hidesidebar') { ?>
      <aside id="column-right" class="col-right col-xs-12  col-sm-3 sidebar">
            <div class="popular-posts widget widget__sidebar wow bounceInUp animated">
               <h3 class="widget-title">
                  Latest Posts 
               </h3>
               <div class="widget-content">
                  <ul class="posts-list unstyled clearfix">
                    <?php
                        

                        $next_args = array(
                                        'post_type' => 'post',
                                        'post_status' => 'publish',
                                        'posts_per_page' => 2,
                                        'order'=>'DESC',
                                        );

                        $the_query = new WP_Query( $next_args );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                ?>
                     <li>
                        <figure class="featured-thumb"> 
                           <a href="<?php the_permalink(); ?>"> 
                           <img style="width: 80px; height: 53px;" src="<?php the_post_thumbnail_url(); ?>" > 
                           </a> 
                        </figure>
                        <!--featured-thumb-->
                        <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                        <p class="post-meta">
                           <i class="icon-calendar"></i>
                           <time class="entry-date">
                              <!-- -->
                              <?php the_date('M d, Y'); ?>   
                           </time>
                           .
                        </p>
                     </li>
                     <?php
                          }
                      } else {
                          echo "";
                      }
                      wp_reset_postdata();
                      ?>
                  </ul>
               </div>
            </div>
            <div class="hot-banner"><img alt="banner" src="<?php echo wp_get_attachment_url(12334); ?>"></div>
            <div class="text-widget widget widget__sidebar">
               <h3 class="widget-title"><span>Text Widget</span></h3>
               <div class="widget-content">Mauris at blandit erat. Nam vel tortor non quam scelerisque cursus. Praesent nunc vitae magna pellentesque auctor. Quisque id lectus.<br>
                  <br>
                  Massa, eget eleifend tellus. Proin nec ante leo ssim nunc sit amet velit malesuada pharetra. Nulla neque sapien, sollicitudin non ornare quis, malesuada.
               </div>
            </div>
         </aside>
    <?php } ?>
        </section>
    </div>
 
<?php get_footer(); ?>