<?php
/* Template name: Contact Template */
get_header(); ?>
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>
<?php the_content(); ?>
<?php 
wp_enqueue_script('revslider');
wp_enqueue_script('jquery.mobile-menu');
wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('owl.carousel.min');
wp_enqueue_script('jquery.mobile-menu.min');
wp_enqueue_script('jquery.countdown.min');
wp_enqueue_script('jquery.flexslider');
wp_enqueue_script('cloud-zoom');
wp_enqueue_script('jquery.magnific-popup.min');
wp_enqueue_script('moment');
wp_enqueue_script('bootstrap-datetimepicker.min');
 ?>

<?php get_footer(); ?>
