<?php
/* Template name: Web Shop */
get_header(); ?>

<div class="page-heading">
    <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
          <?php $TmFlavours->tmFlavours_breadcrumbs(); ?>
		  </div>
		  <div class="page-title"><h1 class="entry-title"><?=get_the_title();?></h1></div>
        </div>
      </div>
    </div>
</div>

<div class="main-container col1-layout wow bounceInUp animated">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1" id="content">
				<div class="page-content clearfix" style="margin-top: 20px;">
					<?php						
						if( have_rows('list') ): $services = 0;
							while ( have_rows('list') ) : the_row(); $services++; ?>								
								<div id="service-<?=$services;?>" class="clearfix">
									<div class="col-md-4"><img src="<?=the_sub_field('image');?>" alt="post img" class="pull-left img-responsive thumb margin10 img-thumbnail" style="width: 100%;     margin-top: 60px;"></div>
									<div class="col-md-8"><h3><?=the_sub_field('title');?></h3><?=the_sub_field('description');?></div>
									<p class="col-md-12 clearfix" style="margin-bottom: 40px; clear: both;"></p>		
								</div>
							<?php endwhile;
						endif;					
					?>					
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>