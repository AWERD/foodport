<?php
/* Template name: Wunschilste Template */
get_header(); ?>
<div class="page-heading" style="background-image: url('/wp-content/uploads/2016/12/apple-1281744_1920.jpg');">
	<div class="page-title">
		<h1 class="entry-title">Wunschilste</h1>
	</div>
</div>
<div class="main-container col1-layout wow bounceInUp animated" style="visibility: visible;padding-top: 50px;">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2" id="content">
				<div class="page-content"><?php the_content(); ?></div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>