<?php 
$TmFlavours = new TmFlavours();?>
 <?php tmFlavours_header_service(); ?>

 <footer>
  <div class="footer-inner">
     <div class="container">
       <div class="row">
          <div class="col-sm-12 col-xs-12 col-lg-8">
            <div class="footer-column pull-left">
               <h4>Customer Service</h4>
               <ul class="links">
                   <li class="first"><a href="index.phpїroute=informationЎcontact.html">Contact Us</a></li>
                   <li><a href="index.phpїroute=accountЎreturnЎadd.html">Returns</a></li>
                   <li><a href="index.phpїroute=accountЎwishlist.html">Wish List</a></li>
                   <li><a href="index.phpїroute=accountЎorder.html">Order History</a></li>
                   <li><a href="<?php echo get_permalink(5798); ?>">Site Map</a></li>
                   <li class="last"><a href="index.phpїroute=accountЎaccount.html" title="My Account">My Account</a></li>
                 </ul>
            </div>
            <div class="footer-column pull-left">
               <h4>Extras</h4>
                 <ul class="links">
                     <li class="first"><a href="index.phpїroute=productЎmanufacturer.html">Brands</a></li>
                     <li><a href="index.phpїroute=accountЎvoucher.html">Gift Certificates</a></li>
                     <li><a href="index.phpїroute=affiliateЎaccount.html">Affiliates</a></li>
                      <li><a href="<?php echo get_page_link(703); ?>">Blog</a></li>
                     <li><a href="index.phpїroute=accountЎorder.html">Order History</a></li>
                     <li class="last"><a href="index.phpїroute=productЎspecial.html">Specials</a></li>
                   </ul>
            </div>
            <div class="footer-column pull-left">
             <h4>Information</h4>
             <ul class="links">
               <li class="first"><a href="<?php echo get_permalink(4177); ?>">Contact Us</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=8.html">Delivery</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=7.html">Suppliers</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=4.html">About Us</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=6.html">Delivery Information</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=3.html">Privacy Policy</a></li>
               <li class=""><a href="index.phpїroute=informationЎinformation&information_id=5.html">Terms &amp; Conditions</a></li>
               </ul>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4">
            <div class="footer-column-last">
              <div class="newsletter-wrap">
                <h4>Sign Up for Our Newsletter</h4>
               <input  type="text" name="subscriber_email" id="subscriber_email" value="" placeholder="Enter Your Email" class="form-control input-text required-entry validate-email" />
               <button class="subscribe" type="button" name="submit_newsletter" id="submit_newsletter" onclick="return MgkEmailValidation()" ><span>Subscribe</span></button>
               <p id="subscriber_content" class="required"></p>
               <script language="javascript">
               function MgkEmailValidation(mail)   
               {  
                subscribemail = document.getElementById("subscriber_email").value;
                var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                if (subscribemail != '') { 

                    if ( subscribemail.search(emailRegEx)!=-1 ) {  
                      

                      email = document.getElementById("subscriber_email").value;
                      var xmlhttp;
                      if (window.XMLHttpRequest){
                        xmlhttp=new XMLHttpRequest();
                      }else{
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                      }
                      
                      xmlhttp.onreadystatechange=function() {
                        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                          document.getElementById("subscriber_content").innerHTML=xmlhttp.responseText;
                        }
                      }
                      xmlhttp.open("GET","index.php?route=extension/module/newslettersubscription/addsubscriberemail&email="+email,true);
                      xmlhttp.send();
                      return (true) ; 
                  }  
                    document.getElementById("subscriber_content").innerHTML="Please enter an email address.";
                    return (false); 
                }
                  document.getElementById("subscriber_content").innerHTML="This is a required field.";
                  return false;
               }  
               </script>
              </div>
              <div class="payment-accept">
                 <div>
                  <img src="<?php echo wp_get_attachment_url(12328); ?>" alt="payment1"> 
                  <img src="<?php echo wp_get_attachment_url(12327); ?>" alt="payment2"> 
                  <img src="<?php echo wp_get_attachment_url(12326); ?>" alt="payment3">
                  <img src="<?php echo wp_get_attachment_url(12325); ?>" alt="payment4"> 
                </div>
               </div>
             <div class="social">
                <h4>Follow Us</h4>
                   <ul>
                    <li class="fb pull-left"><a href="htttp://facebook.com/themessoft/" target="_blank"></a></li>
                     <li class="tw pull-left"><a href="http://twitter.com/themessoft/" target="_blank"></a></li>
                     <li class="googleplus pull-left"><a href="#" target="_blank"></a></li>
                     <li class="rss pull-left"><a href="#" target="_blank"></a></li>
                     <li class="pintrest pull-left"><a href="#" target="_blank"></a></li>
                     <li class="linkedin pull-left"><a href="#" target="_blank"></a></li>
                     <li class="youtube pull-left"><a href="#" target="_blank"></a></li>
                 </ul>
             </div>         
           </div>
           </div>
     </div>
   </div>
 </div>
 <div class="footer-middle">
     <div class="container">
       <div class="row">     
           <address>
              <i class="fa fa-map-marker"></i>ABC Town Luton Street, New York 226688                  
              <i class="fa fa-mobile"></i>
                <span> + 0800 567 345 </span>
              <i class="fa fa-envelope"></i>
              <span>support@themessoft.com</span>
            </address>
           </div>
          </div>
       </div>
 </footer>
 <div class="brand-logo">
  <div class="container">
      <div class="slider-items-products">
        <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col6"> 
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=8.html">
                  <img src="<?php echo wp_get_attachment_url(12324); ?>" alt="Apple">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=9.html">
                  <img src="<?php echo wp_get_attachment_url(12323); ?>" alt="Canon">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=7.html">
                  <img src="<?php echo wp_get_attachment_url(12322); ?>" alt="Hewlett-Packard">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=5.html">
                  <img src="<?php echo wp_get_attachment_url(12321); ?>" alt="HTC">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=6.html">
                  <img src="<?php echo wp_get_attachment_url(12320); ?>" alt="Palm">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=11.html">
                  <img src="<?php echo wp_get_attachment_url(12319); ?>" alt="Red Bull">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=8.html">
                  <img src="<?php echo wp_get_attachment_url(12324); ?>" alt="Apple">
                </a>
              </div>
              <div class="item"> 
                <a href="index.phpїroute=productЎmanufacturerЎinfo&manufacturer_id=9.html">
                  <img src="<?php echo wp_get_attachment_url(12323); ?>" alt="Canon">
                </a>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>
<?php wp_footer() ?>