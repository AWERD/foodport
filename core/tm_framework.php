<?php 
require_once(TMFLAVOURS_THEME_PATH . '/includes/layout.php');
require_once(TMFLAVOURS_THEME_PATH . '/core/resize.php');
require_once(TMFLAVOURS_THEME_PATH . '/includes/tm_menu.php');
require_once(TMFLAVOURS_THEME_PATH . '/includes/widget.php');
require_once(TMFLAVOURS_THEME_PATH . '/includes/tm_widget.php');
require_once(TMFLAVOURS_THEME_PATH .'/core/social_share.php');

function tmFlavours_simple_product_link()
{
  global $product,$class;
  $product_type = $product->product_type;
  $product_id=$product->id;
  if($product->price=='')
  { ?>
<a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span>NA</span>
    </a>
	<div style="height:32.5px;"></div>
<?php  }
  else{
  ?>
<a class="single_add_to_cart_button add_to_cart_button  product_type_simple ajax_add_to_cart button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>' data-quantity="1" data-product_id="<?php echo esc_attr($product->id); ?>"
      href='<?php echo esc_url($product->add_to_cart_url()); ?>'>
    <span><?php echo $product->get_price() . ' ' . get_option('woocommerce_currency'); ?><span class="wg" data-weight=""><span class="hide-small">| </span><?=$product->get_weight();?>kg</span></span>
</a>
<div class="input-group button-quantity">
                            <button type="button" onClick="addQTY(this)" class="btn btn-default btn-number btn-left" data-type="minus">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </button>
                        <input type="text" class="form-control input-number visible_qty" value="1" min="1" max="10">
                            <button type="button"  onClick="addQTY(this)" class="btn btn-default btn-number btn-right" data-type="plus" data-field="quant[1]">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                    </div>
<?php
}
}

function tmFlavours_bundel_link()
{
  global $product,$class;
  $product_type = $product->product_type;
  $product_id=$product->id;
  if($product->price=='')
  { ?>
<a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span>NA</span>
    </a>
	<div style="height:32.5px;"></div>
<?php  }
  else{
		$origin = $product->get_attribute( 'pa_origin' );
		if($origin == 'Fresspäckli'){
  ?>
<form class="cart" action="https://foodport.ch/checkout/?type=fresspackli" method="post" enctype="multipart/form-data">
	<input type="hidden" step="1" min="1" name="quantity" value="1" title="Qty" class="input-text qty text" size="4">
	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->id); ?>">
 	<button type="submit" class="single_add_to_cart_button add_to_cart_button  product_type_simple button btn-cart"><span><?php echo $product->get_price() . ' ' . get_option('woocommerce_currency'); ?><span class="wg" data-weight=""><span class="hide-small">/ </span><?=$product->get_weight();?>kg</span></span></button>
</form>
<?php
  } else { ?>
	  <a class="single_add_to_cart_button add_to_cart_button  product_type_simple ajax_add_to_cart button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>' data-quantity="1" data-product_id="<?php echo esc_attr($product->id); ?>"
      href='<?php echo esc_url($product->add_to_cart_url()); ?>'>
    <span><?php echo $product->get_price() . ' ' . get_option('woocommerce_currency'); ?><span class="wg" data-weight=""><span class="hide-small">| </span><?=$product->get_weight();?>kg</span></span>
</a>
<div style="height:32px;"></div>
  <?php }
}
}

function tmFlavours_allowedtags() {
    // Add custom tags to this string
        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<b>,<blockquote>,<strong>,<figcaption>'; 
    }

if ( ! function_exists( 'tmFlavours_wp_trim_excerpt' ) ) : 

    function tmFlavours_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, tmFlavours_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' '; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   

        }
        return apply_filters('tmFlavours_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'tmFlavours_wp_trim_excerpt');

 
?>