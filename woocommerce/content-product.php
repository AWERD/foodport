<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$TmFlavours = new TmFlavours();
global $product, $woocommerce_loop, $yith_wcwl;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
	$classes[] = 'item col-lg-3 col-md-3 col-sm-6 col-xs-12';
}
if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'item col-lg-3 col-md-3 col-sm-6 col-xs-12';
}
$netto = get_field('volume',  $product->id);
?>

<li <?php post_class('item col-lg-3 col-md-3 col-sm-6 col-xs-12', $classes); ?> class="item" >
   <div class="item-inner">
      <div class="item-img">
         <div class="item-img-info">
            <?php do_action('woocommerce_before_shop_loop_item'); ?>
            <div class="pimg">
            <a  href="<?php the_permalink(); ?>" class="product-image yith-wcqv-button quickview" data-product_id="<?php echo esc_html($product->id); ?>">
              
                  <?php
                     /**
                      * woocommerce_before_shop_loop_item_title hook
                      *
                      * @hooked woocommerce_show_product_loop_sale_flash - 10
                      * @hooked woocommerce_template_loop_product_thumbnail - 10
                      */
                     do_action('woocommerce_before_shop_loop_item_title');
					 
					 if ( $product->is_in_stock() ) {
						if ( has_term( 'bio', 'product_tag' ) ) echo '<img src="/wp-content/uploads/2016/11/bio-1.png" alt="Bio FoodPort.ch" class="quick-icon bio-food" title="Bio FoodPort.ch" />';
						if ( has_term( 'no pork', 'product_tag' ) ) echo '<img src="/wp-content/uploads/2016/11/no-pork-1-150x150.png" alt="No Pork FoodPort.ch" class="quick-icon no-pork" title="No Pork FoodPort.ch" />';
						if ( has_term( 'swiss', 'product_tag' ) ) echo '<img src="/wp-content/uploads/2016/11/swiss.png" alt="Swiss Produkt FoodPort.ch" class="quick-icon swiss" title="Swiss Produkt FoodPort.ch" />';
						if ( has_term( 'neu', 'product_tag' ) ) echo '<img src="/wp-content/uploads/2017/02/neu.png" style="position: absolute;left: 0;top: 0;width: 63px;">';
					 } else echo '<img src="/wp-content/themes/efortis/images/aus.png" style="position: absolute;left: 0;top: 0;width: 100%;top: 40px;">';
					 
					 if(!empty($product->sale_price)) echo '<div class="discount">' . number_format( 100 - (($product->sale_price / $product->regular_price) * 100) ) . '</div>';
					 
					 
                     ?>
             
            </a>
			<div class="rating">
                  <div class="ratings">
                     <div class="rating-box">
                        <?php $average = $product->get_average_rating(); ?>
                        <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"></div>
					 </div>
                  </div>
               </div>
			<?php
				/*
				$sqty = $product->get_stock_quantity();
				$ttitle = '';
				if(empty($sqty)){
					$sqty = 0;
					$waiting = get_field('waiting', $product->id);
					if(empty($waiting)) $waiting = 3;
					//$ttitle = 'Dieses Produkt wird in ' . $waiting . ' Arbeitstagen erhältlich sein, wenn Sie jetzt bestellen.';
					$ttitle = 'Wenn Sie jetzt bestellen, besorgen wir Ihnen dieses Produkt in spätestens ' . $waiting . ' Arbeitstagen';
				}
				if ( $product->is_in_stock() ){
			
			?>
			<div data-toggle="tooltip" data-placement="top" title="<?=$ttitle;?>" style="cursor:default; float: right; margin-top: -22px; z-index: 10; position: relative;"><i class="fa fa-cube" aria-hidden="true"></i> <?=$sqty;?></div>
				<?php } */ ?>
		  </div>
        
         <div class="item-box-hover">
             <div class="box-inner product-action">
              <div class="product-detail-bnt">
                   <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                  <a title="<?php esc_attr_e('Quick View', 'flavours'); ?>" class="button detail-bnt yith-wcqv-button quickview" type="button" data-product_id="<?php echo esc_html($product->id); ?>"><span><?php esc_attr_e('Quick View', 'flavours'); ?></span></a>
                  <?php } ?>
                </div> 
                <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
		        $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="link-wishlist"' : 'class="link-wishlist"';
	        ?>
		<a href="<?php echo esc_url($yith_wcwl->get_addtowishlist_url()) ?>"
	           data-product-id="<?php echo esc_html($product->id); ?>"
        	   data-product-type="<?php echo esc_html($product->product_type); ?>" <?php echo htmlspecialchars_decode($classes); ?>
	           title="<?php esc_attr_e('Add to WishList','flavours'); ?>"></a>
		<?php
	        }
	        
	        if (class_exists('YITH_Woocompare_Frontend')) {

        		$tm_yith_cmp = new YITH_Woocompare_Frontend;
          		$tm_yith_cmp->add_product_url($product->id);
	         }
	        ?>
                                  
	        
            </div>
         </div>
          </div>
      </div>
    <div class="item-info">
      <div class="info-inner">
            <div class="item-title"><a href="<?php the_permalink(); ?>" class="yith-wcqv-button quickview" data-product_id="<?php echo esc_html($product->id); ?>">
               <?php the_title(); ?>
               </a>
			   <br /><small style="text-transform: none;"><?php if($netto > 0){ ?>Volume: <?php $qty = get_field('qty_package',  $product->id); if($qty > 0) echo $qty . ' x '; ?><?=($netto / 1000);?>L<?php } else { ?> Netto: <?php $qty = get_field('qty_package',  $product->id); if($qty > 0) echo $qty . ' x '; ?><?=(get_field('netto_weight',  $product->id) / 1000);?>kg<?php } ?></small>
            </div>
			<?php
				$nutrition = get_field('nutrition-table', $product->id);			
			?>
            <div class="item-content">
				<div class="action-buttons">
					<?php $nut = get_field('nutrition-table'); $cl = 'disabled'; if(!empty($nut)) $cl = '';   ?><a href="<?=$nut;?>" data-lightbox="Nährwerte: <?php the_title(); ?>" data-title="Nährwerte: <?php the_title(); ?>" class=" <?=$cl;?>">Nährwerte</a>
					<?php $wiki = get_field('wikipedia'); if(!empty($wiki)) echo '<a target="_blank" href="' . $wiki . '">Wikipedia</a>'; ?>
				</div>
               <div class="item-price">
                  <div class="price-box"> <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                     <?php
                        /**
                         * woocommerce_after_shop_loop_item_title hook
                         *
                         * @hooked woocommerce_template_loop_rating - 5
                         * @hooked woocommerce_template_loop_price - 10
                         */
                        
                        ?>                   
                  </div>
               </div>
                 
               <div class="desc std">
                  <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
               </div>
               <div class="action">

                   <?php
                   $TmFlavours->tmFlavours_woocommerce_product_add_to_cart_text();
                   ?>
              </div>     
            </div>
         </div>
      </div>
   </div>
</li>
