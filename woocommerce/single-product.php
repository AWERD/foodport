<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>

<?php
/**
 * woocommerce_before_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action('woocommerce_before_main_content');
?>

<?php while (have_posts()) : the_post(); ?>

    <?php wc_get_template_part('content', 'single-product'); ?>

<?php endwhile; // end of the loop. ?>

<div id="slide-panel" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
    <?php dynamic_sidebar( 'category-right' ); ?>               
</div>

<?php
/**
 * woocommerce_after_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('woocommerce_after_main_content');
?>

<link rel='stylesheet' id='jpushMenu'  href="/wp-content/themes/efortis/addons/slide/jPushMenu.css" type='text/css' media='all' />
<script src="/wp-content/themes/efortis/addons/slide/jPushMenu.js"></script>
<script>
jQuery(document).ready(function($) {
	$('.toggle-menu').jPushMenu();
});
</script>

<?php
/**
 * woocommerce_sidebar hook
 *
 * @hooked woocommerce_get_sidebar - 10
 */

?>

<?php get_footer('shop'); ?>
