<?php
/**
 * Checkout terms and conditions checkbox
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( wc_get_page_id( 'terms' ) > 0 && apply_filters( 'woocommerce_checkout_show_terms', true ) ) : ?>
	<?php do_action( 'woocommerce_checkout_before_terms_and_conditions' ); ?>
	<p class="row" style="padding: 0 15px;">
		<?php
			echo '<div name="tnc" rows="10" style="height: 200px;width: 100%; overflow-y: scroll; text-align: left;" readonly>
				<strong>Anwendungsbereich</strong><br /><br />
				Alle Informationen und Regeln in diesen AGB beziehen sich auf die Website FoodPort.ch sowie auf andere Webseiten, die auf FoodPort.ch verweisen. Das Unternehmen FoodPort GmbH wird von den im Impressum aufgeführten juristischen oder natürlichen Person geführt. Ausdrücklich vorbehalten bleiben spezielle Regelungen von Rechtssubjekten in den Partnerschaften mit diversen Detailhändlern. Wir weisen darauf hin, dass wir das Recht vorbehalten Informationen, sowie die Regeln über die Nutzung unserer Dienstleistung jederzeit zu ändern. Diese Änderungen sind bindend.
				<br /><br /><br />

				<strong>Nutzungs-, Immaterialgüter- sowie andere Rechte</strong><br /><br />
				Vorbehältlich anderer Regelungen ist der Betreiber, FoodPort, Inhaber der Plattform. Alle auf der Plattform verwendeten Unternehmen, Marken, Namen, Bezeichnungen, Logos, Bilder, Designs, Texte und andere Materialien gehören jedoch nicht dem Betreiber der Plattform, sondern den aufgelisteten Partnern. Produktbilder, welche im Shop zu finden sind, gehören jedoch der FoodPort GmbH. Das Design der Plattform wurde von den Betreibern gestaltet und gehört somit der FoodPort. Nutzungs-, Immaterialgüterrechte sowie andere Rechte werden durch das Herunterladen, Kopieren oder Aufrufen nicht erworben. Ohne die schriftliche Zustimmung oder Bestätigung des Betreibers ist das Verknüpfen, Verbinden, Übermitteln, teilweise/ganz Reproduzieren, Abändern, Modifizieren oder Benutzen der Plattform für öffentliche oder kommerzielle Zwecke strengstens untersagt.
				<br /><br /><br />

				<strong>Haftungsausschluss</strong><br /><br />
				Der Betreiber garantiert nicht die Nutzung oder den Zugriff auf die Plattform. Des Weiteren versichert er auch nichts betreffend der Sicherung oder Archivierung der übermittelten Daten. FoodPort schliesst Fahrlässigkeit und jede Art Haftung aus, die sich aus dem Zugriff oder in Verbindung Setzung zwischen den Nutzern und der Plattform oder einzelner seiner Elemente und aus der Benutzung zusammensetzt. Alle auf der Plattform verlinkten Websites, welche nicht zu den Domänen der FoodPort gehören,  sind nicht Eigentum des Betreibers der Plattform. Aus diesem Grund werden diese Seiten nicht durch den Betreiber kontrolliert oder überwacht. Zugleich bedeutet das auch dass der Betreiber jegliche Verantwortung für den Inhalt und die Einhaltung der gesetzlichen Datenschutzbestimmungen durch die Betreiber von verlinkten Seiten ablehnt. Bei Lieferung mit Hilfe der Lieferpartner erfolgt keine Rücknahme von PET Flaschen. Glasflaschen mit und ohne Depot, Konservenverpackungen in Dosen und Glas sowie Verpackungskartons müssen durch den Kunden selbst entsorgt werden.
				<br /><br /><br />

				<strong>Datenverkehr durch das Internet</strong><br /><br />
				Der Betreiber schliesst jede Haftung für das Versenden und Übermitteln von Daten jeder Art über das Internet aus. Obwohl die Schweiz den Ruf als Land mit hohem Datenschutzniveau geniesst, ist nicht die Rede davon dass man grundsätzlich davon ausgehen kann, dass der Datenverkehr durch das Internet als risikolos eingestuft werden kann. Jedoch garantieren wir die Verschlüsselung aller versandten Datenpakete. Der Betreiber weist darauf hin, dass Daten auch im Ausland transferiert werden können.
				<br /><br /><br />

				<strong>Newsletter</strong><br /><br />
				Auf der Home Seite der Webseite des Betreibers können Kunden Newsletter abonnieren. Diese können selbstverständlich zu jeder Zeit gekündigt werden und weisen keine Verpflichtungen auf.
				<br /><br /><br />

				<strong>Sammlung und Verwendung von Informationen</strong><br /><br />
				Der Betreiber weist darauf hin, dass er nur zu statistischen Zwecken Daten ohne die Erlaubnis des Kunden auswerten kann, diese allerdings nicht weitergibt an Dritte. Jedoch werden Ausnahmen gemacht, wenn aussergewöhnliche Situationen auftreten wie z. B bei Eingriffen von Strafverfolgungsbehörden, welche gesetzlich über mehr Rechte und Macht als der Betreiber verfügen.  Das Speichern der Daten aller Kunden erfolgt, weil beim Zugreifen auf die Plattform sämtliche Zugriffsdaten, wie Z. B Datum, Uhrzeit, IP-Adresse etc.) gespeichert werden. Ohne Einverständnis des Kunden, welche nur durch separate Erklärung zu regeln ist, werden alle Daten vertraulich behandelt. Ansonsten bleiben alle Daten im Unternehmen der FoodPort.
				<br /><br /><br />

				<strong>Dienste und Dienste von Drittanbietern</strong><br /><br />
				Die Dienste von Drittanbietern, vor allem Instagram, Google Analytics und Facebook-«Like»/«gefällt mir», sind auf der Plattform des Betreibers vertreten. Der Betreiber weist darauf hin, dass diese Dienste oftmals Dateien, meistens in Form von Cookies, auf dem Computer der Kunden speichern können um somit das Surfverhalten zu analysieren. Unter anderem wird auch die IP-Adresse erfasst und gespeichert. Der Betreiber kann diese Dienste Ausnutzen um die eigene Website auszuwerten und um Statistiken zu erstellen. Mit diesen darf der Betreiber der Plattform dann interessenbezogene Werbung aufschalten und verwalten. Zu beachten ist dass diese Informationen ebenfalls an weitere Dritte im In- und Ausland übertragen werden. können und dürfen. Im Ausland besteht allenfalls kein angemessener gesetzlicher Datenschutz. Das Eingreifen dieser Datenbearbeitung lässt sich nur mit sehr grossem Aufwand verhindern und ist sich Sache des Betreibers der Plattform.Durch die Nutzung der Plattform erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden und nehmen zur Kenntnis, dass er Betreiber keinerlei Haftung übernimmt.
				<br /><br /><br />

				<strong>Datensicherheit</strong><br /><br />
				Der Unternehmer gewährleistet die sichere Speicherung personenbezogener Daten.
				<br /><br /><br />

				<strong>Preise von Produkten</strong><br /><br />
				Der Betreiber weist daraufhin,  dass die Mehrwertsteuer, welche 2.5%/8% entspricht, schon in allen Preisen einkalkuliert worden ist. Er ist auch bestrebt alle Preise möglichst rasch zu aktualisieren, jedoch kann es zu Verzögerungen kommen. Auf den Preisen wird nie ein Zuschlag verrechnet. Das heisst der Kunde bezahlt genau so viel wie er/sie im Supermarkt des jeweiligen Produktes bezahlen müsste.  Der Betreiber garantiert dass alle zusätzlichen Kosten fair und transparent zum Vorteil beider Parteien berechnet worden sind.
				<br /><br /><br />

				<strong>Verfügbarkeit und Qualität von Produkten</strong><br /><br />
				Produkte können nicht in übermengen bestellt werden. Zu beachten ist die jeweilige Bestell-limite, welche durch die Geschäftsleitung zu bestimmen ist. Keine auf der Plattform angebotenen Produkte sind Eigentum des Betreibers. FoodPort fungiert nur als Zwischenhändler. Aus diesem Grund versichert der Betreiber nicht die Qualität der Lebensmittel. Bei Fragen, Anregungen oder anderen wünschen steht der Betreiber der Plattform dem Kunden zur Verfügung, jedoch empfiehlt FoodPort dem Kunden direkten Kontakt zum Hersteller oder dem auf der Plattform aufgelisteten Anbieter aufzunehmen. Des Weiteren kann es vorkommen, dass Produkte nicht genau so aussehen wie auf dem Bild.
				<br /><br /><br />

				<strong>Scheitern der Lieferung aufgrund des Kunden</strong><br /><br />
				Ist der Kunde aufgrund seiner/ihrer Privaten Situation nicht wie vereinbart bereit zur Entgegennahme der bestellten Ware, lassen wir die Ware vor der Wohnung/vor dem Haus stehen. Der Betreiber übernimmt keine Haftung für den Zustand der Lebensmittel wenn der Kunde seine Lebensmittel nicht rechtzeitig entgegen nehmen kann. Mittels GPS-Daten und Fotos kann FoodPort genau angeben an welchen Standort, welches Paket beim Kunden deponiert wurde. Sobald die Ware beim Kunden abgelegt wurde, übernimmt FoodPort keine Haftung mehr.
				<br /><br /><br />

				<strong>Scheitern der Lieferung aufgrund des Betreibers</strong><br /><br />
				Ist der Betreiber aus diversen Gründen nicht wie vereinbart fähig die Ware innerhalb einer Zeitrahmens von 2-3 Arbeitstagen zu liefern, besteht kein Schadenersatzanspruch. Die effektiv gelieferte Menge wird auf dem definitiven Lieferschein und auf der Rechnung angegeben. Der Betreiber bietet keine Nachsendungen an.
				<br /><br /><br />

				<strong>Annulationsbedienungen</strong><br /><br />
				Bereits bestättigte Bestellungen können auf Grund des komplizierten Aufwandes nicht abgeändert oder gelöscht werden. Der Betreiber bemüht sich dies in Zukunft zu ändern.
			</div>';
		?>
	</p>
	<p class="form-row terms wc-terms-and-conditions">
		<input type="checkbox" class="input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" />
		<label for="terms" class="checkbox"><?php printf( __( 'I&rsquo;ve read and accept the <a href="%s" target="_blank">terms &amp; conditions</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'terms' ) ) ); ?> <span class="required">*</span></label>
		<input type="hidden" name="terms-field" value="1" />
	</p>
	<?php do_action( 'woocommerce_checkout_after_terms_and_conditions' ); ?>
<?php endif; ?>
