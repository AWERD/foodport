<?php
/**
 * Cart Page
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version 2.3.8
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $product, $woocommerce;
wc_print_notices();

do_action('woocommerce_before_cart'); ?>



       
<div class="col-main">
    <div class="cart wow bounceInUp">
        <form action="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" method="post">
        <div class="table-responsive shopping-cart-tbl row">
           
                <?php do_action('woocommerce_before_cart_table'); ?>
                <fieldset>
                    <table class="data-table cart-table" id="shopping-cart-table">
                        <thead>
                        <tr>
                            <td class="text-center">Image</td>
                            <td class="text-left">
                                <?php _e('Product Name', 'woocommerce'); ?>
                            </td>
                            <td class="text-left">Model</td>
                            <td class="text-left">Quantity</td>
                            <td colspan="1" class="a-center">
                                <span class="nobr">
                                    <?php _e('Unit Price', 'woocommerce'); ?>
                                </span>
                            </td>
                            <td colspan="1" class="a-center"><?php esc_attr_e('Subtotal', 'woocommerce'); ?></td>
                            <td class="a-center" rowspan="1">&nbsp;</td>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr class="first last">
                            <td class="a-right last" colspan="50">
                                <!-- <button
                                    onclick="location.href = '<?php echo esc_url(get_permalink(woocommerce_get_page_id('shop'))); ?>'"
                                    class="button btn-continue" title="<?php esc_attr_e('Continue Shopping', 'woocommerce'); ?>" type="button">
                                    <span>
                                        <?php esc_attr_e('Continue Shopping', 'flavours'); ?>
                                    </span>
                                </button>
                                <button type="submit" class="button btn-update" name="update_cart"
                                        value="<?php _e('Update Cart', 'flavours'); ?>">
                                    <span>
                                        <?php _e('Update Cart', 'woocommerce'); ?>
                                    </span>
                                </button>
                                <button id="empty_cart_button" class="button" title="<?php esc_attr_e('Clear Cart', 'flavours'); ?>" 
                                        name="clear-cart" type="submit"><span><?php esc_attr_e('Clear Cart', 'flavours'); ?></span></button> -->
                                <?php do_action('woocommerce_cart_actions'); ?>
                                <?php wp_nonce_field('woocommerce-cart'); ?>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php do_action('woocommerce_before_cart_contents'); ?>
                        <?php
                        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                                ?>
                                <tr class="<?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
                                    <td class="text-center"><?php
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(array(75, 75)), $_product->get_image(), $cart_item, $cart_item_key);
                                        if (!$_product->is_visible())
                                            echo $thumbnail;
                                        else
                                            printf('<a href="%s" class="product-image">%s</a>', esc_url($_product->get_permalink($cart_item)), $thumbnail);
                                        ?>
                                    </td>
                                    <td class="text-left">
                                            <?php
                                            if (!$_product->is_visible())
                                                echo apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
                                            else
                                                echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', $_product->get_permalink($cart_item), $_product->get_title()), $cart_item, $cart_item_key);

                                            // Meta data
                                            echo WC()->cart->get_item_data($cart_item);

                                            // Backorder notification
                                            if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity']))
                                                echo '<p class="backorder_notification">' . __('Available on backorder', 'woocommerce') . '</p>';
                                            ?>
                                    </td>
                                    <td class="text-left">Product 1</td>
                                    <td class="text-left">
                                        <div class="input-group btn-block" style="max-width: 200px;">
                                        <?php
                                        if ($_product->is_sold_individually()) {
                                            $product_quantity = sprintf('1 <input type="hidden" class="input-text qty name="cart[%s][qty]" value="1" />', $cart_item_key);
                                        } else {
                                            $product_quantity = woocommerce_quantity_input(array(
                                                'input_name' => "cart[{$cart_item_key}][qty]",
                                                'input_value' => $cart_item['quantity'],
                                                'max_value' => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                                'min_value' => '0'
                                            ), $_product, false);
                                        }

                                        echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key);

                                        // echo apply_filters('woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="button remove-item" title="%s"><span><span>Remove item</span></span></a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), __('Remove this item', 'woocommerce')), $cart_item_key);
                                        ?>
                                        <span class="input-group-btn">
                                             <button type="submit" class="update-button button custom-update" name="update_cart" value="<?php _e('Update Shopping Bag', 'theretailer'); ?>" ><i class="fa fa-refresh"></i></button>
                                            <?php
                                            echo apply_filters('woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="" title="%s"><button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Remove"><i class="fa fa-times-circle"></i></button></a>', esc_url(WC()->cart->get_remove_url($cart_item_key)), __('Remove this item', 'woocommerce')), $cart_item_key);
                                            ?>
                                        </div>
                                    </td>
                                    <td class="text-left">
                                        <span class="cart-price">
                                            <span class="price">
                                            <?php
                                            echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                                            ?>
                                            </span>
                                        </span>
                                    </td>
                                    <td class="a-center movewishlist">
                                        <span class="cart-price">
                                            <span class="price">
                                            <?php
                                            echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
                                            ?>
                                            </span>
                                        </span>
                                    </td>
                                    <td class="a-center last"><?php
                                        
                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }

                        do_action('woocommerce_cart_contents');
                        ?>
                        <?php do_action('woocommerce_after_cart_contents'); ?>
                        </tbody>
                    </table>
                </fieldset>
                 </div>
                <?php do_action('woocommerce_after_cart_table'); ?>

                <!-- BEGIN CART COLLATERALS -->
                <div class="row">
                   <div class="col-sm-8 col-xs-12">
                      <h2 class="custom-h2">What would you like to do next?</h2>
                      <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                      <div class="panel-group" id="accordion">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-coupon" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Use Coupon Code <i class="fa fa-caret-down"></i></a></h4>
                            </div>
                            <div id="collapse-coupon" class="panel-collapse collapse">
                               <div class="panel-body">
                                    <?php if (WC()->cart->coupons_enabled()) { ?>
                                        <div class="discount">
                                            <div id="discount-coupon-form">
                                                <label for="coupon_code">
                                                    <?php _e('Enter your coupon here', 'woocommerce'); ?>
                                                    :</label>
                                                <input type="text" name="coupon_code" class="input-text fullwidth" id="coupon_code"
                                                       value="" placeholder="<?php _e('Coupon code', 'woocommerce'); ?>"/>
                                                <span>
                                                    <button type="submit" class="button coupon-apply" name="apply_coupon" value="<?php _e('Apply Coupon', 'woocommerce'); ?>"/>
                                                    <span>
                                                        <?php _e('Apply Coupon', 'woocommerce'); ?>
                                                    </span>
                                                    </button>
                                                </span>
                                                <?php do_action('woocommerce_cart_coupon'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                  <!-- <label class="col-sm-2 control-label" for="input-coupon">Enter your coupon here</label>
                                  <div class="input-group">
                                    <?php if (WC()->cart->coupons_enabled()) { ?>
                                     <input type="text" name="coupon_code" value="" placeholder="Enter your coupon here" id="coupon_code" class="form-control">
                                     <span class="input-group-btn">
                                     <input type="button" value="Apply Coupon" id="button-coupon" data-loading-text="Loading..." class="btn btn-primary" value="<?php _e('Apply Coupon', 'woocommerce'); ?>">
                                     </span>
                                     <?php do_action('woocommerce_cart_coupon'); ?>
                                     <?php } ?>
                                  </div> -->
                               </div>
                            </div>
                         </div>
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-shipping" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Estimate Shipping &amp; Taxes <i class="fa fa-caret-down"></i></a></h4>
                            </div>
                            <div id="collapse-shipping" class="panel-collapse collapse">
                               <div class="panel-body">
                                  <p>Enter your destination to get a shipping estimate.</p>
                                  <div class="form-horizontal">
                                     <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-country">Country</label>
                                        <div class="col-sm-10">
                                           <select name="country_id" id="input-country" class="form-control">
                                              <option value=""> --- Please Select --- </option>
                                              
                                              <option value="239">Zimbabwe</option>
                                           </select>
                                        </div>
                                     </div>
                                     <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-zone">Region / State</label>
                                        <div class="col-sm-10">
                                           <select name="zone_id" id="input-zone" class="form-control">
                                              <option value=""> --- Please Select --- </option>
                                              <option value="3452">Adjumani</option>
                                              <option value="3464">Yumbe</option>
                                           </select>
                                        </div>
                                     </div>
                                     <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-postcode">Post Code</label>
                                        <div class="col-sm-10">
                                           <input type="text" name="postcode" value="567675" placeholder="Post Code" id="input-postcode" class="form-control">
                                        </div>
                                     </div>
                                     <button type="button" id="button-quote" data-loading-text="Loading..." class="btn btn-primary">Get Quotes</button>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-voucher" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Use Gift Certificate <i class="fa fa-caret-down"></i></a></h4>
                            </div>
                            <div id="collapse-voucher" class="panel-collapse collapse">
                               <div class="panel-body">
                                  <label class="col-sm-2 control-label" for="input-voucher">Enter your gift certificate code here</label>
                                  <div class="input-group">
                                     <input type="text" name="voucher" value="" placeholder="Enter your gift certificate code here" id="input-voucher" class="form-control">
                                     <span class="input-group-btn">
                                     <input type="submit" value="Apply Gift Certificate" id="button-voucher" data-loading-text="Loading..." class="btn btn-primary">
                                     </span> 
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="col-sm-4 col-xs-12">
                      <table class="table table-bordered">
                         <tbody>
                            <tr>
                               <td class="text-right"><strong>Sub-Total:</strong></td>
                               <td class="text-right"><?php wc_cart_totals_subtotal_html(); ?></td>
                            </tr>
                            <tr>
                               <td class="text-right"><strong>Flat Shipping Rate:</strong></td>
                               <td class="text-right">

                               <!--  <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
                               
                                   <?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
                               
                                   <?php wc_cart_totals_shipping_html(); ?>
                               
                                   <?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
                               <?php endif; ?> -->
                            </tr>
                            <tr>
                               <td class="text-right"><strong>Total:</strong></td>
                               <td class="text-right"><?php wc_cart_totals_order_total_html(); ?></td>
                            </tr>
                         </tbody>
                      </table>
                   </div>
                </div>
                <div class="buttons clearfix">
                  <div class="pull-left">
                    <a href="/"><button class="button btn-continue" id="btn-continue" type="button"><span><span>Continue Shopping</span></span></button></a>
                  </div>
                  <div class="pull-right">
                    <a href="/checkout"><button class="button btn-proceed-checkout" type="button"><span>Checkout</span></button></a>
                  </div>
                </div>

                <!-- <div class="cart-collaterals row">
                
                    <div class="col-sm-6">
                        <?php if (WC()->cart->coupons_enabled()) { ?>
                            <div class="discount">
                                <h3><?php _e('Discount Codes', 'flavours'); ?></h3>
                
                                <div id="discount-coupon-form">
                                    <label for="coupon_code">
                                        <?php _e('Enter your coupon code if you have one.', 'woocommerce'); ?>
                                        :</label>
                                    <input type="text" name="coupon_code" class="input-text fullwidth" id="coupon_code"
                                           value="" placeholder="<?php _e('Coupon code', 'woocommerce'); ?>"/>
                                    <button type="submit" class="button" name="apply_coupon"
                                            value="<?php _e('Apply Coupon', 'woocommerce'); ?>"/>
                <span>
                <?php _e('Apply Coupon', 'woocommerce'); ?>
                </span>
                                    </button>
                                    <?php do_action('woocommerce_cart_coupon'); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <?php woocommerce_cart_totals(); ?>
                    </div>
                </div> -->

            </form>
        
    </div>
    <!-- begin cart-collaterals-->
    <?php do_action('woocommerce_cart_collaterals'); ?>
    <!--cart-collaterals-->
    <?php do_action('woocommerce_after_cart'); ?>
</div>
