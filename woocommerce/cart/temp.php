<section class="main-container col1-layout">
   <div class="main container">
      <div class="col-main">
         <div class="cart wow bounceInUp animated">
            <div class="row">
               <div id="content" class="col-sm-12">
                  <div class="page-title">
                     <h2>Shopping Cart</h2>
                  </div>
                  <form action="http://occrocus3.themessoft.com/index.php?route=checkout/cart/edit" method="post" enctype="multipart/form-data">
                     <div class="table-responsive">
                        <table class="data-table cart-table" id="shopping-cart-table">
                           <thead>
                              <tr>
                                 <td class="text-center">Image</td>
                                 <td class="text-left">Product Name</td>
                                 <td class="text-left">Model</td>
                                 <td class="text-left">Quantity</td>
                                 <td class="text-right">Unit Price</td>
                                 <td class="text-right">Total</td>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td class="text-center">
                                    <a href="http://occrocus3.themessoft.com/index.php?route=product/product&amp;product_id=49">
                                    <img src="./Shopping Cart_files/f14-75x91.jpg" alt="Retis lapen casen" title="Retis lapen casen" class="img-thumbnail">
                                    </a>
                                 </td>
                                 <td class="text-left">
                                    <a href="http://occrocus3.themessoft.com/index.php?route=product/product&amp;product_id=49">Retis lapen casen</a>
                                    <br>
                                    <small>Reward Points: 400</small>
                                 </td>
                                 <td class="text-left">Product 1</td>
                                 <td class="text-left">
                                    <div class="input-group btn-block" style="max-width: 200px;">
                                       <input type="text" name="quantity[922]" value="1" size="1" class="form-control">
                                       <span class="input-group-btn">
                                       <button type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button>
                                       <button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="cart.remove(&#39;922&#39;);" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                                       </span>
                                    </div>
                                 </td>
                                 <td class="text-right">£77.63</td>
                                 <td class="text-right">£77.63</td>
                              </tr>
                              <tr>
                                 <td class="text-center">
                                    <a href="http://occrocus3.themessoft.com/index.php?route=product/product&amp;product_id=64">
                                    <img src="./Shopping Cart_files/f10-75x91.jpg" alt="Retis lapen casen" title="Retis lapen casen" class="img-thumbnail">
                                    </a>
                                 </td>
                                 <td class="text-left">
                                    <a href="http://occrocus3.themessoft.com/index.php?route=product/product&amp;product_id=64">Retis lapen casen</a>
                                 </td>
                                 <td class="text-left">Product 19</td>
                                 <td class="text-left">
                                    <div class="input-group btn-block" style="max-width: 200px;">
                                       <input type="text" name="quantity[923]" value="1" size="1" class="form-control">
                                       <span class="input-group-btn">
                                       <button type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button>
                                       <button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="cart.remove(&#39;923&#39;);" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                                       </span>
                                    </div>
                                 </td>
                                 <td class="text-right">£776.30</td>
                                 <td class="text-right">£776.30</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </form>
                  <div class="row">
                     <div class="col-sm-8 col-xs-12">
                        <h2>What would you like to do next?</h2>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                        <div class="panel-group" id="accordion">
                           <div class="panel panel-default">
                              <div class="panel-heading">
                                 <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-coupon" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Use Coupon Code <i class="fa fa-caret-down"></i></a></h4>
                              </div>
                              <div id="collapse-coupon" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <label class="col-sm-2 control-label" for="input-coupon">Enter your coupon here</label>
                                    <div class="input-group">
                                       <input type="text" name="coupon" value="" placeholder="Enter your coupon here" id="input-coupon" class="form-control">
                                       <span class="input-group-btn">
                                       <input type="button" value="Apply Coupon" id="button-coupon" data-loading-text="Loading..." class="btn btn-primary">
                                       </span>
                                    </div>
                                    <script type="text/javascript"><!--
                                       $('#button-coupon').on('click', function() {
                                           $.ajax({
                                               url: 'index.php?route=extension/total/coupon/coupon',
                                               type: 'post',
                                               data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
                                               dataType: 'json',
                                               beforeSend: function() {
                                                   $('#button-coupon').button('loading');
                                               },
                                               complete: function() {
                                                   $('#button-coupon').button('reset');
                                               },
                                               success: function(json) {
                                                   $('.alert').remove();
                                       
                                                   if (json['error']) {                
                                                       $('#content').parent().before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                       
                                                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                                                   }
                                       
                                                   if (json['redirect']) {
                                                       location = json['redirect'];
                                                   }
                                               }
                                           });
                                       });
                                       //-->
                                    </script>
                                 </div>
                              </div>
                           </div>
                           <div class="panel panel-default">
                              <div class="panel-heading">
                                 <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-shipping" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Estimate Shipping &amp; Taxes <i class="fa fa-caret-down"></i></a></h4>
                              </div>
                              <div id="collapse-shipping" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <p>Enter your destination to get a shipping estimate.</p>
                                    <div class="form-horizontal">
                                       <div class="form-group required">
                                          <label class="col-sm-2 control-label" for="input-country">Country</label>
                                          <div class="col-sm-10">
                                             <select name="country_id" id="input-country" class="form-control">
                                                <option value=""> --- Please Select --- </option>
                                                
                                                <option value="239">Zimbabwe</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="form-group required">
                                          <label class="col-sm-2 control-label" for="input-zone">Region / State</label>
                                          <div class="col-sm-10">
                                             <select name="zone_id" id="input-zone" class="form-control">
                                                <option value=""> --- Please Select --- </option>
                                                <option value="3452">Adjumani</option>
                                                <option value="3464">Yumbe</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="col-sm-2 control-label" for="input-postcode">Post Code</label>
                                          <div class="col-sm-10">
                                             <input type="text" name="postcode" value="567675" placeholder="Post Code" id="input-postcode" class="form-control">
                                          </div>
                                       </div>
                                       <button type="button" id="button-quote" data-loading-text="Loading..." class="btn btn-primary">Get Quotes</button>
                                    </div>
                                    <script type="text/javascript"><!--
                                       $('#button-quote').on('click', function() {
                                           $.ajax({
                                               url: 'index.php?route=extension/total/shipping/quote',
                                               type: 'post',
                                               data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
                                               dataType: 'json',
                                               beforeSend: function() {
                                                   $('#button-quote').button('loading');
                                               },
                                               complete: function() {
                                                   $('#button-quote').button('reset');
                                               },
                                               success: function(json) {
                                                   $('.alert, .text-danger').remove();
                                       
                                                   if (json['error']) {
                                                       if (json['error']['warning']) {
                                                           $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                       
                                                           $('html, body').animate({ scrollTop: 0 }, 'slow');
                                                       }
                                       
                                                       if (json['error']['country']) {
                                                           $('select[name=\'country_id\']').after('<div class="text-danger">' + json['error']['country'] + '</div>');
                                                       }
                                       
                                                       if (json['error']['zone']) {
                                                           $('select[name=\'zone_id\']').after('<div class="text-danger">' + json['error']['zone'] + '</div>');
                                                       }
                                       
                                                       if (json['error']['postcode']) {
                                                           $('input[name=\'postcode\']').after('<div class="text-danger">' + json['error']['postcode'] + '</div>');
                                                       }
                                                   }
                                       
                                                   if (json['shipping_method']) {
                                                       $('#modal-shipping').remove();
                                       
                                                       html  = '<div id="modal-shipping" class="modal">';
                                                       html += '  <div class="modal-dialog">';
                                                       html += '    <div class="modal-content">';
                                                       html += '      <div class="modal-header">';
                                                       html += '        <h4 class="modal-title">Please select the preferred shipping method to use on this order.</h4>';
                                                       html += '      </div>';
                                                       html += '      <div class="modal-body">';
                                       
                                                       for (i in json['shipping_method']) {
                                                           html += '<p><strong>' + json['shipping_method'][i]['title'] + '</strong></p>';
                                       
                                                           if (!json['shipping_method'][i]['error']) {
                                                               for (j in json['shipping_method'][i]['quote']) {
                                                                   html += '<div class="radio">';
                                                                   html += '  <label>';
                                       
                                                                   if (json['shipping_method'][i]['quote'][j]['code'] == 'flat.flat') {
                                                                       html += '<input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" />';
                                                                   } else {
                                                                       html += '<input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" />';
                                                                   }
                                       
                                                                   html += json['shipping_method'][i]['quote'][j]['title'] + ' - ' + json['shipping_method'][i]['quote'][j]['text'] + '</label></div>';
                                                               }
                                                           } else {
                                                               html += '<div class="alert alert-danger">' + json['shipping_method'][i]['error'] + '</div>';
                                                           }
                                                       }
                                       
                                                       html += '      </div>';
                                                       html += '      <div class="modal-footer">';
                                                       html += '        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>';
                                       
                                                                       html += '        <input type="button" value="Apply Shipping" id="button-shipping" data-loading-text="Loading..." class="btn btn-primary" />';
                                                       
                                                       html += '      </div>';
                                                       html += '    </div>';
                                                       html += '  </div>';
                                                       html += '</div> ';
                                       
                                                       $('body').append(html);
                                       
                                                       $('#modal-shipping').modal('show');
                                       
                                                       $('input[name=\'shipping_method\']').on('change', function() {
                                                           $('#button-shipping').prop('disabled', false);
                                                       });
                                                   }
                                               },
                                               error: function(xhr, ajaxOptions, thrownError) {
                                                   alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                               }
                                           });
                                       });
                                       
                                       $(document).delegate('#button-shipping', 'click', function() {
                                           $.ajax({
                                               url: 'index.php?route=extension/total/shipping/shipping',
                                               type: 'post',
                                               data: 'shipping_method=' + encodeURIComponent($('input[name=\'shipping_method\']:checked').val()),
                                               dataType: 'json',
                                               beforeSend: function() {
                                                   $('#button-shipping').button('loading');
                                               },
                                               complete: function() {
                                                   $('#button-shipping').button('reset');
                                               },
                                               success: function(json) {
                                                   $('.alert').remove();
                                       
                                                   if (json['error']) {                
                                                       $('#content').parent().before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                       
                                                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                                                   }
                                       
                                                   if (json['redirect']) {
                                                       location = json['redirect'];
                                                   }
                                               },
                                               error: function(xhr, ajaxOptions, thrownError) {
                                                   alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                               }
                                           });
                                       });
                                       //-->
                                    </script>
                                    <script type="text/javascript"><!--
                                       $('select[name=\'country_id\']').on('change', function() {
                                           $.ajax({
                                               url: 'index.php?route=extension/total/shipping/country&country_id=' + this.value,
                                               dataType: 'json',
                                               beforeSend: function() {
                                                   $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
                                               },
                                               complete: function() {
                                                   $('.fa-spin').remove();
                                               },
                                               success: function(json) {
                                                   if (json['postcode_required'] == '1') {
                                                       $('input[name=\'postcode\']').parent().parent().addClass('required');
                                                   } else {
                                                       $('input[name=\'postcode\']').parent().parent().removeClass('required');
                                                   }
                                       
                                                   html = '<option value=""> --- Please Select --- </option>';
                                       
                                                   if (json['zone'] && json['zone'] != '') {
                                                       for (i = 0; i < json['zone'].length; i++) {
                                                           html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                                       
                                                           if (json['zone'][i]['zone_id'] == '3442') {
                                                               html += ' selected="selected"';
                                                           }
                                       
                                                           html += '>' + json['zone'][i]['name'] + '</option>';
                                                       }
                                                   } else {
                                                       html += '<option value="0" selected="selected"> --- None --- </option>';
                                                   }
                                       
                                                   $('select[name=\'zone_id\']').html(html);
                                               },
                                               error: function(xhr, ajaxOptions, thrownError) {
                                                   alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                               }
                                           });
                                       });
                                       
                                       $('select[name=\'country_id\']').trigger('change');
                                       //-->
                                    </script>
                                 </div>
                              </div>
                           </div>
                           <div class="panel panel-default">
                              <div class="panel-heading">
                                 <h4 class="panel-title"><a href="http://occrocus3.themessoft.com/#collapse-voucher" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Use Gift Certificate <i class="fa fa-caret-down"></i></a></h4>
                              </div>
                              <div id="collapse-voucher" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <label class="col-sm-2 control-label" for="input-voucher">Enter your gift certificate code here</label>
                                    <div class="input-group">
                                       <input type="text" name="voucher" value="" placeholder="Enter your gift certificate code here" id="input-voucher" class="form-control">
                                       <span class="input-group-btn">
                                       <input type="submit" value="Apply Gift Certificate" id="button-voucher" data-loading-text="Loading..." class="btn btn-primary">
                                       </span> 
                                    </div>
                                    <script type="text/javascript"><!--
                                       $('#button-voucher').on('click', function() {
                                         $.ajax({
                                           url: 'index.php?route=extension/total/voucher/voucher',
                                           type: 'post',
                                           data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
                                           dataType: 'json',
                                           beforeSend: function() {
                                             $('#button-voucher').button('loading');
                                           },
                                           complete: function() {
                                             $('#button-voucher').button('reset');
                                           },
                                           success: function(json) {
                                             $('.alert').remove();
                                       
                                             if (json['error']) {        
                                               $('#content').parent().before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                                       
                                               $('html, body').animate({ scrollTop: 0 }, 'slow');
                                             }
                                       
                                             if (json['redirect']) {
                                               location = json['redirect'];
                                             }
                                           }
                                         });
                                       });
                                       //-->
                                    </script>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4 col-xs-12">
                        <table class="table table-bordered">
                           <tbody>
                              <tr>
                                 <td class="text-right"><strong>Sub-Total:</strong></td>
                                 <td class="text-right">£853.93</td>
                              </tr>
                              <tr>
                                 <td class="text-right"><strong>Flat Shipping Rate:</strong></td>
                                 <td class="text-right">£3.88</td>
                              </tr>
                              <tr>
                                 <td class="text-right"><strong>Total:</strong></td>
                                 <td class="text-right">£857.81</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="buttons clearfix">
                     <div class="pull-left">
                        <!-- <a href="" class="btn btn-default"></a> -->
                        <button onclick="window.location=&#39;http://occrocus3.themessoft.com/index.php?route=common/home&#39;" class="button btn-continue" type="button"><span><span>Continue Shopping</span></span></button>
                     </div>
                     <div class="pull-right">
                        <!-- <a href="" class="btn btn-primary"></a> -->
                        <button onclick="window.location=&#39;http://occrocus3.themessoft.com/index.php?route=checkout/checkout&#39;" class="button btn-proceed-checkout" type="button"><span>Checkout</span></button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>