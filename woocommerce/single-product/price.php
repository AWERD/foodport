<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$weight = '';
$weight = get_field('netto_weight', $product->id);
if($weight != '') $weight = ' <small style="text-transform: none;margin-left: 36px;" class="weight">Gewicht: '.$weight.'g</small>';
$netto = get_field('volume',  $product->id);
$ablaufdatum = get_field('ablaufdatum',  $product->id);
$ablaufdatumT = '';
if(!empty($ablaufdatum)) $ablaufdatumT = '<small style="display: block; font-weight: bold; margin-bottom: 20px; color: red;">Ablaufdatum: '.$ablaufdatum.'</small>';
?>

<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="price-block">
    <div class="price-box price"> <?php echo $product->get_price_html() . $weight ; ?>
	<br /><small style="text-transform: none;font-weight: bold;"><?php if($netto > 0){ ?>Volume: <?php $qty = get_field('qty_package',  $product->id); if($qty > 0) echo $qty . ' x '; ?><?=($netto / 1000);?>L<?php } ?></small>
	<br /><small style="display: inline-block; text-transform: none;">Lieferzeit: "1-2 Arbeitstage"</small>
	<br /><?=$ablaufdatumT;?>
	</div>

        <?php echo $product->is_in_stock() ? '<p class="availability in-stock pull-right"><span>Auf Lager</span></p>' : '<p class="availability out-of-stock pull-right"><span>Out of Stock</span></p>'; ?>
		
        <meta itemprop="price" content="<?php echo $product->get_price(); ?>"/>
        <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>"/>
        <link itemprop="availability"
              href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>"/>
    </div>

