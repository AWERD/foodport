<?php
/**
 * Single Product Image
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.14
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;


?>

<div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
   <div class="product-image">
      <div class="product-full"> <img id="product-zoom" src="<?php the_post_thumbnail_url(); ?> " data-zoom-image="http://occrocus3.themessoft.com/image/cache/catalog/f14-800x800.jpg"/> 
      </div>
      <div class="more-views">
         <div class="slider-items-products">
            <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
               <div class="slider-items slider-width-col4 block-content">
                <?php
                    global $product;

                    $attachment_ids = $product->get_gallery_attachment_ids();

                    foreach( $attachment_ids as $attachment_id ) { ?>
                        <div class="more-views-items"> <a href="image/cache/catalog/f14-800x800.jpg" data-image="http://occrocus3.themessoft.com/image/cache/catalog/f14-800x800.jpg" data-zoom-image="http://occrocus3.themessoft.com/image/cache/catalog/f14-800x800.jpg"> <img id="product-zoom"  src="<?php wp_get_attachment_url( $attachment_id ); ?>" /> </a></div>
                <?php
                    }
                ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

           <!--  <div class="product-image">
                <div class="large-image">
                    <?php
                    if (has_post_thumbnail()) {

                        $image_title = esc_html(get_the_title(get_post_thumbnail_id()));
                        $image_link = wp_get_attachment_url(get_post_thumbnail_id());
                        $image = get_the_post_thumbnail($post->ID, apply_filters('single_product_large_thumbnail_size', 'shop_single'), array(
                            'title' => $image_title
                        ));

                        $attachment_count = count($product->get_gallery_attachment_ids());

                        if ($attachment_count > 0) {
                            $gallery = '[product-gallery]';
                        } else {
                            $gallery = '';
                        }

                        echo apply_filters('woocommerce_single_product_image_html', sprintf('<a href="%s" onClick="return false" data-lightbox="gallery" itemprop="image" class="woocommerce-main-image zoom cloud-zoom" data-zoom-image="'. esc_url($image_link) .'" title="%s" id="zoom1">%s</a>', esc_url($image_link), esc_html($image_title), $image), $post->ID);

                    } else {

                        echo apply_filters('woocommerce_single_product_image_html', sprintf('<img src="%s" onClick="return false" alt="%s" />', esc_url(wc_placeholder_img_src()), __('Placeholder', 'woocommerce')), $post->ID);

                    }
                    ?>
                </div>
                <?php do_action('woocommerce_product_thumbnails'); ?>
            </div> -->
            <!-- end: more-images -->
           
      