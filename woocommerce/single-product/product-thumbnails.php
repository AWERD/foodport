<?php
/**
 * Single Product Thumbnails
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ($attachment_ids) {
    $loop = 0;
    $columns = apply_filters('woocommerce_product_thumbnails_columns', 3);
    ?>
    
        <div class="flexslider flexslider-thumb">
            <ul class="previews-list slides">
                <?php

                foreach ($attachment_ids as $attachment_id) {

                    $classes = array('');

                    if ($loop == 0 || $loop % $columns == 0)
                        $classes[] = 'first';

                    if (($loop + 1) % $columns == 0)
                        $classes[] = 'last';

                     $image_link = wp_get_attachment_url($attachment_id);

                    if (!$image_link)
                        continue;

                    $image = wp_get_attachment_image($attachment_id, apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail'));
                    $image_class = esc_html(implode(' ', $classes));
                    $image_title = esc_html(get_the_title($attachment_id));
                    $rel="gallery";
                    echo apply_filters('woocommerce_single_product_image_thumbnail_html', sprintf('<li><a href="%s" class="%s" title="%s" data-lightbox="%s">%s</a></li>', esc_html($image_link), $image_class, esc_html($image_title), $rel, $image), $attachment_id, $post->ID, $image_class);
                    ?>
                   

          
                    <?php
                    $loop++;
                }

                ?>
            </ul>
        </div>
    <?php

}

