<?php
/**
 * Simple product add to cart
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

if (!$product->is_purchasable()) {
    return;
}

?>





<?php if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>
    <div class="add-to-box">
        <div class="add-to-cart">


            <form class="cart" method="post" enctype='multipart/form-data'>
                <?php do_action('woocommerce_before_add_to_cart_button'); ?>
                <div class="pull-left">
                    <div class="custom pull-left">
                        <?php
                        if (!$product->is_sold_individually())
                            woocommerce_quantity_input(array(
                                'min_value' => apply_filters('woocommerce_quantity_input_min', 1, $product),
                                'max_value' => apply_filters('woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product)
                            ));
                        ?>

                        <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->id); ?>"/>
                    </div>
                </div>
                <div>
                    <button type="submit" class="single_add_to_cart_button button alt button btn-cart"><span><i
                                class="icon-basket"></i> <?php echo $product->single_add_to_cart_text(); ?></span>
                    </button>
                </div>
				<?php
					$user = get_current_user_id();
					$field = get_field('buying_lists', 'user_' . $user);
					$ids = explode(", ", $field);
					if(!empty($field)){
				?>
				<div class="clearfix buying-list-add">
					<select name="buying_list" class="pull-left">
						<option value="0" disabled selected="selected">Einkaufsliste wählen</option>
						<?php
							foreach($ids as $id){
								if($id){
									$term = get_term($id);
									echo '<option value="'.$term->name.'">'.$term->name.'</option>';
								}
							}
						?>
					</select>
					<a href="#" class="button btn-default btn-add-to-ein" onClick="addToEin(event, this, <?php echo esc_attr($product->id); ?>)">In die Einkaufsliste</a>
				</div>
				
				<?php } ?>
				<?php
					$terms = get_the_terms( $product->ID, 'product_cat' );
					foreach ($terms as $term) {
						if($term->slug == 'alkoholhaltige-getraenke'){
							echo '<p style="clear: both; padding-top: 20px;font-size: 11px;"><strong>Dieses Produkt wird nur an Kunden/innen, welche das 18. Lebensjahr erreicht haben, verkauft. Sind Sie noch nicht 18 Jahre alt, bitten wir Sie den Kauf dieses Produktes zu vermeiden. Wir übernehmen keine Haftung.</strong></p>';
							break;
						}						
					}
				
				?>
                <?php do_action('woocommerce_after_add_to_cart_button'); ?>
            </form>
        </div>
    </div>
    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>
