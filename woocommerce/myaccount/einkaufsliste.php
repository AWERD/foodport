<?php
/**
 * Einkaufsliste
 *
 * Shows the details of a particular order on the account page
 *
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version   2.2.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
do_action( 'woocommerce_account_navigation' ); ?>


<div class="woocommerce-MyAccount-content">
			<input type="hidden" name="count" value="1" />
			<div class="control-group" id="fields">
				<h2 class="control-label">Einkaufsliste</h2>
				<div class="controls" id="profs"> 
					<form class="input-append" method="POST">
						<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
							<div id="field"><input autocomplete="off" class="woocommerce-Input woocommerce-Input--email input-text" id="field1" name="prof1" type="text" placeholder="Haus" data-items="8"/><button id="b1" class="btn add-more" type="button">+</button></div>
						</p>
					<small style="text-transform: none;">Clicken Sie auf + um eine zusätzliche Einkaufsliste zu erstellen.</small><br>
					<br>
					<input type="submit" class="woocommerce-Button button" name="save_account_details" value="Einkaufsliste speichern">						
					</form>
				</div>
			</div>
			<hr />
			<div class="active-ein">
				<?php
					$user = get_current_user_id();
					$field = get_field('buying_lists', 'user_' . $user);
					$ids = explode(", ", $field);
					if(!empty($field)){
						foreach($ids as $id){
							if($id){
								$term = get_term($id);
								echo '<p><input class="woocommerce-Input woocommerce-Input--email input-text" disabled value="'.$term->name.'" name="prof1" type="text" /><button id="b1" onClick="removeBuyingList(\''.$term->name.'\',event)" class="btn btn-danger" type="button">X</button></p>';
							}
						}
					}
				
				?>
			</div>
</div>

<script type="text/Javascript">
	$ = jQuery;
	$(document).ready(function(){
		var next = 1;
		$(".add-more").click(function(e){
			e.preventDefault();
			var addto = "#field" + next;
			var addRemove = "#field" + (next);
			next = next + 1;
			var newIn = '<input autocomplete="off" class="woocommerce-Input woocommerce-Input--email input-text" id="field' + next + '" name="field' + next + '" type="text">';
			var newInput = $(newIn);
			var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
			var removeButton = $(removeBtn);
			$(addto).after(newInput);
			$(addRemove).after(removeButton);
			$("#field" + next).attr('data-source',$(addto).attr('data-source'));
			$("#count").val(next);  			
				$('.remove-me').click(function(e){
					e.preventDefault();
					var fieldNum = this.id.charAt(this.id.length-1);
					var fieldID = "#field" + fieldNum;
					$(this).remove();
					$(fieldID).remove();
				});
		});
		$('form').submit(function(e){			
			e.preventDefault();			
			var lists = new Array();
			$('#field input').each(function(){
				lists.push($(this).val());
			});			
			$.ajax({
				type : "post",
				url : '/wp-admin/admin-ajax.php',
				data : {action: "add_buyinglist", list: lists},
				dataType: "text",
				success: function(response) {
					window.location.href = "/shop/";
				}
			});	
			
		});
	});
</script>
<style type="text/css">
	.btn{
		height: 42px;
		width: 42px;
		border: none;
		border-radius: 0;
		margin-left: -1px;
		outline: none !important;
	}
    input{
		float: left;
		margin-bottom: 20px;
		width: 100%;
		width: calc(100% - 42px);
		max-width: 320px;
	}
	.btn, input{
		margin-bottom: 20px !important;
	}
	hr, .active-ein{
		clear: both;
	}
	.active-ein p{
		margin: 0;
	}
</style>
