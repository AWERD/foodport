

<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>

<div class="main-container col2-right-layout">
   <div class="main container">
      <div class="account-login">
         <div class="row">
         	<?php wc_print_notices(); ?>

         	<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

         	<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
            <div id="content" class="col-sm-12">
               <div class="page-title">
                  <h2>Account Login</h2>
               </div>
               <div class="col2-set">
                  <div class="col-1 new-users">
                     <div class="content">
                        <h2>New Customer</h2>
                        <p><strong>Register Account</strong></p>
                        <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                        <!-- <a href="" class="btn btn-primary"></a> -->
                        <button class="button create-account button-login-custom" type="button"><span>Continue</span></button>
                     </div>
                  </div>
                  <div class="col-2 registered-users">
                     <div class="content">
                        <h2>Returning Customer</h2>
                        <p><strong>I am a returning customer</strong></p>
                        <form method="post" enctype="multipart/form-data">
                        	<?php do_action( 'woocommerce_login_form_start' ); ?>
                           <div class="form-group">
														<label class="custom-login-label" for="username"><?php _e( 'E-Mail Address', 'woocommerce' ); ?></label>
														<input type="text" class="input-text-custom" name="username" id="username" placeholder="E-Mail Address" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                           </div>
                           <div class="form-group">
															<label class="custom-login-label" for="password"><?php _e( 'Password', 'woocommerce' ); ?></label>
															<input class="input-text-custom" placeholder="Password" type="password" name="password" id="password" />
                           </div>
                           <?php do_action( 'woocommerce_login_form' ); ?>
                           <!--  <input type="submit" value="" class="btn btn-primary" /> -->

														<?php wp_nonce_field( 'woocommerce-login' ); ?>
                           <!-- <button type="submit" class="button login"> Login </button> -->
                           <input type="submit" class="button button-login-custom" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
                           <!-- <a href="index.php?route=account?forgotten.html">Forgot Password</a> -->
                           <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Forgot Password', 'woocommerce' ); ?></a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <?php endif; ?>
         </div>
      </div>
   </div>
</div>

<!-- <?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="col2-set" id="customer_login">

	<div class="col-1">

<?php endif; ?>

		<h2><?php _e( 'Login', 'woocommerce' ); ?></h2>

		<form method="post" class="login">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-row form-row-wide">
				<label for="username"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			</p>
			<p class="form-row form-row-wide">
				<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="input-text" type="password" name="password" id="password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<label for="rememberme" class="inline">
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
				</label>
				<input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
				
			</p>
			<p class="lost_password">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Passwort vergessen?', 'woocommerce' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="col-2">

		<h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

		<form method="post" class="register">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>
			
			<p class="form-row form-row-wide">
				<label for="reg_username"><?php _e( 'Gender', 'woocommerce' ); ?> <span class="required">*</span></label>
				<label><input type="radio" value="Female" name="gender">Female</label>
				<label><input type="radio" value="Male" name="gender">Male</label>
			</p>
			
			<p class="form-row form-row-wide">
				<label for="reg_username"><?php _e( 'Geburtsdatum', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="geburtsdatum" value="<?php if ( ! empty( $_POST['geburtsdatum'] ) ) echo esc_attr( $_POST['geburtsdatum'] ); ?>" />
			</p>
			
			<p class="form-row form-row-wide">
				<label for="reg_username"><?php _e( 'Language', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="language" value="<?php if ( ! empty( $_POST['language'] ) ) echo esc_attr( $_POST['language'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="input-text" name="password" id="reg_password" />
				</p>

			<?php endif; ?>

			Spam Trap
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<p class="form-row">
				<label for="terms18"><input type="checkbox" class="" id="terms18" name="terms18" style="margin: 7px 0px;" /> <strong>Ich bestätige hiermit, dass ich über 18 Jahre alt bin.</strong> <span class="required">*</span></label>
			</p>
			
			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-register' ); ?>
				<input type="submit" class="button disabeld" id="register-button" disabled name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?> -->
<?php 
wp_enqueue_script('jquery.mobile-menu');
wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('owl.carousel.min');
 ?>