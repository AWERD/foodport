<?php 
/* Template name: lost-password */
get_header();
$TmFlavours = new TmFlavours();
?>

<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>

<div class="main-container col2-right-layout">
   <div class="main container">
      <div class="account-login">
         <div class="row">
            <div id="content" class="col-sm-12">
               <div class="page-title">
                  <h2>Account Login</h2>
               </div>
               <div class="col2-set">
                  <div class="col-1 new-users">
                     <div class="content">
                        <h2>New Customer</h2>
                        <p><strong>Register Account</strong></p>
                        <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                        <!-- <a href="" class="btn btn-primary"></a> -->
                        <button onclick="window.location='http://occrocus3.themessoft.com/index.php?route=account/register';" class="button create-account" type="button"><span>Continue</span></button>
                     </div>
                  </div>
                  <div class="col-2 registered-users">
                     <div class="content">
                        <h2>Returning Customer</h2>
                        <p><strong>I am a returning customer</strong></p>
                        <form action="http://occrocus3.themessoft.com/index.php?route=account/login" method="post" enctype="multipart/form-data">
                           <div class="form-group">
                              <label class="control-label" for="input-email">E-Mail Address</label>
                              <input type="text" name="email" value="" placeholder="E-Mail Address" id="input-email" class="form-control" />
                           </div>
                           <div class="form-group">
                              <label class="control-label" for="input-password">Password</label>
                              <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" />
                           </div>
                           <!--  <input type="submit" value="" class="btn btn-primary" /> -->
                           <button type="submit" class="button login"> Login </button>
                           <a href="index.php?route=account?forgotten.html">Forgot Password</a>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php 

wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('jquery.countdown.min');
wp_enqueue_script('jquery.mobile-menu.min');
wp_enqueue_script('owl.carousel.min');
wp_enqueue_script('countdown');
wp_enqueue_script('revslider');
?>



<?php tmFlavours_home_sub_banners ();?>
<?php get_footer();
