
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div style="color: gray;" class="col-xs-12">
             <?php woocommerce_breadcrumb(); ?>
         </div>
      </div>
   </div>
</div>
<section class="main-container col1-layout">
  <div class="main" id="content">
     <div class="container">
        <div class="col-main">
           <div class="product-view">
              <div class="product-essential">
                 <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                    <div class="product-image">
                       <div class="product-full"> <img id="product-zoom" src="<?php the_post_thumbnail_url(); ?>" data-zoom-image="<?php the_post_thumbnail_url(); ?>"/> 
                       </div>
                       <div class="more-views">
                          <div class="slider-items-products">
                             <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                                <div class="slider-items slider-width-col4 block-content">
                                    <?php
                                        global $product;

                                        $attachment_ids = $product->get_gallery_attachment_ids();

                                        foreach( $attachment_ids as $attachment_id ) { ?>
                                            <div class="more-views-items"> <a href="" data-image="<?php echo wp_get_attachment_image_src( $attachment_id )[0]; ?>" data-zoom-image="<?php echo wp_get_attachment_image_src( $attachment_id )[0]; ?>"> <img id="product-zoom"  src="<?php echo wp_get_attachment_image_src( $attachment_id )[0]; ?>" /> </a></div>
                                    <?php
                                        }
                                    ?>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                 <!-- product-img-box col-lg-6 col-sm-6 col-xs-12 -->
                 <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                    <div class="product-name">
                       <h1><?php  echo $product->get_title(); ?></h1>
                    </div>
                    <div class="ratings">
                       <div class="rating-box">
                          <div class="rating">
                            
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                             <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                          </div>
                       </div>
                       <p class="rating-links">
                          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">0 reviews</a>
                          <span class="separator">|</span> 
                          <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Write a review</a>
                       </p>
                    </div>
                    <!-- ratings -->
                    <div class="price-block">
                       <div class="price-box">
                          <p class="regular-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $product->get_price(); ?></span></p>
                          <?php if($product->get_stock_quantity() > 0) { ?>
                          <p class="availability in-stock"><span>
                                In Stock
                            <?php } else { ?>
                            <p class="availability not-in-stock"><span>
                                Out of Stock
                            <?php } ?>
                            </span>
                      </p>
                       </div>
                    </div>
                    <ul class="list-unstyled">
                       <!-- <li><span>Reward Points: </span>200</li> -->
                    </ul>
                    <ul class="list-unstyled">
                       <li><a href=""><?php echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', sizeof( get_the_terms( $post->ID, 'product_cat' ) ), 'woocommerce' ) . ' ', '.</span>' ); ?></a></li>
                       <!-- <li>Product Code:<?php echo $product->get_sku(); ?></li> -->
                       <!-- <li>Reward Points: 400</li> -->
                    </ul>
                    <div id="product">
                       <div class="add-to-box">
                          <div class="add-to-cart">
                             <label class="control-label" for="input-quantity">Qty</label>
                             <div class="pull-left">
                                <div class="custom pull-left">          
                                    <!-- <?php woocommerce_quantity_input(); ?> -->
                                   <button class="reduced items-count" onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) && qty > 0 ) result.value--;return false;" type="button">
                                   <i class="fa fa-minus"> </i>
                                   </button>
                                   <input type="text" name="quantity" value="1" size="2" id="qty" class="input-text quantity qty" maxlength="12"/>
                                   <button class="increase items-count" onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false; " type="button">
                                   <i class="fa fa-plus"> </i>
                                   </button>
                                   <input type="hidden" name="product_id" value="49" />
                                </div>
                             </div>
                             <div class="pull-left">
                                <?php
                                    echo do_shortcode('[add_to_cart id="'. $product->get_id() .'" show_price="false" style="border:0;"]');
                                ?>
                             </div>
                          </div>
                          <div class="email-addto-box">
                             <ul class="add-to-links">
                                <?php echo do_shortcode( '[yith_wcwl_add_to_wishlist link_classes="link-wishlist"]' ); ?>
                                <li>
                                    <?php echo do_shortcode('[yith_compare_button]'); ?>
                                </li>
                             </ul>
                          </div>
                       </div>
                    </div>
                    <script>
                        jQuery( document ).ready( function( $ ) {
                            $( document ).on( 'change', '#qty', function() {
                                $( '.ajax_add_to_cart' ).attr( 'data-quantity', $( '#qty' ).val() );
                            });
                            $( document ).on( 'click', '.increase', function() {
                                $( '.ajax_add_to_cart' ).attr( 'data-quantity', $( '#qty' ).val() );
                            });
                            $( document ).on( 'click', '.reduced', function() {
                                $( '.ajax_add_to_cart' ).attr( 'data-quantity', $( '#qty' ).val() );
                            });
                        });
                    </script>
                    <!-- product-id -->
                 </div>
              </div>
              <!-- product essential -->
           </div>
           <!-- product-view -->
        </div>
        <!-- col-main  -->
        <div class="product-collateral col-lg-12 col-sm-12 col-xs-12">
           <div class="row">
              <div class="add_info">
                 <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                    <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                    <li><a href="#tab-review" data-toggle="tab">Reviews (0)</a></li>
                 </ul>
                 <div id="productTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="tab-description">
                       <p><strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;"></strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;"><?php the_excerpt(); ?></span><br></p>
                    </div>
                    <div class="tab-pane" id="tab-review">
                       <form class="form-horizontal" id="form-review">
                          <div id="review"></div>
                          <h2>Write a review</h2>
                          <div class="form-group required">
                             <div class="col-sm-12">
                                <label class="control-label" for="input-name">Your Name</label>
                                <input type="text" name="name" value="" id="input-name" class="form-control" />
                             </div>
                          </div>
                          <div class="form-group required">
                             <div class="col-sm-12">
                                <label class="control-label" for="input-review">Your Review</label>
                                <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                             </div>
                          </div>
                          <div class="form-group required">
                             <div class="col-sm-12">
                                <label class="control-label">Rating</label>
                                &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                <input type="radio" name="rating" value="1" />
                                &nbsp;
                                <input type="radio" name="rating" value="2" />
                                &nbsp;
                                <input type="radio" name="rating" value="3" />
                                &nbsp;
                                <input type="radio" name="rating" value="4" />
                                &nbsp;
                                <input type="radio" name="rating" value="5" />
                                &nbsp;Good
                             </div>
                          </div>
                          <div class="buttons clearfix">
                             <div class="pull-right">
                                <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-primary">Continue</button>
                             </div>
                          </div>
                       </form>
                    </div>
                 </div>
              </div>
              <!-- col-sm-12 wow bounceInUp animated animated -->    
           </div>
        </div>
        <!-- product-collateral -->   
     </div>
  </div>
</section>
<div class="container">
   <div class="related-pro">
      <div class="slider-items-products">
         <div class="related-block">
            <div class="block-title">
               <h2>Related</h2>
            </div>
            <div id="related-products-slider" class="product-flexslider hidden-buttons">
               <div class="slider-items slider-width-col4 products-grid block-content">
                  <!-- Item -->
                  <?php
                   
                  /* crossells */
                   
                  $crosssell_ids = get_post_meta( get_the_ID(), '_crosssell_ids' ); 
                  $crosssell_ids = $crosssell_ids[0];
                   
                  if(count($crosssell_ids)>0){
                  $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'post__in' => $crosssell_ids );
                  $loop = new WP_Query( $args );
                  while ( $loop->have_posts() ) : $loop->the_post();
                  ?>
                  <div class="item">
                     <div class="item-inner">
                        <div class="item-img">
                           <div class="item-img-info">
                              <a class="product-image" href="<?php the_permalink(); ?>" title="Retis lapen casen">
                              <img src="<?php the_post_thumbnail_url(); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
                              </a>
                              <div class="box-hover">
                                 <ul class="add-to-links">
                                    <li>
                                       <a class="link-wishlist"></a> 
                                    </li>
                                    <li>
                                       <a class="link-compare"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="item-info">
                           <div class="info-inner">
                              <div class="item-title"> 
                                 <a title="Retis lapen casen" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                              </div>
                              <div class="item-content">
                                 <div class="rating">
                                    <div class="ratings">
                                       <div class="rating-box">
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- rating -->
                                 <div class="item-price">
                                    <div class="price-box">
                                       <p class="regular-price"><span class="price"><?php // echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
                                    </div>
                                 </div>
                                 <div class="action">
                                    <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart"><a style="color: white;" href="<?php echo $product->add_to_cart_url($product->id, 3); ?>"><span>Add to Cart</span></a></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- End Item info --> 
                     </div>
                     <!-- End  Item inner--> 
                  </div>
                  <?php
                  endwhile;
                  }
                  ?>
               </div>
            </div>
            <!-- featured --> 
         </div>
      </div>
      <!-- slider Item products --> 
   </div>
</div>



<!-- <?php woocommerce_upsell_display(); ?> -->
<?php 
wp_enqueue_script('revslider');
wp_enqueue_script('jquery.mobile-menu');
wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('owl.carousel.min');
wp_enqueue_script('jquery.mobile-menu.min');
wp_enqueue_script('jquery.countdown.min');
wp_enqueue_script('jquery.flexslider');
wp_enqueue_script('cloud-zoom');
wp_enqueue_script('jquery.magnific-popup.min');
wp_enqueue_script('moment');
wp_enqueue_script('bootstrap-datetimepicker.min');
 ?>
