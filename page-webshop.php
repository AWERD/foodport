<?php
/* Template name: Web Shop */
get_header(); ?>

<div id="thm-mart-slideshow" class="thm-mart-slideshow cut-down" style="max-height:400px;">
  <div class="container">
    <div id="thm_slider_wrapper" class="thm_slider_wrapper fullwidthbanner-container" style="max-height: none; overflow: visible; height: 375px;">
      <div id="thm-rev-slider" class="rev_slider fullwidthabanner revslider-initialised tp-simpleresponsive" style="height: 100%; max-height: none;">
      <ul style="display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;">
        <li style="width: 100%; height: 100%; overflow: hidden; visibility: visible; left: 0px; top: 0px; z-index: 20; opacity: 1; position: absolute; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00166, 0, 0, 0, 1); transform-origin: center center 0px;"><div class="slotholder" style="width: 100%; height: 100%; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00166, 0, 0, 0, 1);" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="left top" data-kenburns="undefined" data-easeme="undefined" data-bgfit="cover" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined"><div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" data-lazydone="undefined" src="http://wptest.themesmart.net/wp-content/uploads/2016/02/9595slide-img1.jpg" data-src="http://wptest.themesmart.net/wp-content/uploads/2016/02/9595slide-img1.jpg" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url(&quot;http://wptest.themesmart.net/wp-content/uploads/2016/02/9595slide-img1.jpg&quot;); background-size: cover; background-position: left top; width: 100%; height: 100%; opacity: 1; position: relative;"></div></div>
           <div class="info">
            <div class="tp-caption ExtraLargeTitle sft  tp-resizeme  start" data-x="0" data-y="210" data-endspeed="500" data-speed="500" data-start="1100" data-easing="Linear.easeNone" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 2; white-space: nowrap; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 70px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 18px; left: 0px; top: 210px; visibility: visible; opacity: 0.71; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00218, 0, -14.5, 0, 1);"><span style="transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 150px; border-width: 10px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 22px;">Get 50% off</span></div>
                <div class="tp-caption LargeTitle sfl  tp-resizeme  start" data-x="0" data-y="300" data-endspeed="500" data-speed="500" data-start="1300" data-easing="Linear.easeNone" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 3; white-space: nowrap; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: -3px; font-size: 120px; left: 0px; top: 300px; visibility: visible; opacity: 0.31; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00185, -34.5, 0, 0, 1);"><span style="transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 100px; border-width: 0px; margin: 0px; padding: 0px 20px; letter-spacing: -3px; font-size: 120px;">Simply delicious</span></div>
                <div class="tp-caption sfb  tp-resizeme  start" data-x="0" data-y="550" data-endspeed="500" data-speed="500" data-start="1500" data-easing="Linear.easeNone" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 4; white-space: nowrap; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 13px; left: 0px; top: 550px; visibility: visible; opacity: 0; transform: none;"><a href="#" class="buy-btn" style="transition: all 0.3s cubic-bezier(0.8, 0, 0, 1) 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 2px; margin: 0px 0px 0px 10px; padding: 18px 35px; letter-spacing: 1px; font-size: 16px;">Shop Now</a></div>
                <div class="tp-caption Title sft  tp-resizeme  start" data-x="0" data-y="420" data-endspeed="500" data-speed="500" data-start="1500" data-easing="Power2.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 4; white-space: nowrap; transition: all 0s ease 0s; min-height: 0px; min-width: 0px; line-height: 20px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-size: 24px; left: 0px; top: 420px; visibility: visible; opacity: 0; transform: none;">Little things make a big difference</div>
 
           </div>  
          </li>      
      </ul>
    <div class="tp-loader spinner0" style="visibility: hidden; opacity: 0;"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div><div class="tp-bannertimer" style="visibility: visible; width: 9.1%;"></div></div>  
  <div class="tp-bullets tp-thumbs round hidebullets" style="width: 406px; height: 56px; bottom: 20px; left: 50%; margin-left: -203px;"><div class="tp-mask" style="width: 400px; height: 50px;"><div class="tp-thumbcontainer" style="left: 0px;"><div class="bullet thumb first selected" style="background-color:rgba(0, 0, 0, 0);position:relative;width:200px;height:50px;background-image:url(http://wptest.themesmart.net/wp-content/uploads/2016/02/9595slide-img1-150x150.jpg) !important;background-size:cover;background-position:center center;"></div><div class="bullet thumb last" style="background-color:rgba(0, 0, 0, 0);position:relative;width:200px;height:50px;background-image:url(http://wptest.themesmart.net/wp-content/uploads/2016/02/7855slide-img2-150x150.jpg) !important;background-size:cover;background-position:center center;"></div></div></div></div></div>
 </div>
</div>

<div id="top">
    <div class="container">
		<div class="row">
			<ul>            
                <li>
					<div>         
					  <a href="#" title="esc_attr_e('link', 'flavours')">
						<img alt="offer banner1" src="http://wptest.themesmart.net/wp-content/uploads/2016/02/register.png">              
					  </a>        
					</div>
				</li>
            </ul>  
        </div>
    </div>
</div>

<?php getTopCategories(); ?>
<?php get_footer(); ?>