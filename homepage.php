<?php
/* Template name: Home */
get_header();
$TmFlavours = new TmFlavours();
?>


<div>
  <div id="content">
     <div id="thmsoft-slideshow" class="thmsoft-slideshow">
        <div class="container">
           <div class="row">
              <div class="thm_topsection">
                 <div class="col-md-9 col-sm-12">
                    <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
                       <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                          <ul>
                             <style>#ms0{ cursor: pointer;}</style>                         
                             <li id="ms0" data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='<?php echo wp_get_attachment_url(12336); ?>'>
                                <img src='<?php echo wp_get_attachment_url(12336); ?>' alt="Slide1" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' /> 
                                <div class="info">
                                   <div class='tp-caption ExtraLargeTitle sft tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;'>
                                      <span>Crocus Deal</span>
                                   </div>
                                   <div class='tp-caption LargeTitle sfl tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'>
                                      <span>digital Time</span>
                                   </div>
                                   <div class='tp-caption Title sft tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                                   <div class='tp-caption sfb tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>
                                      <a href='#' class="buy-btn">Shop Now</a>
                                   </div>
                                </div>
                             </li>
                             <style>#ms1{ cursor: pointer;}</style>
                             <li id="ms1" data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='<?php echo wp_get_attachment_url(12335); ?>'>
                                <img src='<?php echo wp_get_attachment_url(12335); ?>' alt="Slide2" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />       
                                <div class="info">
                                   <div class='tp-caption ExtraLargeTitle sft slide2 tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;padding-right:0px'>
                                      <span>Spring Collection</span>
                                   </div>
                                   <div class='tp-caption LargeTitle sfl tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'>Hot Fashion</div>
                                   <div class='tp-caption Title sft tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                                   <div class='tp-caption sfb tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>
                                      <a href='#' class="buy-btn">Buy Now</a> 
                                   </div>
                                </div>
                             </li>
                          </ul>
                       </div>
                    </div>
                    <script type='text/javascript'>
                    jQuery(document).ready(function() {
                        jQuery('#rev_slider_4').show().revolution({
                        dottedOverlay: 'none',
                        delay: 5000,
                        startwidth: 877,
                        startheight: 493,
                        hideThumbs: 200,
                        thumbWidth: 200,
                        thumbHeight: 50,
                        thumbAmount: 2,
                        navigationType: 'thumb',
                        navigationArrows: 'solo',
                        navigationStyle: 'round',
                        touchenabled: 'on',
                        onHoverStop: 'on',
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                        spinner: 'spinner0',
                        keyboardNavigation: 'off',
                        navigationHAlign: 'center',
                        navigationVAlign: 'bottom',
                        navigationHOffset: 0,
                        navigationVOffset: 20,
                        soloArrowLeftHalign: 'left',
                        soloArrowLeftValign: 'center',
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,
                        soloArrowRightHalign: 'right',
                        soloArrowRightValign: 'center',
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,
                        shadow: 0,
                        fullWidth: 'on',
                        fullScreen: 'off',
                        stopLoop: 'off',
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: 'off',
                        autoHeight: 'off',
                        forceFullWidth: 'on',
                        fullScreenAlignForce: 'off',
                        minFullScreenHeight: 0,
                        hideNavDelayOnMobile: 1500,
                        hideThumbsOnMobile: 'off',
                        hideBulletsOnMobile: 'off',
                        hideArrowsOnMobile: 'off',
                        hideThumbsUnderResolution: 0,
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        startWithSlide: 0,
                        fullScreenOffsetContainer: ''
                    });
                    });
                    </script>
                 </div>
                 <div class="col-md-3 col-sm-12">
                    <div class="home-banner">
                       <img src="<?php echo wp_get_attachment_url(12333); ?>">
                    </div>
                    <div class="home-banner1">
                       <img src="<?php echo wp_get_attachment_url(12334); ?>">
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>

     <div class="our-features-box home_feature">
        <div class="container">
           <div class="row">
              <div class="col-md-3 col-xs-12 col-sm-6">
                 <div class="feature-box first">
                    <span class="fa fa-truck"></span>
                    <div class="content">
                       <h3>FREE WORLDWIDE DELIVERY</h3>
                    </div>
                 </div>
              </div>
              <div class="col-md-3 col-xs-12 col-sm-6">
                 <div class="feature-box">
                    <span class="fa fa-headphones"></span>
                    <div class="content">
                       <h3>24X7 HELP CENTER</h3>
                    </div>
                 </div>
              </div>
              <div class="col-md-3 col-xs-12 col-sm-6">
                 <div class="feature-box">
                    <span class="fa fa-share"></span>
                    <div class="content">
                       <h3>EASY RETURNS AND EXCHANGE</h3>
                    </div>
                 </div>
              </div>
              <div class="col-md-3 col-xs-12 col-sm-6">
                 <div class="feature-box last">
                    <span class="fa fa-phone"></span>
                    <div class="content">
                       <h3>Helpline +0800 567 345</h3>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
   </div>
 </div>
 <section class="bestsell-pro">
    <div class="container">
       <div class="slider-items-products">
          <div class="bestsell-block">
             <div id="bestsell-slider" class="product-flexslider hidden-buttons">
                <div class="block-title">
                   <h2>Best<em>Sellers</em></h2>
                </div>
                <div class="slider-items slider-width-col4 products-grid block-content">

								<?php
								$args = array(
								    'post_type' => 'product',
								    'meta_key' => 'total_sales',
								    'orderby' => 'meta_value_num',
								    'posts_per_page' => 16,
								);
								 
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post(); 
								global $product; 
								$_product = wc_get_product( $loop->post->ID );
								?>
	                <div class="item">
	                   <div class="item-inner">
	                      <div class="item-img">
	                         <div class="item-img-info">
	                            <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="Retis lapen casen">
	                            <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                            </a>
	                            <div class="box-hover">
	                               <ul class="add-to-links">
	                                  <li>
	                                     <!-- <a href="index.phpїroute=productЎquickview&product_id=59;.html" class="link-quickview" data-name="Retis lapen casen"></a> -->
                                      <?php echo do_shortcode('[yith_quick_view product_id="' . $product->get_id() . '"  type="icon" label=""]'); ?>
	                                  </li>
	                                  <li>
	                                     <!-- <a onclick="thm_hm_wishlist.add('59');" class="link-wishlist"></a>  -->
                                       <?php echo do_shortcode( '[yith_wcwl_add_to_wishlist link_classes="link-wishlist"]' ); ?>
	                                  </li>
	                                  <li>
	                                     <a class="link-compare"></a>
	                                  </li>
	                               </ul>
	                            </div>
	                         </div>
	                      </div>
	                      <div class="item-info">
	                         <div class="info-inner">
	                            <div class="item-title"> 
	                               <a title="<?php the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>">
	                              <?php the_title(); ?></a>
	                            </div>
	                            <div class="rating">
	                               <div class="ratings">
	                                  <div class="rating-box">
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                  </div>
	                               </div>
	                            </div>
	                            <div class="item-content">
	                               <div class="item-price">
	                                  <div class="price-box">
	                                     <p class="regular-price"><?php echo WC()->cart->get_product_price($_product) ?></span></p>
	                                  </div>
	                               </div>
	                               <div class="action">
                                  <?php
                                      echo do_shortcode('[add_to_cart id="'. $product->get_id() .'" show_price="false" style="border:0;"]');
                                  ?>
	                                  <!-- <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart"><a href="<?php echo $_product->add_to_cart_url() ?>"><span>Add to Cart</span></a></button> -->
	                               </div>
	                            </div>
	                         </div>
	                      </div>
	                   </div>
	                </div>

									<?php endwhile; ?>
									<?php wp_reset_query(); ?>

	             </div>
	          </div>
	       </div>
	    </div>
	 </div>
	</section>

	<div>
	   <div class="container">
	      <div class="row">
	         <div class="col-md-6 col-lg-5 col-sm-12 special bestsell-pro">
	            <div class="bestsell-block slider-items-products">
	               <div id="special" class="product-flexslider hidden-buttons">
	                  <div class="block-title">
	                     <h2>Specials</h2>
	                  </div>
	                  <div class="slider-items slider-width-col4 products-grid block-content">
											<?php
											$args = array(
											    'post_type'      => 'product',
											    'posts_per_page' => 8,
											    'meta_query'     => array(
											        'relation' => 'OR',
											        array( // Simple products type
											            'key'           => '_sale_price',
											            'value'         => 0,
											            'compare'       => '>',
											            'type'          => 'numeric'
											        ),
											        array( // Variable products type
											            'key'           => '_min_variation_sale_price',
											            'value'         => 0,
											            'compare'       => '>',
											            'type'          => 'numeric'
											        )
											    )
											);
											 
											$loop = new WP_Query( $args );
											while ( $loop->have_posts() ) : $loop->the_post(); 
											global $product; 
											$_product = wc_get_product( $loop->post->ID );
											?>

											<div class="item">
											   <div class="item-inner">
											      <div class="item-img">
											         <div class="item-img-info">
											            <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php the_title(); ?>">
											            <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
											            </a>
											            <div class="sale-label sale-top-right">Sale</div>
											            <div class="box-hover">
											               <ul class="add-to-links">
											                  <li>
											                     <a href="index.phpїroute=productЎquickview&product_id=62;.html" class="link-quickview" data-name="Retis lapen casen"></a>
											                  </li>
											                  <li>
											                     <a onclick="thm_hm_wishlist.add('62');" class="link-wishlist"></a> 
											                  </li>
											                  <li>
											                     <a class="link-compare"  onclick="thm_hm_compare.add('62');"></a>
											                  </li>
											               </ul>
											            </div>
											         </div>
											      </div>
											      <div class="item-info">
											         <div class="info-inner">
											            <div class="item-title"> 
											               <a title="<?php the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>">
											               <?php the_title(); ?></a>
											            </div>
											            <div class="rating">
											               <div class="ratings">
											                  <div class="rating-box">
											                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
											                  </div>
											               </div>
											            </div>
											            <div class="item-content">
											               <div class="item-price">
											                  <div class="price-box">
											                     <p class="old-price"><span class="price">$1,000.00</span></p>
											                     <p class="special-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
											                  </div>
											               </div>
											               <div class="action">
											                  <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('62');"><a href="<?php echo $_product->add_to_cart_url() ?>"><span>Add to Cart</span></a></button>
											               </div>
											            </div>
											         </div>
											      </div>
											   </div>
											</div>


											<?php endwhile; ?>
											<?php wp_reset_query(); ?>
				
	                  </div>
	               </div>
	            </div>
	         </div>
	         <div class="col-md-6 col-lg-7 col-sm-12">
	            <div class="offer-slider parallax parallax-2">
	               <h2>Hot Deals</h2>
	               <div class="starSeparator"></div>
	               <p> <span>Retis lapen casen:</span> Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. ..          </p>
	               <div class="box-timer">
	                  <div class="timer-grid"  data-time="2018-10-09"></div>
	               </div>
	               <a target="_self" href="index.phpїroute=productЎproduct&product_id=56.html" class="shop-now">Shop Now</a>
	               <div></div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>
	<div class="content-page">
	   <div class="container">
	      <div class="row">
	         <div class="col-md-12">
	            <!-- featured category fashion -->
	            <div class="category-product">
	               <div class="navbar nav-menu">
	                  <div class="navbar-collapse">
	                     <ul class="nav navbar-nav">
	                        <li>
	                           <div class="new_title">
	                              <h2>Hot Products</h2>
	                           </div>
	                        </li>
	                        <li class="active"><a data-toggle="tab" href="#tab-1">Salads</a></li>
	                        <li class=" "><a data-toggle="tab" href="#tab-2">Cream Soups</a></li>
	                        <li class=""><a data-toggle="tab" href="#tab-3" class="active">Sandwiches</a></li>
	                        <li class=""><a data-toggle="tab" href="#tab-4" class="active">Pizza Strips</a></li>
	                     </ul>
	                  </div>
	                  <!-- /.navbar-collapse --> 
	               </div>
	               
	               <div class="product-bestseller">
	                  <div class="product-bestseller-content">
	                     <div class="product-bestseller-list">
	                        <div class="tab-container">
	                           <!-- tab product -->
	                           <div class="tab-panel active" id="tab-1">
	                              <div class="category-products">
	                                 <ul class="products-grid">
	                                 	<?php 
	                                 	$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'fruechte-und-gemuese', 'orderby' => 'rand' );
	                                 	$loop = new WP_Query($args);
																		while ( $loop->have_posts() ) : $loop->the_post(); 
																		global $product; 
																		$_product = wc_get_product( $loop->post->ID );
	                                 	?>
	                                    <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
	                                       <div class="item-inner">
	                                          <div class="item-img">
	                                             <div class="item-img-info">
	                                                <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php the_title(); ?>">
	                                                <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                                                </a>
	                                                <div class="box-hover">
	                                                   <ul class="add-to-links">
	                                                      <li>
	                                                         <a href="index.phpїroute=productЎquickview&product_id=66;.html" class="link-quickview" data-name="Retis lapen casen"></a>
	                                                      </li>
	                                                      <li>
	                                                         <a onclick="thm_hm_wishlist.add('66');" class="link-wishlist"></a> 
	                                                      </li>
	                                                      <li>
	                                                         <a class="link-compare"  onclick="thm_hm_compare.add('66');"></a>
	                                                      </li>
	                                                   </ul>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <div class="item-info">
	                                             <div class="info-inner">
	                                                <div class="item-title"> 
	                                                   <a title="<?php the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>"><?php echo the_title(); ?></a>
	                                                </div>
	                                                <div class="item-content">
	                                                   <div class="rating">
	                                                      <div class="ratings">
	                                                         <div class="rating-box">
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                   <!-- rating -->
	                                                   <div class="item-price">
	                                                      <div class="price-box">
	                                                         <p class="regular-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
	                                                      </div>
	                                                   </div>
	                                                   <div class="action">
	                                                      <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('66');"><a href="<?php echo $_product->add_to_cart_url(); ?>"><span>Add to Cart</span></a></button>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <!-- End Item info --> 
	                                       </div>
	                                       <!-- End  Item inner--> 
	                                    </li>
	                                    <?php endwhile; ?>
	                                    <?php wp_reset_query(); ?>

	                                 </ul>
	                              </div>
	                           </div>
	                           <!-- tab product -->
	                           <div class="tab-panel " id="tab-2">
	                              <div class="category-products">
	                                 <ul class="products-grid">
	                                  <?php 
	                                 	$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'suppen', 'orderby' => 'rand' );
	                                 	$loop = new WP_Query($args);
																		while ( $loop->have_posts() ) : $loop->the_post(); 
																		global $product; 
																		$_product = wc_get_product( $loop->post->ID );
	                                 	?>
	                                    <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
	                                       <div class="item-inner">
	                                          <div class="item-img">
	                                             <div class="item-img-info">
	                                                <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php echo the_title(); ?>">
	                                                <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                                                </a>
	                                                <div class="box-hover">
	                                                   <ul class="add-to-links">
	                                                      <li>
	                                                         <a href="index.phpїroute=productЎquickview&product_id=66;.html" class="link-quickview" data-name="Retis lapen casen"></a>
	                                                      </li>
	                                                      <li>
	                                                         <a onclick="thm_hm_wishlist.add('66');" class="link-wishlist"></a> 
	                                                      </li>
	                                                      <li>
	                                                         <a class="link-compare"  onclick="thm_hm_compare.add('66');"></a>
	                                                      </li>
	                                                   </ul>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <div class="item-info">
	                                             <div class="info-inner">
	                                                <div class="item-title"> 
	                                                   <a title="<?php echo the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>"><?php echo the_title(); ?></a>
	                                                </div>
	                                                <div class="item-content">
	                                                   <div class="rating">
	                                                      <div class="ratings">
	                                                         <div class="rating-box">
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                   <!-- rating -->
	                                                   <div class="item-price">
	                                                      <div class="price-box">
	                                                         <p class="regular-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
	                                                      </div>
	                                                   </div>
	                                                   <div class="action">
	                                                      <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('66');"><a href="<?php echo $_product->add_to_cart_url(); ?>"><span>Add to Cart</span></a></button>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <!-- End Item info --> 
	                                       </div>
	                                       <!-- End  Item inner--> 
	                                    </li>
	                                    <?php endwhile; ?>
	                                    <?php wp_reset_query(); ?>
	                                 </ul>
	                              </div>
	                           </div>
	                           <div class="tab-panel " id="tab-3">
	                              <div class="category-products">
	                                 <ul class="products-grid">
	                                  <?php 
	                                 	$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'backwaren', 'orderby' => 'DESC' );
	                                 	$loop = new WP_Query($args);
																		while ( $loop->have_posts() ) : $loop->the_post(); 
																		global $product; 
																		$_product = wc_get_product( $loop->post->ID );
	                                 	?>
	                                    <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
	                                       <div class="item-inner">
	                                          <div class="item-img">
	                                             <div class="item-img-info">
	                                                <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php echo the_title(); ?>">
	                                                <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                                                </a>
	                                                <div class="box-hover">
	                                                   <ul class="add-to-links">
	                                                      <li>
	                                                         <a href="index.phpїroute=productЎquickview&product_id=66;.html" class="link-quickview" data-name="Retis lapen casen"></a>
	                                                      </li>
	                                                      <li>
	                                                         <a onclick="thm_hm_wishlist.add('66');" class="link-wishlist"></a> 
	                                                      </li>
	                                                      <li>
	                                                         <a class="link-compare"  onclick="thm_hm_compare.add('66');"></a>
	                                                      </li>
	                                                   </ul>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <div class="item-info">
	                                             <div class="info-inner">
	                                                <div class="item-title"> 
	                                                   <a title="<?php echo the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>"><?php echo the_title(); ?></a>
	                                                </div>
	                                                <div class="item-content">
	                                                   <div class="rating">
	                                                      <div class="ratings">
	                                                         <div class="rating-box">
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                   <!-- rating -->
	                                                   <div class="item-price">
	                                                      <div class="price-box">
	                                                         <p class="regular-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
	                                                      </div>
	                                                   </div>
	                                                   <div class="action">
	                                                      <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('66');"><a href="<?php echo $_product->add_to_cart_url(); ?>"><span>Add to Cart</span></a></button>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <!-- End Item info --> 
	                                       </div>
	                                       <!-- End  Item inner--> 
	                                    </li>
	                                    <?php endwhile; ?>
	                                    <?php wp_reset_query(); ?>
	                                 </ul>
	                              </div>
	                           </div>
	                           <div class="tab-panel " id="tab-4">
	                              <div class="category-products">
	                                 <ul class="products-grid">
	                                 	<?php 
	                                 	$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => 'chips-und-snacks', 'orderby' => 'rand' );
	                                 	$loop = new WP_Query($args);
																		while ( $loop->have_posts() ) : $loop->the_post(); 
																		global $product; 
																		$_product = wc_get_product( $loop->post->ID );
	                                 	?>
	                                    <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
	                                       <div class="item-inner">
	                                          <div class="item-img">
	                                             <div class="item-img-info">
	                                                <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php echo the_title(); ?>">
	                                                <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                                                </a>
	                                                <div class="box-hover">
	                                                   <ul class="add-to-links">
	                                                      <li>
	                                                         <a href="index.phpїroute=productЎquickview&product_id=66;.html" class="link-quickview" data-name="Retis lapen casen"></a>
	                                                      </li>
	                                                      <li>
	                                                         <a  class="link-wishlist"></a> 
	                                                      </li>
	                                                      <li>
	                                                         <a class="link-compare"  onclick="thm_hm_compare.add('66');"></a>
	                                                      </li>
	                                                   </ul>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <div class="item-info">
	                                             <div class="info-inner">
	                                                <div class="item-title"> 
	                                                   <a title="<?php echo the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>"><?php echo the_title(); ?></a>
	                                                </div>
	                                                <div class="item-content">
	                                                   <div class="rating">
	                                                      <div class="ratings">
	                                                         <div class="rating-box">
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                   <!-- rating -->
	                                                   <div class="item-price">
	                                                      <div class="price-box">
	                                                         <p class="regular-price"><span class="price"><?php echo get_woocommerce_currency_symbol() . $_product->get_price(); ?></span></p>
	                                                      </div>
	                                                   </div>
	                                                   <div class="action">
	                                                      <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('66');"><a href="<?php echo $_product->add_to_cart_url(); ?>"><span>Add to Cart</span></a></button>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                          <!-- End Item info --> 
	                                       </div>
	                                       <!-- End  Item inner--> 
	                                    </li>
	                                    <?php endwhile; ?>
	                                    <?php wp_reset_query(); ?>
	                                 </ul>
	                              </div>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>
	<div class="bottom-banner-section">
	   <div class="container">
	      <div>
	         <img alt="banner" src="<?php echo wp_get_attachment_url(12337); ?>">
	      </div>
	   </div>
	</div>
	 <section class="featured-pro">
	    <div class="container">
	       <div class="slider-items-products">
	          <div class="featured-block">
	             <div class="home-block-inner">
	                <div class="block-title">
	                   <h2>Top Products</h2>
	                </div>
	             </div>
	             <div id="featured-slider" class="product-flexslider hidden-buttons">
	                <div class="slider-items slider-width-col4 products-grid block-content">

								<?php
								$args = array(
								    'post_type' => 'product',
								    'meta_key' => 'total_sales',
								    'orderby' => 'meta_value_num',
								    'posts_per_page' => 16,
								);
								 
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post(); 
								global $product; 
								$_product = wc_get_product( $loop->post->ID );
								?>
	                <div class="item">
	                   <div class="item-inner">
	                      <div class="item-img">
	                         <div class="item-img-info">
	                            <a class="product-image" href="<?php echo get_permalink($_product->get_id()); ?>" title="<?php the_title(); ?>">
	                            <img src="<?php echo get_the_post_thumbnail_url($loop->post->ID); ?>" alt="Retis lapen casen" title="Retis lapen casen"/>
	                            </a>
	                            <div class="box-hover">
	                               <ul class="add-to-links">
	                                  <li>
	                                     <a href="index.phpїroute=productЎquickview&product_id=59;.html" class="link-quickview" data-name="Retis lapen casen"></a>
	                                  </li>
	                                  <li>
	                                     <a onclick="thm_hm_wishlist.add('59');" class="link-wishlist"></a> 
	                                  </li>
	                                  <li>
	                                     <a class="link-compare"  onclick="thm_hm_compare.add('59');"></a>
	                                  </li>
	                               </ul>
	                            </div>
	                         </div>
	                      </div>
	                      <div class="item-info">
	                         <div class="info-inner">
	                            <div class="item-title"> 
	                               <a title="<?php the_title(); ?>" href="<?php echo get_permalink($_product->get_id()); ?>">
	                              <?php the_title(); ?></a>
	                            </div>
	                            <div class="rating">
	                               <div class="ratings">
	                                  <div class="rating-box">
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                     <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
	                                  </div>
	                               </div>
	                            </div>
	                            <div class="item-content">
	                               <div class="item-price">
	                                  <div class="price-box">
	                                     <p class="regular-price"><?php echo get_woocommerce_currency_symbol() ?><span class="price"><?php echo $_product->get_price(); ?></span></p>
	                                  </div>
	                               </div>
	                               <div class="action">
	                                  <button type="button" title="" data-original-title="Add to Cart" class="button btn-cart link-cart" onclick="thm_hm_cart.add('59');"><a href="<?php echo $_product->add_to_cart_url() ?>"><span>Add to Cart</span></a></button>
	                               </div>
	                            </div>
	                         </div>
	                      </div>
	                   </div>
	                </div>

									<?php endwhile; ?>
									<?php wp_reset_query(); ?>

	             </div>
	          </div>
	       </div>
	    </div>
	 </div>
	</section>


	<div class="container">
	   <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	         <div class="blog-outer-container">
	            <div class="blog-inner">
	               <div class="row">
	               	<?php
	               	    $next_args = array(
	               	                    'post_type' => 'post',
	               	                    'post_status' => 'publish',
	               	                    'posts_per_page' => 2,
	               	                    'order'=>'DESC',
	               	                    'orderby'=>'ID',
	               	                    );

	               	    $the_query = new WP_Query( $next_args );
	               	    if ( $the_query->have_posts() ) {
	               	        while ( $the_query->have_posts() ) {
	               	            $the_query->the_post();
	               	            ?>

	               	            <div class="col-lg-6 col-md-6 col-sm-6 blog-preview_item">
	               	               <div class="entry-thumb image-hover2">
	               	                  <a href="<?php the_permalink($the_query->post->ID); ?>">
	               	                  <img src="<?php the_post_thumbnail_url($the_query->post->ID); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/></a>
	               	               </div>
	               	               <div class="blog-preview_info">
	               	                  <ul class="post-meta">
	               	                     <li><i class="fa fa-user"></i><a href="index.phpїroute=thmsoftblogЎarticleЎview&thmblogarticle_id=7.html"><?php the_author(); ?></a></li>
	               	                     <li><i class="fa fa-comments"></i><a href="index.phpїroute=thmsoftblogЎarticleЎview&thmblogarticle_id=7.html"><?php echo comments_number(); ?></a></li>
	               	                     <li><i class="fa fa-calendar"></i><?php the_date('M d, Y'); ?></li>
	               	                  </ul>
	               	                  <h4 class="blog-preview_title"><a href="<?php the_permalink($the_query->post->ID); ?>"><?php the_title(); ?></a></h4>
	               	                  <div class="blog-preview_desc"><?php the_excerpt(); ?></div>
	               	                  <a class="blog-preview_btn" href="<?php the_permalink($the_query->post->ID); ?>">Read More</a>                        
	               	               </div>
	               	            </div>

	               	            <?php
	               	        }
	               	    } else {
	               	        echo "";
	               	    }
	               	    wp_reset_postdata();
	               	    ?>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>


<?php 

wp_enqueue_script('common');
wp_enqueue_script('common1');
wp_enqueue_script('jquery.countdown.min');
wp_enqueue_script('jquery.mobile-menu.min');
wp_enqueue_script('owl.carousel.min');
wp_enqueue_script('countdown');
wp_enqueue_script('revslider');
?>



<?php tmFlavours_home_sub_banners ();?>
<?php get_footer();


